###############################################
# This file contains the list of syscalls     #
###############################################
import re
import os
import subprocess
import pprint
from collections import deque

syscalls_currenly_inuse = set()
directory_to_search = '/home/nos1abt/testing/c_files'


syscalls_1_2 = set()
with open('strace_calls.txt', 'r') as f:
    data = f.read()
lines = data.split('\n')
# count number of calls
counter = 0
# set of unique syscalls(1), syscalls(2)
pattern = r'\d+\s+(\w+)\s+'
for line in lines:
    counter += 1
    match = re.search(pattern, line)
    # If a match is found, add to set()
    if match:
        syscalls_1_2.add(match.group(1))

# ! Test fetch_syscalls()
#  /usr/lib/gcc/x86_64-linux-gnu/11/include
# print(syscalls_1_2)
# if "chmod" in syscalls_1_2:
#     print("xxxxxxxx")


#####################################################
#  find all the files that end with either  C/CPP   #
#####################################################
def find_and_delete_preprocessed_files(directory):
    for root, _, files in os.walk(directory):
        for filename in files:
            if filename.endswith(('_preprocessed.c', '_preprocessed.cpp')):
                file_path = os.path.join(root, filename)
                os.remove(file_path)

#####################################################

# take a directory and return a set of all the files that end with either .c or .cpp


def find_c_cpp_files(directory) -> set:
    c_cpp_files = set()
    find_and_delete_preprocessed_files(directory)
    for root, _, files in os.walk(directory):
        for filename in files:
            if filename.endswith(('.c', '.cpp')):
                file_path = os.path.join(root, filename)
                c_cpp_files.add(os.path.abspath(file_path))

    return c_cpp_files


# # ! Test the function - find_c_cpp_files():
# c_cpp_files_set = find_c_cpp_files(directory_to_search)

# # ! Print the set of file paths
# for file_path in c_cpp_files_set:
#     print(file_path)
#####################################################
#  find all the headers in C/CPP                    #
#####################################################


def headers_hunter(list_of_c_cpp_files):
    headers_set = set()

    for file_path in list_of_c_cpp_files:
        if len(file_path) > 2:
            try:
                with open(file_path, 'r') as file:
                    for line in file:
                        # Use regular expression to find lines starting with #include
                        match = re.match(r'#include\s+<(.+?)>', line)
                        if match:
                            header = match.group(1)
                            headers_set.add(header)
            except FileNotFoundError:
                print(f"File not found: {file_path}")

    return headers_set





# ! Test the function - headers_hunter():
# directory_to_search = '/home/sam/c_files/'
c_cpp_files_set = find_c_cpp_files(directory_to_search)
headers_set = headers_hunter(c_cpp_files_set)

#Print the set of headers
for header in headers_set:
     print(header)


"""  
   C++ preprocessor
   $ `gcc -print-prog-name=cc1plus` -v
"""
#  /usr/include/c++/11
#  /usr/include/x86_64-linux-gnu/c++/11
#  /usr/include/c++/11/backward
#  /usr/lib/gcc/x86_64-linux-gnu/11/include
#  /usr/local/include
#  /usr/include


"""
    C preprocessor
    $ `gcc -print-prog-name=cpp` -v
"""
# /usr/lib/gcc/x86_64-linux-gnu/11/include
# /usr/lib/gcc/x86_64-linux-gnu/11/include
# /usr/local/include# /usr/lib/gcc/x86_64-linux-gnu/11/include
# /usr/include/x86_64-linux-gnu
# /usr/include

# Take a list of directories from the preprocessors and return a list of existing directories


def check_directories_exist(directories):
    existing_directories = set()

    for directory in directories:
        if os.path.exists(directory):
            existing_directories.add(directory)

    return existing_directories


# ! Testing check_directories_exist()
directories_to_check = []
output = subprocess.check_output("locate " + header, shell=True, text=True)
fmt_output = output.strip().splitlines()

existing_dirs = check_directories_exist(directories_to_check)
print(existing_dirs)

#####################################################
#  find all the headers Locations on system         #
#  while ignoring  the bits/                        #
#####################################################

# take a set of headers and return a set of all the headers locations


def headers_locations(set_of_headers: set, seraching_lctn: set) -> set:
    # List of directories to search for stdio.h
    # TODO: Directories are system specific, need to find a way to make it generic.
    # ! DONE - check_directories_exist()
    # directories_to_search = [
    #     "/usr/lib/gcc/x86_64-linux-gnu/11/include",
    #     "/usr/local/include",
    #     "/usr/include",
    #     "/usr/include/x86_64-linux-gnu"
    # ]

    # "Never include <bits/xxxxx.h> directly; use <xxxx.h> instead."
    headers_lctn = set()
    for header in set_of_headers:

        command = ["find"] + list(seraching_lctn) + \
            ["-type", "f", "-name", header]
        #command = subprocess.check_output("locate " + header, shell=True, text=True)
        # Run the command and capture only stdout, ignore bs
        try:
            output = subprocess.check_output(
                command, stderr=subprocess.DEVNULL, text=True)

            if output.strip():
                # Output contains the list of stdio.h files found
                for lines in output.split('\n'):
                    if "bits" not in lines:
                        headers_lctn.add(lines)
            else:
                print(f"No {header} files found in the specified directories.")
        except subprocess.CalledProcessError as e:
            # If the command returns a non-zero exit code, you can handle it here
            print("Error:", e)

        # ! Print the set of headers locations
        # for header in headers_lctn:
        #     print(header)

    return headers_lctn


print("Header locations (used and not used by preprocessor \n")
pprint.pprint(headers_locations(
     headers_set, check_directories_exist(directories_to_check)))

# TODO: Check if the header is used by the preprocessor

# TODO: preprocess the c and cpp files and saves them to a file to be used to check


def preprocess_and_save(files):

    for file in files:
        if file.endswith(('.c')):
            output_file_c = file.replace('.c', '_preprocessed.c')
            cmd = f'gcc -E {file} -o {output_file_c}'
            try:
                subprocess.check_output(cmd, shell=True)
                print(
                    f'Successfully preprocessed {file} and saved to {output_file_c}')
            except subprocess.CalledProcessError as e:
                print(f'Error preprocessing {file}: {e}')

        elif file.endswith(('.cpp')):
            output_file_cpp = file.replace('.cpp', '_preprocessed.cpp')
            # Execute the gcc -E command to preprocess the file
            cmd = f'g++ -E {file} -o {output_file_cpp}'
            try:
                subprocess.check_output(cmd, shell=True)
                print(
                    f'Successfully preprocessed {file} and saved to {output_file_cpp}')
            except subprocess.CalledProcessError as e:
                print(f'Error preprocessing {file}: {e}')


# List of C/C++ files to preprocess
file_set = find_c_cpp_files(directory_to_search)

# Call the preprocess_and_save function with the set of files
# print(file_set)


# ! check wich header locations  used by the preprocessor or not
def headers_used_by_the_preprocessor(directory, headers_lctn_list):
    preprocess_and_save(file_set)
    matching_files = set()

    for root, _, files in os.walk(directory):
        for filename in files:
            if filename.endswith(('_preprocessed.c', '_preprocessed.cpp')):
                file_path = os.path.join(root, filename)
                with open(file_path, 'r') as file:
                    file_contents = file.read()
                    for header_lctn in headers_lctn_list:
                        if header_lctn in file_contents:
                            matching_files.add(header_lctn)

    return matching_files


# TODO: Recursively for each header file if it contain other header files that had not been checked yet, and check them
# check a file for syscalls
#syscall_directory = ()
def check_file_for_syscalls(file_path, syscall_1_2):
    try:
        with open(file_path, 'r') as file:
            content = file.read()
            for syscall in syscall_1_2:
                if syscall in content:
                    syscalls_currenly_inuse.add(syscall)
                    #syscall_directory
                    # print(f"Found {syscall} in {file_path}")
    except FileNotFoundError:
        print(f"File not found: {file_path}")
    except Exception as e:
        print(f"Error reading {file_path}: {str(e)}")


# ! Test the function - check_files_for_string():
headers_lctns_used = headers_used_by_the_preprocessor(directory_to_search, headers_locations(
    headers_set, check_directories_exist(directories_to_check)))

print("Header locations used by the preprocessor \n")
pprint.pprint(headers_lctns_used)


#pprint.pprint(headers_hunter(["/usr/include/asm-generic/errno.h"]))





for header_lctn in headers_lctns_used:
     check_file_for_syscalls(header_lctn, syscalls_1_2)
print("syscalls used by the preprocessor \n")
pprint.pprint(syscalls_currenly_inuse)


