import pprint
import re

from collections import deque
import subprocess

checked_headers = set()

to_be_checked = set()
# List of specified directories
specified_directories = [
    "/usr/include/c++/11",
    "/usr/include/x86_64-linux-gnu/c++/11",
    "/usr/include/c++/11/backward",
    "/usr/lib/gcc/x86_64-linux-gnu/11/include",
    "/usr/local/include",
    "/usr/include",
    "/usr/lib/gcc/x86_64-linux-gnu/11/include",
    "/usr/lib/gcc/x86_64-linux-gnu/11/include",
    "/usr/local/include",
    "/usr/include/x86_64-linux-gnu",
    "/usr/include"
]
import os
def is_in_specified_directory(file_path):
    # Check if the file_path is in one of the specified directories
    for directory in specified_directories:
        common_prefix = os.path.commonprefix([file_path, directory])
        if common_prefix == directory:
            return True
    return False


to_be_checked = []

def headers_hunter_mock(list_of_c_cpp_files):
  
    for file_path in list_of_c_cpp_files:
        if file_path not in checked_headers:
            if len(file_path) > 2:
                checked_headers.add(file_path)
                try:
                    if os.path.isfile(file_path):
                        with open(file_path, 'r') as file:
                            for line in file:
                                # Use regular expression to find lines starting with #include
                                match = re.match(r'#include\s+<(.+?)>', line)
                                if match:
                                    header = match.group(1)
                                    if header not in checked_headers:
                                        output = subprocess.check_output("locate " + header, shell=True, text=True)
                                        fmt_output = output.strip().splitlines()
                                        if len(fmt_output) == 1:
                                            if is_in_specified_directory(fmt_output[0]):
                                                to_be_checked.append(fmt_output[0])
                                        else:
                                            for i in fmt_output:
                                                if is_in_specified_directory(i):
                                                    to_be_checked.append(i)
                    
                except FileNotFoundError:
                    print(f"File not found: {file_path}")
    

sets = set()
my_list = ['/usr/include/asm-generic/errno.h',
 '/usr/include/c++/11/algorithm',
 '/usr/include/c++/11/cmath',
 '/usr/include/c++/11/cstdlib',
 '/usr/include/c++/11/ctime',
 '/usr/include/c++/11/fstream',
 '/usr/include/c++/11/iomanip',
 '/usr/include/c++/11/iostream',
 '/usr/include/c++/11/stdexcept',
 '/usr/include/c++/11/string',
 '/usr/include/c++/11/vector',
 '/usr/include/errno.h',
 '/usr/include/fcntl.h',
 '/usr/include/linux/errno.h',
 '/usr/include/stdio.h',
 '/usr/include/stdlib.h',
 '/usr/include/termios.h',
 '/usr/include/unistd.h',
 '/usr/include/x86_64-linux-gnu/asm/errno.h'
]
headers_hunter_mock( my_list)
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))
headers_hunter_mock( set(to_be_checked))


print("to_be_checked")
pprint.pprint(set(to_be_checked))
