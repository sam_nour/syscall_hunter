import re
import pprint
import os
import helper_methods
SYSCALLS_EXTRACTED_FROM_C_CPP_FILES = set() 
FOUND_SYSCALLS = set()
LOCATION_LOCAL = "/home/nos1abt/testing/c_files"
LOCATION_OF_FILES_TO_EXAMINE =  "/home/nos1abt/Downloads/ptpd"
#LOCATION_OF_FILES_TO_EXAMINE =  "/home/nos1abt/repo_cloner"




def x86_64_syscalls():
    syscalls_1_2 = set()
    with open('strace_calls.txt', 'r') as f:
        data = f.read()
    lines = data.split('\n')
    counter = 0
    pattern = r'\d+\s+(\w+)\s+'
    for line in lines:
        counter += 1
        match = re.search(pattern, line)
        if match:
            syscalls_1_2.add(match.group(1))
        with open("debugging/x86_64_syscalls.txt", 'w') as outfile:
            for syscall in syscalls_1_2:
                outfile.write(syscall + '\n')
    return syscalls_1_2

def find_c_cpp_files(directory) -> set:
    c_cpp_files = set()
    for root, _, files in os.walk(directory):
        for filename in files:
            if filename.endswith(('.c', '.cpp')):
                file_path = os.path.join(root, filename)
                c_cpp_files.add(os.path.abspath(file_path))

    return c_cpp_files

def find_words_in_file(file_path, words_to_find):
    with open(file_path, 'r') as file:
        for line in file:
            words_in_line = line.split()  # Split the line into words
            for word in words_in_line:
                # Check if the word is in the set of words to find
                if word in words_to_find:
                    SYSCALLS_EXTRACTED_FROM_C_CPP_FILES.add(word)
    return SYSCALLS_EXTRACTED_FROM_C_CPP_FILES

def syscall_hunter(syscalls: set(), c_cpp_file):
    
    with open(c_cpp_file, 'r') as file:
        code = file.read()
    pattern = r'\b(' + '|'.join(syscalls) + r')\s*\('
    matches = re.findall(pattern, code)
    FOUND_SYSCALLS.update(matches)
    # for syscall in found_syscalls:
    #     print(syscall)
    return FOUND_SYSCALLS

def header_hunter(list_of_c_cpp_files):
    headers_set = set()

    for file_path in list_of_c_cpp_files:
        if len(file_path) > 2:
            try:
                with open(file_path, 'r') as file:
                    for line in file:
                        # Use regular expression to find lines starting with #include
                        match = re.match(r'#include\s+<(.+?)>', line)
                        if match:
                            header = match.group(1)
                            headers_set.add(header)
            except FileNotFoundError:
                print(f"File not found: {file_path}")

    return headers_set



def searching_for_syscalls_in_headers(syscalls_set, headers_set, header_directory):
    syscall_to_headers_mapping = {}  # Initialize an empty dictionary to store the mapping
    
    for header in headers_set:  
        if header.endswith(".h"):
            header_path = os.path.join(header_directory, header)
            if os.path.exists(header_path):
                with open(header_path, 'r') as header_file:
                    header_content = header_file.read()
                
                for syscall in syscalls_set:
                    if syscall in header_content:
                        if syscall not in syscall_to_headers_mapping:
                            syscall_to_headers_mapping[syscall] = set()
                        syscall_to_headers_mapping[syscall].add(header)
            else:
                print(f"Header file not found: {header_path}")
    return syscall_to_headers_mapping


if __name__=="__main__":
    # ! x86_64_syscalls()
    print("\n##################################################")
    print("#            all availabe syscalls(1)-(2)          #")
    print("##################################################\n")
    all_x86_64_syscalls = x86_64_syscalls()
    pprint.pprint(all_x86_64_syscalls)

    # ! find_c_cpp_files(),
    print("\n##################################################")
    print("#  all the files that end with either .c or .cpp #")
    print("##################################################\n")
    all_c_cpp_files = find_c_cpp_files(LOCATION_OF_FILES_TO_EXAMINE)
    pprint.pprint(all_c_cpp_files)

    for files in all_c_cpp_files:
        syscall_hunter(all_x86_64_syscalls,files)
        print("\n####################################################################################################")
        print(f"#    System calls found in the  {files} are        ")
        print("####################################################################################################\n")
        pprint.pprint(FOUND_SYSCALLS)
        FOUND_SYSCALLS.clear()

    # ! header_hunter()
    print("\n####################################################################################################")
    print(f"#    Headers Found in the  {all_c_cpp_files}       ")
    print("####################################################################################################\n")
    pprint.pprint(header_hunter(all_c_cpp_files))

    print("\n####################################################################################################")
    print(f"#   Locations of  headers in the konan compiler     ")
    print("####################################################################################################\n")
    res = helper_methods.execute_locate_cmd("locate stdio.h")
    for i in res:
        print( i)


    print("\n####################################################################################################")
    print(f"#   Which syscall in Which header    ")
    print("####################################################################################################\n")
    pprint.pprint(searching_for_syscalls_in_headers(all_x86_64_syscalls,header_hunter(all_c_cpp_files),"/opt/rta-vrte-linux/3.8.0/sysroots/aarch64-boschdenso-linux/usr/include/"))