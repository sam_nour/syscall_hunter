##############
Delivery Notes
##############

******************************
1.1.8 release
******************************

Minor
=====

* Adding COPYRIGHT file to conan package
* Modifying Disclaimer in release documentation


******************************
1.1.5 release (AUTOSAR R21-11)
******************************

Minor
=====

* Improvement of code quality and report

*******************************
R22.11 release
*******************************

- New features:

   None

- Defect fix:

   - The population of Seconds, SecondsHi and Nanoseconds fields into PTP frame's PreciseOriginTime field, had alignment issues. 
     Due to this, the time interpreted by the Consumer was incorrect. This issue is now fixed.

- Minor:

   - Cleanup with respect to API names and variable names

- Know Issues:

   - When executing PTPd on QNX 7.1 based target, it seen that the application crashes due to SIGTERM.
     This is because of 64-bit address translation issue in QNX microkernel. There is a workaround to get past this.
     Just execute the application with prefix "on -ad" followed by usual command to run PTPd.


*******************************
R22.08 release
*******************************

- New features:

   - PTPd can now be executed in the Provider mode
   - SubTLV support (Userdata) is now introduced. It is available for both Provider and Consumer mode

- Defect fix:

   - None

- Minor:

   - To distinguish PTPd Bosch implementation from the Open Source implementation,the binary is now renamed to rb-aptpd, where 'a' stands for 'adapted'
   - The Bosch adapted files have been renamed to follow above naming conventions
   - Classic Autosar terms are replaced with Adaptive Autosar equivalents

- Know Issues:

   - When executing PTPd on QNX 7.1 based target, it seen that the application crashes due to SIGTERM.
     This is because of 64-bit address translation issue in QNX microkernel. There is a workaround to get past this.
     Just execute the application with prefix "on -ad" followed by ususal command to run PTPd.
