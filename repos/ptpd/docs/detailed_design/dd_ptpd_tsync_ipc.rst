PTPd IPC Detailed Design
==============================

*******************************
PTPd IPC Required Interfaces
*******************************

The PTPd is a deamon that receives/sends the PTP packets over Ethernet - Time Synchronization over Ethernet.
The PTP messages are forwarded to ara::tsync via tsync-ptp-lib. In order to be able to send these time messages, tsync-ptp-lib is providing following APIs:

   +-------------------------------------------+-----------------------------------------------------+
   | **API Name**                              | **Description**                                     |
   +-------------------------------------------+-----------------------------------------------------+
   | TSync_Open                                | API to perform basic initialization of the          |
   |                                           | tsync-ptp-lib.                                      |
   +-------------------------------------------+-----------------------------------------------------+
   | TSync_Close                               | API to shutdown the tsync-ptp-lib.                  |
   +-------------------------------------------+-----------------------------------------------------+
   | TSync_OpenTimebase                        | API to open a timebase for reading and writing.     |
   |                                           | (For every configured time base)                    |
   +-------------------------------------------+-----------------------------------------------------+
   | TSync_CloseTimebase                       | API to close and invalidate the given timebase      |
   |                                           | handle.                                             |
   +-------------------------------------------+-----------------------------------------------------+
   | TSync_BusSetGlobalTime                    | API to allow the Time Base Provider Modules to      |
   |                                           | forward a new Global Time tuple (i.e., the Received |
   |                                           | Time Tuple) to the tsync-ptp-lib                    |
   +-------------------------------------------+-----------------------------------------------------+
   | TSync_BusGetGlobalTime                    | API to allow the Time Base Provider Modules to      |
   |                                           | read new Global Time tuple from tsync-ptp-lib       |
   |                                           | and send over Ethenet.                              |
   +-------------------------------------------+-----------------------------------------------------+
   | TSync_GetCurrentVirtualLocalTime          | API to read Virtual Local Time of the referenced    |
   |                                           | Time Base.                                          |
   +-------------------------------------------+-----------------------------------------------------+
   | TSync_RegisterTransmitGlobalTimeCallback  | API to register trigger transmit callback.          |
   |                                           | tsync-ptp-lib will call this callback to trigger    |
   |                                           | immediate time transmission.                        |
   +-------------------------------------------+-----------------------------------------------------+
