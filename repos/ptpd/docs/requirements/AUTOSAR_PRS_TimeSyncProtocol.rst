.. _PRS_TimeSyncProtocol:

###########################################
Time Synchronization Protocol Specification
###########################################

   ========================== ===========================================
   Document Title             Time Synchronization Protocol Specification
   Document Owner             AUTOSAR
   Document Responsibility    AUTOSAR
   Document Identification No 897
   Document Classification    
   Document Status            published
   Part of AUTOSAR Standard   Foundation
   Part of Standard Release   R20-11
   ========================== ===========================================
   
   +---------------------------------------------------------------------------------------------------------+
   | Document Change History                                                                                 |
   +------------+---------+----------------------------+-----------------------------------------------------+
   | Date       | Release | Changed by                 | Description                                         |
   +------------+---------+----------------------------+-----------------------------------------------------+
   | 2020-11-30 | R20-11  | AUTOSAR Release Management | - Added Sequence Counter handling                   |
   |            |         |                            | - New configuration parameters                      |
   +------------+---------+----------------------------+-----------------------------------------------------+
   | 2019-11-28 | R19-11  | AUTOSAR Release Management | - Clarified SGW value handling for missing Sub-TLVs |
   |            |         |                            | - Changed Document Status from Final to published   |
   +------------+---------+----------------------------+-----------------------------------------------------+
   | 2019-03-29 | 1.5.1   | AUTOSAR Release Management | - Minor changes                                     |
   +------------+---------+----------------------------+-----------------------------------------------------+
   | 2018-10-31 | 1.5.0   | AUTOSAR Release Management | - Initial release                                   |
   +------------+---------+----------------------------+-----------------------------------------------------+


.. _Disclaimer:

**Disclaimer**

This work (specification and/or software implementation) and the material contained in it, as released by AUTOSAR, is for the purpose of information only. AUTOSAR and the companies that have contributed to it shall not be liable for any use of the work.

The material contained in this work is protected by copyright and other types of intellectual property rights. The commercial exploitation of the material contained in this work requires a license to such intellectual property rights.

This work may be utilized or reproduced without any modification, in any form or by any means, for informational purposes only. For any other purpose, no part of the work may be utilized or reproduced, in any form or by any means, without permission in writing from the publisher.

The work has been developed for automotive applications only. It has neither been developed, nor tested for non-automotive applications.

The word AUTOSAR and the AUTOSAR logo are registered trademarks.


*************************
Introduction and overview
*************************

This protocol specification specifies the format, message sequences and semantics of the AUTOSAR Time synchronization Protocol.

The Time synchronization Protocol handles the distribution of time information over Ethernet. The Ethernet mechanism is based on existing PTP (Precision Time Protocol) mechanisms that are described in standards like IEEE1588 (IEEE Standard for a Precision Clock Synchronization Protocol for Networked Measurement and Control Systems) and IEEE802.1AS (Timing and Synchronization for Time-Sensitive Applications in Bridged Local Area Networks). IEEE802.1AS, also known as gPTP (generalized Precision Time Protocol), can be seen as a profile (or subset) for using IEEE1588. However, neither IEEE1588 nor IEEE802.1AS have been developed considering automotive requirements. Therefore, the Time Synchronization over Ethernet uses the current mechanisms as defined in IEEE802.1AS with specific extensions and/or restrictions. Automotive Ethernet networks deviate from commercial Ethernet networks in terms of the following items:

- Role and functions of ECUs is known and defined a priori
- The network is static, i.e. components like ECUs, switches and characteristics like cable length, don't change during operation or even after switching off and switching on the vehicle. Components of course may be unavailable (due to failure situations or by purpose) but mostly only change when the vehicle is at a service facility.

Therefore, dynamic mechanisms like determining the Global Time Master (denoted as grandmaster in IEEE802.1AS) by the best master clock algorithm (BMCA) during operation are not required. It is also possible to omit the cyclic measurement of link delays on Ethernet links due to the static nature of the automotive network and restrict mechanisms that belonging to dynamic network topology.


Protocol purpose and objectives
===============================

The Time synchronization protocol is used to

- synchronize time bases and the corresponding Ethernet messages
- measure time differences between Ethernet frames


Applicability of the protocol
=============================

The concept is targeted at supporting time-critical and safety-related automotive applications such as airbag systems and braking systems. This doesn't mean that the concept has all that is required by such systems though, but crucial timing-related features that cannot be deferred to implementation are considered.


Constraints and assumptions
---------------------------

This document specifies the AUTOSAR Time Synchronization Protocol. It was created during elaboration of the AUTOSAR Foundation Standard 1.5.0 which took place in parallel to the development of the AUTOSAR Classic Standard 4.4.0. It already reflects all changes implied to TimeSyncOverEthernet by the work which was done for AUTOSAR Classic Platform.


Limitations
-----------

- No support of BMCA protocol, like specified in `DOC_IEEEStandard8021AS30`_
- No support of Announce and Signaling messages, like specified in `DOC_IEEEStandard8021AS30`_.
- The reception of a Pdelay_Req is not taken as a pre-condition to start with the transmission of Sync messages.
- The Rate Correction will be performed by the Time synchronization protocol, which does not require the Pdelay mechanism. For some applications, e.g. for Audio/Video, it might be necessary to use Pdelay based Rate Correction performed by Time synchronization protocol itself, which is optional and not considered by this specification.
- Because of (4), the Time synchronization protocol will not maintain the Ethernet HW clock but may use it as a source for the Virtual Local Time.
- While IEEE 802.1AS states, that IEEE 802.1AS message shall not have a VLAN tag nor a priority tag, the Time synchronization protocol would allow Time Synchronization on VLANs under the condition, that the switch HW supports forwarding of reserved multicast addresses using the range of 01:80:C2:00:00:00 .. 0F
- 'CRC secured' in the context of this document refers to CRC integrity protection mechanism and does not imply that CRC is used as a cybersecurity solution.


Accuracy
--------

Time Master and Time Slave shall work with a Time Base reference clock accuracy as defined in `DOC_IEEEStandard8021AS30`_, ANNEX B.1.2 "Time measurement granularity".


Dependencies
============


Dependencies to other protocol layers
-------------------------------------

There are no dependencies to other protocols.


Dependencies to other standards and norms
-----------------------------------------

The AUTOSAR Time Synchronization protocol is derived from `DOC_IEEEStandard8021AS30`_. For VLAN characteristics refer to `DOC_IEEE8021Q2011`_.


Dependencies to the Application Layer
-------------------------------------

There are no dependencies to the application layer.


*********
Use Cases
*********


.. _table_3a_usecases:
   
   +----------+----------------------+-----------------------------------------------+
   | **ID**   | **Name**             | **Description**                               |
   +==========+======================+===============================================+
   | **0001** | Pdelay measurement   | Measuring of delays between Ethernet messages |
   +----------+----------------------+-----------------------------------------------+
   | **0002** | Time Synchronization | Time synchronization of different time bases. |
   +----------+----------------------+-----------------------------------------------+


*********************
Protocol Requirements
*********************


Requirements Traceability
=========================

   
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Requirement**  | **Description**                                                                                                                                     | **Satisfied by**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
   +==================+=====================================================================================================================================================+==========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================+
   | [_`RS_TS_20047`] | The Timesync over Ethernet module shall trigger Time Base Synchronization transmission                                                              | [:need:`PRS_TS_00016`] [:need:`PRS_TS_00050`] [:need:`PRS_TS_00186`]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | [_`RS_TS_20048`] | The Timesync over Ethernet module shall support IEEE 802.1AS as well as AUTOSAR extensions                                                          | [:need:`PRS_TS_00002`] [:need:`PRS_TS_00003`] [:need:`PRS_TS_00004`] [:need:`PRS_TS_00005`] [:need:`PRS_TS_00011`] [:need:`PRS_TS_00012`] [:need:`PRS_TS_00016`] [:need:`PRS_TS_00018`] [:need:`PRS_TS_00023`] [:need:`PRS_TS_00025`] [:need:`PRS_TS_00028`] [:need:`PRS_TS_00050`] [:need:`PRS_TS_00053`] [:need:`PRS_TS_00054`] [:need:`PRS_TS_00055`] [:need:`PRS_TS_00056`] [:need:`PRS_TS_00057`] [:need:`PRS_TS_00058`] [:need:`PRS_TS_00059`] [:need:`PRS_TS_00060`] [:need:`PRS_TS_00061`] [:need:`PRS_TS_00062`] [:need:`PRS_TS_00063`] [:need:`PRS_TS_00066`]  |
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   |                  |                                                                                                                                                     | [:need:`PRS_TS_00067`] [:need:`PRS_TS_00068`] [:need:`PRS_TS_00069`] [:need:`PRS_TS_00070`] [:need:`PRS_TS_00071`] [:need:`PRS_TS_00072`] [:need:`PRS_TS_00075`] [:need:`PRS_TS_00077`] [:need:`PRS_TS_00079`] [:need:`PRS_TS_00086`] [:need:`PRS_TS_00141`] [:need:`PRS_TS_00142`] [:need:`PRS_TS_00149`] [:need:`PRS_TS_00154`] [:need:`PRS_TS_00163`] [:need:`PRS_TS_00164`] [:need:`PRS_TS_00166`] [:need:`PRS_TS_00167`] [:need:`PRS_TS_00168`] [:need:`PRS_TS_00169`] [:need:`PRS_TS_00170`] [:need:`PRS_TS_00171`] [:need:`PRS_TS_00181`] [:need:`PRS_TS_00206`]  |
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   |                  |                                                                                                                                                     | [:need:`PRS_TS_00207`] [:need:`PRS_TS_00208`] [:need:`PRS_TS_00209`] [:need:`PRS_TS_00210`]                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | [_`RS_TS_20051`] | The Timesync over Ethernet module shall detect and handle errors in synchronization protocol / communication                                        | [:need:`PRS_TS_00004`] [:need:`PRS_TS_00025`] [:need:`PRS_TS_00164`] [:need:`PRS_TS_00210`]                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | [_`RS_TS_20052`] | The configuration of the Time Synchronization over Ethernet module shall allow the module to work as a Time Master                                  | [:need:`PRS_TS_00094`]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | [_`RS_TS_20054`] | The Implementation of the Time Synchronization shall evaluate and propagate Time Gateway relevant information                                       | [:need:`PRS_TS_00094`] [:need:`PRS_TS_00156`] [:need:`PRS_TS_00211`] [:need:`PRS_TS_00212`] [:need:`PRS_TS_00213`]                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | [_`RS_TS_20059`] | The Timesync over Ethernet module shall access all communication ports belonging to Time Synchronization                                            | [:need:`PRS_TS_00053`] [:need:`PRS_TS_00054`] [:need:`PRS_TS_00055`] [:need:`PRS_TS_00056`] [:need:`PRS_TS_00057`] [:need:`PRS_TS_00058`] [:need:`PRS_TS_00059`] [:need:`PRS_TS_00060`] [:need:`PRS_TS_00166`] [:need:`PRS_TS_00167`] [:need:`PRS_TS_00168`] [:need:`PRS_TS_00169`] [:need:`PRS_TS_00170`] [:need:`PRS_TS_00171`] [:need:`PRS_TS_00207`] [:need:`PRS_TS_00208`] [:need:`PRS_TS_00209`]                                                                                                                                                                   |
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | [_`RS_TS_20061`] | The Timesync over Ethernet module shall support means to protect the Time Synchronization protocol                                                  | [:need:`PRS_TS_000104`] [:need:`PRS_TS_00028`] [:need:`PRS_TS_00062`] [:need:`PRS_TS_00063`] [:need:`PRS_TS_00066`] [:need:`PRS_TS_00067`] [:need:`PRS_TS_00068`] [:need:`PRS_TS_00069`] [:need:`PRS_TS_00070`] [:need:`PRS_TS_00071`] [:need:`PRS_TS_00072`] [:need:`PRS_TS_00074`] [:need:`PRS_TS_00075`] [:need:`PRS_TS_00076`] [:need:`PRS_TS_00077`] [:need:`PRS_TS_00078`] [:need:`PRS_TS_00079`] [:need:`PRS_TS_00081`] [:need:`PRS_TS_00082`] [:need:`PRS_TS_00084`] [:need:`PRS_TS_00085`] [:need:`PRS_TS_00086`] [:need:`PRS_TS_00088`] [:need:`PRS_TS_00089`] |
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   |                  |                                                                                                                                                     | [:need:`PRS_TS_00091`] [:need:`PRS_TS_00092`] [:need:`PRS_TS_00093`] [:need:`PRS_TS_00097`] [:need:`PRS_TS_00098`] [:need:`PRS_TS_00099`] [:need:`PRS_TS_00100`] [:need:`PRS_TS_00101`] [:need:`PRS_TS_00102`] [:need:`PRS_TS_00103`] [:need:`PRS_TS_00105`] [:need:`PRS_TS_00106`] [:need:`PRS_TS_00107`] [:need:`PRS_TS_00108`] [:need:`PRS_TS_00109`] [:need:`PRS_TS_00112`] [:need:`PRS_TS_00113`] [:need:`PRS_TS_00114`] [:need:`PRS_TS_00115`] [:need:`PRS_TS_00116`] [:need:`PRS_TS_00117`] [:need:`PRS_TS_00118`] [:need:`PRS_TS_00119`] [:need:`PRS_TS_00120`]  |
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   |                  |                                                                                                                                                     | [:need:`PRS_TS_00157`] [:need:`PRS_TS_00181`] [:need:`PRS_TS_00182`] [:need:`PRS_TS_00183`] [:need:`PRS_TS_00184`] [:need:`PRS_TS_00185`] [:need:`PRS_TS_00187`] [:need:`PRS_TS_00188`] [:need:`PRS_TS_00189`] [:need:`PRS_TS_00190`] [:need:`PRS_TS_00191`] [:need:`PRS_TS_00192`] [:need:`PRS_TS_00193`] [:need:`PRS_TS_00194`] [:need:`PRS_TS_00195`] [:need:`PRS_TS_00196`] [:need:`PRS_TS_00197`] [:need:`PRS_TS_00198`] [:need:`PRS_TS_00199`]                                                                                                                     |
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | [_`RS_TS_20062`] | The Timesync over Ethernet module shall support user specific data within the time measurement and synchronization protocol                         | [:need:`PRS_TS_000104`] [:need:`PRS_TS_00028`] [:need:`PRS_TS_00062`] [:need:`PRS_TS_00063`] [:need:`PRS_TS_00066`] [:need:`PRS_TS_00067`] [:need:`PRS_TS_00068`] [:need:`PRS_TS_00069`] [:need:`PRS_TS_00070`] [:need:`PRS_TS_00071`] [:need:`PRS_TS_00072`] [:need:`PRS_TS_00074`] [:need:`PRS_TS_00075`] [:need:`PRS_TS_00076`] [:need:`PRS_TS_00077`] [:need:`PRS_TS_00078`] [:need:`PRS_TS_00079`] [:need:`PRS_TS_00081`] [:need:`PRS_TS_00082`] [:need:`PRS_TS_00084`] [:need:`PRS_TS_00085`] [:need:`PRS_TS_00086`] [:need:`PRS_TS_00088`] [:need:`PRS_TS_00089`] |
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   |                  |                                                                                                                                                     | [:need:`PRS_TS_00092`] [:need:`PRS_TS_00103`] [:need:`PRS_TS_00105`] [:need:`PRS_TS_00106`] [:need:`PRS_TS_00118`] [:need:`PRS_TS_00119`] [:need:`PRS_TS_00120`] [:need:`PRS_TS_00181`]                                                                                                                                                                                                                                                                                                                                                                                  |
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | [_`RS_TS_20063`] | The Timesync over Ethernet module shall use the Time Synchronization protocol for Synchronized Time Bases to transmit and receive Offset Time Bases | [:need:`PRS_TS_000104`] [:need:`PRS_TS_00092`] [:need:`PRS_TS_00095`] [:need:`PRS_TS_00103`] [:need:`PRS_TS_00105`] [:need:`PRS_TS_00106`] [:need:`PRS_TS_00110`] [:need:`PRS_TS_00117`] [:need:`PRS_TS_00118`] [:need:`PRS_TS_00119`] [:need:`PRS_TS_00120`]                                                                                                                                                                                                                                                                                                            |
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | [_`RS_TS_20066`] | The Timesync over Ethernet module shall support a static (pre)configuration of IEEE 802.1AS Pdelay                                                  | [:need:`PRS_TS_00003`] [:need:`PRS_TS_00011`] [:need:`PRS_TS_00012`] [:need:`PRS_TS_00140`] [:need:`PRS_TS_00141`] [:need:`PRS_TS_00142`] [:need:`PRS_TS_00143`] [:need:`PRS_TS_00149`]                                                                                                                                                                                                                                                                                                                                                                                  |
   +------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+


********************************
Definition of terms and acronyms
********************************


Acronyms and abbreviations
==========================

   
   +-------------------------------------------------------------+--------------------------------------------------------+
   | **Abbreviation / Acronym:**                                 | **Description:**                                       |
   +=============================================================+========================================================+
   |                                       (G)TD                 | (Global) Time Domain                                   |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       (G)TM                 | (Global)Time Master                                    |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                     <Bus>TSyn               | A bus specific Time Synchronization module             |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       AVB                   | Audio Video Bridging                                   |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       BMCA                  | Best Master Clock Algorithm                            |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       CID                   | Company ID (IEEE)                                      |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       CRC                   | Cyclic Redundancy Checksum                             |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       Debounce Time         | Minimum gap between two Tx messages with the same PDU. |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       ETH                   | Ethernet                                               |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       EthTSyn               | Time Synchronization Provider module for Ethernet      |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       Follow_Up             | Time transport message (Follow-Up)                     |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       GM(C)                 | Grand Master (Clock)                                   |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       OFS                   | Offset synchronization                                 |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       Pdelay                | Propagation / path delay as given in IEEE 802.1AS      |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       Pdelay_Req            | Propagation / path delay request message               |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       Pdelay_Resp           | Propagation / path delay response message              |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       Pdelay_Resp_Follow_Up | Propagation / path delay Follow-Up message             |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       PDU                   | Protocol Data Unit                                     |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       PTP                   | Precision Time Protocol                                |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       StbM                  | (Global) Time Domain                                   |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       Timesync              | Time Synchronization                                   |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       Sync                  | Time synchronization message (Sync)                    |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       TG                    | Time Gateway                                           |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       TLV                   | Type, Length, Value field (acc. to IEEE 802.1AS)       |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       TS                    | Time Slave                                             |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       TSD                   | Time Sub-domain                                        |
   +-------------------------------------------------------------+--------------------------------------------------------+
   |                                       VLAN                  | Virtual Local Area Network                             |
   +-------------------------------------------------------------+--------------------------------------------------------+


**********************
Protocol specification
**********************


General
=======

.. sw_req:: PRS_TS_00002
   :id: PRS_TS_00002
   :status: new/changed
   :safety_level: QM
   
   The Time Master and Time Slave shall use the default configuration values as defined by `DOC_IEEEStandard8021AS30`_ (e.g. MAC destination address or Ethernet frame type), if not otherwise specified within this specification.
   
   (`RS_TS_20048`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00002
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00005
   :id: PRS_TS_00005
   :status: new/changed
   :safety_level: QM
   
   The Time Master and Time Slave shall start their protocol state machines without Announce message recognition.
   
   (`RS_TS_20048`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00005
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00206
   :id: PRS_TS_00206
   :status: new/changed
   :safety_level: QM
   
   All messages belonging to the IEEE Rapid Spanning Tree Protocol (\ ``PortAnnounceReceive, PortAnnounceInformation, PortRoleSelection, PortAnnounceTransmit``\ ) shall be ignored on the receiver side.
   
   (`RS_TS_20048`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00206
      
      <Verification criteria text>
   
**Note:** AUTOSAR implementations shall not send those messages.


VLAN Support
============

.. sw_req:: PRS_TS_00163
   :id: PRS_TS_00163
   :status: new/changed
   :safety_level: QM
   
   If ``FramePrio`` exists, a frame format with priority and VLAN tags shall be used. Otherwise a frame format without priority and VLAN tags shall be used.
   
   (`RS_TS_20048`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00163
      
      <Verification criteria text>
   

Message format
==============

Some message extensions to the `DOC_IEEEStandard8021AS30`_ are required. This is accomplished by a new AUTOSAR specific *TLV*, which is using a new IEEE CID (\ ``0x1A75FB``\ ) belonging to AUTOSAR only. An IEEE 802.1AS *TLV* is only available for the ``message-type Announce`` (not considered by this specification) and ``Follow_Up`` (extended by this specification). The ``organizationId`` of the new *TLV* identifies the AUTOSAR *TLV*, which is succeeding the IEEE 802.1AS *TLV*.

The AUTOSAR *TLV* contains *Sub-TLVs* which always consist of a Type, a Length and a data area.

The usage of the *CRC* is optional. To ensure a great variability between several time observing units, the configuration decides of how to handle the *CRC* of a secured *Sub-TLV*. If the receiver does not support the *CRC* calculation, it might be possible, that a receiver just uses the given values, without evaluating the *CRC* itself.

If the *CRC* option is used, one side effect must be considered. Due to the fact, that ``Pdelay`` messages do not contain any *TLV*, a *CRC* protection of the related timestamps is not possible. If applications using a *CRC* for ``Follow_Up`` together with a non-static ``Pdelay``, unprotected ``Pdelay`` time values have to be mixed with protected ``Follow_Up`` time values, while calculating the value of the corresponding Time Base.

.. sw_req:: PRS_TS_00028
   :id: PRS_TS_00028
   :status: new/changed
   :safety_level: QM
   
   The message format, etc. shall be derived from `DOC_IEEEStandard8021AS30`_ chapter 10. Media-independent layer specification and chapter 11. Media-dependent layer specification for full-duplex, point-to-point links, if not otherwise specified.
   
   (`RS_TS_20048`_, `RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00028
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00181
   :id: PRS_TS_00181
   :status: new/changed
   :safety_level: QM
   
   The byte order for multibyte values is Big Endian, which is equal to the byte order defined by `DOC_IEEEStandard8021AS30`_.
   
   (`RS_TS_20048`_, `RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00181
      
      <Verification criteria text>
   

Header format
-------------


Sync and Follow_Up acc. to IEEE 802.1AS
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. sw_req:: PRS_TS_00061
   :id: PRS_TS_00061
   :status: new/changed
   :safety_level: QM
   
   If ``MessageCompliance`` is set to TRUE, ``Sync`` and ``Follow_Up`` format shall be supported acc. to `DOC_IEEEStandard8021AS30`_.
   
   (`RS_TS_20048`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00061
      
      <Verification criteria text>
   
**Note:** This implies one Time Domain (0).

The table below gives an overview, how an `DOC_IEEEStandard8021AS30`_ conformant ``Sync`` looks like.


.. _table_3a_usecases11:
   
   +-----------------------------------------------------------------------------------------------------------------------------------------------+
   | **Sync Message Header [IEEE 802.1AS]**                                                                                                        |
   +===========================+==================+============+============+======================================================================+
   | **High Nibble**           | **Low Nibble**   | **Octets** | **Offset** | **Value**                                                            |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **transportSpecific**     | **message-type** | 1          | 0          | ``0x10``                                                             |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **reserved**              | **versionPTP**   | 1          | 1          | `` 2``                                                               |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **length of the message** |                  | 2          | 2          | ``44``                                                               |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **domainNumber**          |                  | 1          | 4          | ``(UInteger8) domainNumber = 0``                                     |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **reserved**              |                  | 1          | 5          | `` 0``                                                               |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **flags**                 |                  | 2          | 6          | ``Octet 0: 0x02, Octet 1: 0x08``                                     |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **correctionField**       |                  | 8          | 8          | `` 0..281474976710655ns [1ns = 2^16 = 0x0000 0000 0001 0000]``       |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **reserved**              |                  | 4          | 16         | ``0``                                                                |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **sourcePortIdentity**    |                  | 10         | 20         | ``(PortIdentity) portIdentity`` from origin Time Aware End Station   |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **sequenceId**            |                  | 2          | 30         | ``(UInteger16) SyncSequenceId =(UInteger16) (prevSyncSequenceId+1)`` |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **control**               |                  | 1          | 32         | ``0``                                                                |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **logMessageInterval**    |                  | 1          | 33         | ``(Integer8) currentLogSyncInterval``                                |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **Sync Message Fields [IEEE 802.1AS]**                                                                                                        |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **High Nibble**           | **Low Nibble**   | **Octets** | **Offset** | **Value**                                                            |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **PTP Message Header**    |                  | 34         | 0          | [refer ``Sync`` Message Header]                                      |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+
   | **reserved**              |                  | 10         | 34         | ``0``                                                                |
   +---------------------------+------------------+------------+------------+----------------------------------------------------------------------+

The table below gives an overview, how an `DOC_IEEEStandard8021AS30`_ conformant ``Follow_Up`` looks like.

**Follow_Up Message Header [IEEE 802.1AS]**


.. _table_3a_usecases12:
   
   +--------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Follow_Up Message Header [IEEE 802.1AS]**                                                                                                      |
   +================================+==================+============+============+====================================================================+
   | **High Nibble**                | **Low Nibble**   | **Octets** | **Offset** | **Value**                                                          |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **transportSpecific**          | **message-type** | 1          | 0          | ``0x18``                                                           |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **reserved**                   | **versionPTP**   | 1          | 1          | `` 0x02``                                                          |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **length of the message**      |                  | 2          | 2          | ``76``                                                             |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **domainNumber**               |                  | 1          | 4          | ``(UInteger8) domainNumber = 0``                                   |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **reserved**                   |                  | 1          | 5          | `` 0``                                                             |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **flags**                      |                  | 2          | 6          | ``Octet 0: 0x00, Octet 1: 0x08``                                   |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **correctionField**            |                  | 8          | 8          | `` 0..281474976710655ns [1ns = 2^16 = 0x0000 0000 0001 0000]``     |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **reserved**                   |                  | 4          | 16         | ``0``                                                              |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **sourcePortIdentity**         |                  | 10         | 20         | ``(PortIdentity) portIdentity`` from origin Time Aware End Station |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **sequenceId**                 |                  | 2          | 30         | ``UInteger16) SyncSequenceId``                                     |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **control**                    |                  | 1          | 32         | ``2``                                                              |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **logMessageInterval**         |                  | 1          | 33         | ``(Integer8) currentLogSyncInterval``                              |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **Follow_Up Message Fields [IEEE 802.1AS]**                                                                                                      |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **High Nibble**                | **Low Nibble**   | **Octets** | **Offset** | **Value**                                                          |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **PTP Message Header**         |                  | 34         | 0          | [refer ``Follow_Up`` Message Header]                               |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **preciseOriginTimestamp**     |                  | 10         | 34         | (Timestamp) preciseOriginTimestamp                                 |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **Follow_Up information TLV**  |                  | 32         | 44         | refer ``Follow_Up`` information TLV                                |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **Follow_Up information TLV [IEEE 802.1AS]**                                                                                                     |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **High Nibble**                | **Low Nibble**   | **Octets** | **Offset** | **Value**                                                          |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **tlvType**                    |                  | 2          | 0          | 3                                                                  |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **lengthField**                |                  | 2          | 2          | 28                                                                 |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **organizationId**             |                  | 3          | 4          | 0x0080c2                                                           |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **organizationSubType**        |                  | 3          | 7          | 1                                                                  |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **cumulativeScaledRateOffset** |                  | 4          | 10         | ``(Integer32) ((RateRatio-1)*2^41)``                               |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **gmTimeBaseIndicator**        |                  | 2          | 14         | 0                                                                  |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **lastGmPhaseChange**          |                  | 12         | 16         | 0                                                                  |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+
   | **scaledLastGmFreqChange**     |                  | 4          | 28         | 0                                                                  |
   +--------------------------------+------------------+------------+------------+--------------------------------------------------------------------+


Sync and Follow_Up acc. to AUTOSAR
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. sw_req:: PRS_TS_00062
   :id: PRS_TS_00062
   :status: new/changed
   :safety_level: QM
   
   If ``MessageCompliance`` is set to ``FALSE``, the ``Sync`` and ``Follow_Up`` format shall be supported acc. to: ``Follow_Up Message Header [AUTOSAR]``\ and ``Sync Message Header [AUTOSAR]``\ depending on configuration.
   
   (`RS_TS_20048`_, `RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00062
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00063
   :id: PRS_TS_00063
   :status: new/changed
   :safety_level: QM
   
   If ``MessageCompliance`` is set to ``FALSE``, the ``Follow_Up`` shall contain an AUTOSAR *TLV*, depending on configuration.
   
   (`RS_TS_20048`_, `RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00063
      
      <Verification criteria text>
   
**Message Header [AUTOSAR]Sync Message Header [AUTOSAR]**


.. _prsts00064:
   
   +------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Sync Message Header [AUTOSAR]**                                                                                                              |
   +===========================+==================+============+============+=======================================================================+
   | **High Nibble**           | **Low Nibble**   | **Octets** | **Offset** | **Value**                                                             |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **transportSpecific**     | **message-type** | 1          | 0          | ``0x10``                                                              |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **reserved**              | **versionPTP**   | 1          | 1          | `` 2``                                                                |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **length of the message** |                  | 2          | 2          | ``44``                                                                |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **domainNumber**          |                  | 1          | 4          | ``(UInteger8) domainNumber = 0..15``                                  |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **reserved**              |                  | 1          | 5          | `` 0``                                                                |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **flags**                 |                  | 2          | 6          | ``Octet 0: 0x02``,                                                    |
   |                           |                  |            |            |                                                                       |
   |                           |                  |            |            | ``Octet 1: 0x08``                                                     |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **correctionField**       |                  | 8          | 8          | `` 0..281474976710655ns [1ns = 2^16 = 0x0000 0000 0001 0000]``        |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **reserved**              |                  | 4          | 16         | ``0``                                                                 |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **sourcePortIdentity**    |                  | 10         | 20         | ``(PortIdentity) portIdentity`` from origin Time Aware End Station    |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **sequenceId**            |                  | 2          | 30         | ``(UInteger16) SyncSequenceId = (UInteger16) (prevSyncSequenceId+1)`` |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **control**               |                  | 1          | 32         | ``0``                                                                 |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **logMessageInterval**    |                  | 1          | 33         | ``(Integer8) currentLogSyncInterval``                                 |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **Sync Message Fields [AUTOSAR]**                                                                                                              |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **High Nibble**           | **Low Nibble**   | **Octets** | **Offset** | **Value**                                                             |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **PTP Message Header**    |                  | 34         | 0          | [refer ``Sync`` Message Header]                                       |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+
   | **reserved**              |                  | 10         | 34         | ``0``                                                                 |
   +---------------------------+------------------+------------+------------+-----------------------------------------------------------------------+

   
   +-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Follow_Up Message Header [AUTOSAR]Follow_Up Message Header [AUTOSAR]**                                                                                                                                                                                                                                                                  |
   +================================+==================+======================================+============+===================================================================================================================================================================================================================================+
   | **High Nibble**                | **Low Nibble**   | **Octets**                           | **Offset** | **Value**                                                                                                                                                                                                                         |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **transportSpecific**          | **message-type** | 1                                    | 0          | ``0x18``                                                                                                                                                                                                                          |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **reserved**                   | **versionPTP**   | 1                                    | 1          | `` 0x02``                                                                                                                                                                                                                         |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **length of the message**      |                  | 2                                    | 2          | ``76+10+Sum``\ (\ ``Sub-TLVs``\ )                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **domainNumber**               |                  | 1                                    | 4          | ``(UInteger8) domainNumber = 0..15``                                                                                                                                                                                              |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **reserved**                   |                  | 1                                    | 5          | `` 0``                                                                                                                                                                                                                            |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **flags**                      |                  | 2                                    | 6          | ``Octet 0: 0x00, Octet 1: 0x08``                                                                                                                                                                                                  |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **correctionField**            |                  | 8                                    | 8          | `` 0..281474976710655ns [1ns = 2^16 = 0x0000 0000 0001 0000]``                                                                                                                                                                    |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **reserved**                   |                  | 4                                    | 16         | ``0``                                                                                                                                                                                                                             |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **sourcePortIdentity**         |                  | 10                                   | 20         | ``(PortIdentity) portIdentity`` from origin Time Aware End Station                                                                                                                                                                |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **sequenceId**                 |                  | 2                                    | 30         | ``(UInteger16) SyncSequenceId``                                                                                                                                                                                                   |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **control**                    |                  | 1                                    | 32         | ``2``                                                                                                                                                                                                                             |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **logMessageInterval**         |                  | 1                                    | 33         | ``(Integer8) currentLogSyncInterval``                                                                                                                                                                                             |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Follow_Up Message Fields [AUTOSAR]**                                                                                                                                                                                                                                                                                                    |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **High Nibble**                | **Low Nibble**   | **Octets**                           | **Offset** | **Value**                                                                                                                                                                                                                         |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **PTP Message Header**         |                  | 34                                   | 0          | [refer ``Follow_Up`` Message Header]                                                                                                                                                                                              |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **preciseOriginTimestamp**     |                  | 10                                   | 34         | (Timestamp) preciseOriginTimestamp                                                                                                                                                                                                |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Follow_Up information TLV**  |                  | 32 + 10 + sum(\ ``Sub`` -``TLVs``\ ) | 44         | [refer ``Follow_Up`` information TLV]                                                                                                                                                                                             |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Follow_Up information TLV [IEEE 802.1AS]**                                                                                                                                                                                                                                                                                              |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **High Nibble**                | **Low Nibble**   | **Octets**                           | **Offset** | **Value**                                                                                                                                                                                                                         |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **tlvType**                    |                  | 2                                    | 0          | 3                                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **length of the field**        |                  | 2                                    | 2          | 28                                                                                                                                                                                                                                |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **organizationId**             |                  | 3                                    | 4          | ``0x0080C2`` [ ``IEEE 802.1AS``]                                                                                                                                                                                                  |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **organizationSubType**        |                  | 3                                    | 7          | 1                                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **cumulativeScaledRateOffset** |                  | 4                                    | 10         | (Integer32)((RateRatio\ ``-``\ 1)\ ``*``\ 2\ ``^``\ 41)                                                                                                                                                                           |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **gmTimeBaseIndicator**        |                  | 2                                    | 14         | 0                                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **lastGmPhaseChange**          |                  | 12                                   | 16         | 0                                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **scaledLastGmFreqChange**     |                  | 4                                    | 28         | 0                                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Follow_Up information TLV [AUTOSAR]**                                                                                                                                                                                                                                                                                                   |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **High Nibble**                | **Low Nibble**   | **Octets**                           | **Offset** | **Value**                                                                                                                                                                                                                         |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **AUTOSAR TLV Header**                                                                                                                                                                                                                                                                                                                    |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **tlvType**                    |                  | 2                                    | 0          | 3                                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **length of the field**        |                  | 2                                    | 0          | 6 + Sum(\ *Sub-TLVs*\ )                                                                                                                                                                                                           |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **organizationId**             |                  | 3                                    | 4          | 0x1A75FB [AUTOSAR]                                                                                                                                                                                                                |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **organizationSubType**        |                  | 3                                    | 7          | 0x605676 [BCD coded GlobalTimeEthTSyn]                                                                                                                                                                                            |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **AUTOSAR TLV Sub-TLV: Time Secured**                                                                                                                                                                                                                                                                                                     |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **High Nibble**                | **Low Nibble**   | **Octets**                           | **Offset** | **Value**                                                                                                                                                                                                                         |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Type**                       |                  | 1                                    | 0          | 0x28 [Time secured]                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Length**                     |                  | 1                                    | 1          | 3                                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **CRC_Time_Flags**             |                  | 1                                    | 2          | BitMask 0x01 [length of the message]                                                                                                                                                                                              |
   |                                |                  |                                      |            |                                                                                                                                                                                                                                   |
   |                                |                  |                                      |            | BitMask 0x02 [domainNumber]                                                                                                                                                                                                       |
   |                                |                  |                                      |            |                                                                                                                                                                                                                                   |
   |                                |                  |                                      |            | BitMask 0x04 [correctionField]                                                                                                                                                                                                    |
   |                                |                  |                                      |            |                                                                                                                                                                                                                                   |
   |                                |                  |                                      |            | BitMask 0x08 [sourcePortIdentity]                                                                                                                                                                                                 |
   |                                |                  |                                      |            |                                                                                                                                                                                                                                   |
   |                                |                  |                                      |            | BitMask 0x10 [sequenceId]                                                                                                                                                                                                         |
   |                                |                  |                                      |            |                                                                                                                                                                                                                                   |
   |                                |                  |                                      |            | BitMask 0x20 [preciseOriginTimestamp]                                                                                                                                                                                             |
   |                                |                  |                                      |            |                                                                                                                                                                                                                                   |
   |                                |                  |                                      |            | BitMask 0x40 [reserved]                                                                                                                                                                                                           |
   |                                |                  |                                      |            |                                                                                                                                                                                                                                   |
   |                                |                  |                                      |            | BitMask 0x80 [reserved]                                                                                                                                                                                                           |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **CRC_Time_0**                 |                  | 1                                    | 3          | 0..255                                                                                                                                                                                                                            |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **CRC_Time_1**                 |                  | 1                                    | 4          | 0..255                                                                                                                                                                                                                            |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **AUTOSAR TLV Sub-TLV: Status Secured**                                                                                                                                                                                                                                                                                                   |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **High Nibble**                | **Low Nibble**   | **Octets**                           | **Offset** | **Value**                                                                                                                                                                                                                         |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Type**                       |                  | 1                                    | 0          | 0x50[Status secured]                                                                                                                                                                                                              |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Length**                     |                  | 1                                    | 1          | 2                                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Status**                     |                  | 1                                    | 2          | BitMask 0x01 [SGW with SyncToGTM = 0 SyncToSubDomain = 1] BitMask 0x02 [reserved] BitMask 0x04 [reserved] BitMask 0x08 [reserved] BitMask 0x10 [reserved] BitMask 0x20 [reserved] BitMask 0x40 [reserved] BitMask 0x80 [reserved] |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **CRC_Status**                 |                  | 1                                    | 3          | 0..255                                                                                                                                                                                                                            |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **AUTOSAR TLV Sub-TLV: Status Not Secured**                                                                                                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **High Nibble**                | **Low Nibble**   | **Octets**                           | **Offset** | **Value**                                                                                                                                                                                                                         |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Type**                       |                  | 1                                    | 0          | 0x51 [Status Not Secured]                                                                                                                                                                                                         |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Length**                     |                  | 1                                    | 1          | 2                                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Status**                     |                  | 1                                    | 2          | BitMask 0x01 [SGW with SyncToGTM = 0 SyncToSubDomain = 1] BitMask 0x02 [reserved] BitMask 0x04 [reserved] BitMask 0x08 [reserved] BitMask 0x10 [reserved] BitMask 0x20 [reserved] BitMask 0x40 [reserved] BitMask 0x80 [reserved] |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **reserved**                   |                  | 1                                    | 3          | 0                                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **AUTOSAR TLV Sub-TLV: UserData Secured**                                                                                                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **High Nibble**                | **Low Nibble**   | **Octets**                           | **Offset** | **Value**                                                                                                                                                                                                                         |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Type**                       |                  | 1                                    | 0          | 0x60[UserData secured]                                                                                                                                                                                                            |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Length**                     |                  | 1                                    | 1          | 5                                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserDataLength**             |                  | 1                                    | 2          | 1..3                                                                                                                                                                                                                              |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserByte_0**                 |                  | 1                                    | 3          | 0..255 (default: 0)                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserByte_1**                 |                  | 1                                    | 4          | 0..255 (default: 0)                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserByte_2**                 |                  | 1                                    | 5          | 0..255 (default: 0)                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **CRC_UserData**               |                  | 1                                    | 6          | 0..255                                                                                                                                                                                                                            |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **AUTOSAR TLV Sub-TLV: UserData Not Secured**                                                                                                                                                                                                                                                                                             |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **High Nibble**                | **Low Nibble**   | **Octets**                           | **Offset** | **Value**                                                                                                                                                                                                                         |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Type**                       |                  | 1                                    | 0          | 0x61 [UserData not secured]                                                                                                                                                                                                       |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Length**                     |                  | 1                                    | 1          | 5                                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserDataLength**             |                  | 1                                    | 2          | 1..3                                                                                                                                                                                                                              |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserByte_0**                 |                  | 1                                    | 3          | 0..255 (default: 0)                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserByte_1**                 |                  | 1                                    | 4          | 0..255 (default: 0)                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserByte_2**                 |                  | 1                                    | 5          | 0..255 (default: 0)                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **reserved**                   |                  | 1                                    | 6          | 0                                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **AUTOSAR TLV Sub-TLV: OFS Secured**                                                                                                                                                                                                                                                                                                      |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **High Nibble**                | **Low Nibble**   | **Octets**                           | **Offset** | **Value**                                                                                                                                                                                                                         |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Type**                       |                  | 1                                    | 0          | 0x44 [OFS secured]                                                                                                                                                                                                                |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Length**                     |                  | 1                                    | 1          | 17                                                                                                                                                                                                                                |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **OfsTimeDomain**              |                  | 1                                    | 2          | 16..31                                                                                                                                                                                                                            |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **OfsTimeSec**                 |                  | 6                                    | 3          | 0..281474976710655s                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **OfsTimeNSec**                |                  | 4                                    | 9          | 0..999999999ns                                                                                                                                                                                                                    |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Status**                     |                  | 1                                    | 13         | BitMask 0x01 [SGW with SyncToGTM = 0 SyncToSubDomain = 1] BitMask 0x02 [reserved] BitMask 0x04 [reserved] BitMask 0x08 [reserved] BitMask 0x10 [reserved] BitMask 0x20 [reserved] BitMask 0x40 [reserved] BitMask 0x80 [reserved] |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserDataLength**             |                  | 1                                    | 14         | 0..3 (default: 0)                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserByte_0**                 |                  | 1                                    | 15         | 0..255 (default: 0)                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserByte_1**                 |                  | 1                                    | 16         | 0..255 (default: 0)                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserByte_2**                 |                  | 1                                    | 17         | 0..255 (default: 0)                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **CRC_OFS**                    |                  | 1                                    | 18         | 0..255                                                                                                                                                                                                                            |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **AUTOSAR TLV Sub-TLV: OFS Not Secured**                                                                                                                                                                                                                                                                                                  |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **High Nibble**                | **Low Nibble**   | **Octets**                           | **Offset** | **Value**                                                                                                                                                                                                                         |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Type**                       |                  | 1                                    | 0          | 0x34 [OFS not secured]                                                                                                                                                                                                            |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Length**                     |                  | 1                                    | 1          | 17                                                                                                                                                                                                                                |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **OfsTimeDomain**              |                  | 1                                    | 2          | 16..31                                                                                                                                                                                                                            |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **OfsTimeSec**                 |                  | 6                                    | 3          | 0..281474976710655s                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **OfsTimeNSec**                |                  | 4                                    | 9          | 0..999999999ns                                                                                                                                                                                                                    |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Status**                     |                  | 1                                    | 13         | BitMask 0x01 [SGW with SyncToGTM = 0 SyncToSubDomain = 1] BitMask 0x02 [reserved] BitMask 0x04 [reserved] BitMask 0x08 [reserved] BitMask 0x10 [reserved] BitMask 0x20 [reserved] BitMask 0x40 [reserved] BitMask 0x80 [reserved] |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserDataLength**             |                  | 1                                    | 14         | 0..3 (default: 0)                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserByte_0**                 |                  | 1                                    | 15         | 0..255 (default: 0)                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserByte_1**                 |                  | 1                                    | 16         | 0..255 (default: 0)                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **UserByte_2**                 |                  | 1                                    | 17         | 0..255 (default: 0)                                                                                                                                                                                                               |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **reserved**                   |                  | 1                                    | 18         | 0                                                                                                                                                                                                                                 |
   +--------------------------------+------------------+--------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+


Follow_Up Message Header [AUTOSAR]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. sw_req:: PRS_TS_00066
   :id: PRS_TS_00066
   :status: new/changed
   :safety_level: QM
   
   The ``messageLength`` of the ``Follow_Up`` Message Header has to be adapted according to the length of all existing *TLVs*.
   
   (`RS_TS_20048`_, `RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00066
      
      <Verification criteria text>
   

AUTOSAR *TLV* Header
^^^^^^^^^^^^^^^^^^^^

.. sw_req:: PRS_TS_00067
   :id: PRS_TS_00067
   :status: new/changed
   :safety_level: QM
   
   The AUTOSAR *TLV* Header has a multiplicity of 1.
   
   (`RS_TS_20048`_, `RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00067
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00068
   :id: PRS_TS_00068
   :status: new/changed
   :safety_level: QM
   
   If an AUTOSAR *TLV* Header exists, at least one AUTOSAR *Sub-TLV* must exist as well.
   
   (`RS_TS_20048`_, `RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00068
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00069
   :id: PRS_TS_00069
   :status: new/changed
   :safety_level: QM
   
   If an AUTOSAR *TLV* Header exists, the ``lengthField`` shall be adapted according the number of existing AUTOSAR *Sub-TLVs*.
   
   (`RS_TS_20048`_, `RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00069
      
      <Verification criteria text>
   

AUTOSAR TLV Sub-TLVs
^^^^^^^^^^^^^^^^^^^^

.. sw_req:: PRS_TS_00070
   :id: PRS_TS_00070
   :status: new/changed
   :safety_level: QM
   
   If an AUTOSAR *Sub-TLV* exists, it shall be placed after the AUTOSAR *TLV* Header.
   
   (`RS_TS_20048`_, `RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00070
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00071
   :id: PRS_TS_00071
   :status: new/changed
   :safety_level: QM
   
   If more than one AUTOSAR *Sub-TLV* exists, each *Sub-TLV* shall be placed after the preceding *Sub-TLV* without gaps.
   
   (`RS_TS_20048`_, `RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00071
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00072
   :id: PRS_TS_00072
   :status: new/changed
   :safety_level: QM
   
   If more than one AUTOSAR *Sub-TLV* exists, the positon of each *Sub-TLV* is arbitrary.
   
   (`RS_TS_20048`_, `RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00072
      
      <Verification criteria text>
   

.. _chap_3a_AUTSAR_TLV_SubLV_3a__Time_Secred__f6f2c474:

AUTOSAR *TLV Sub-TLV*: Time Secured
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. sw_req:: PRS_TS_00074
   :id: PRS_TS_00074
   :status: new/changed
   :safety_level: QM
   
   The AUTOSAR *Sub-TLV*: Time Secured has a multiplicity of 1 and is only available, if *CRC* protection is required.
   
   (`RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00074
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00075
   :id: PRS_TS_00075
   :status: new/changed
   :safety_level: QM
   
   If ``MessageCompliance`` is ``FALSE`` and ``TLVFollowUpTimeSubTLV`` is set to ``TRUE``, the Time Master shall send a ``Follow_Up``, which contains an AUTOSAR *Sub-TLV*: Time Secured.
   
   (`RS_TS_20048`_, `RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00075
      
      <Verification criteria text>
   

AUTOSAR *TLV Sub-TLV*: Status Secured / Not Secured
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. sw_req:: PRS_TS_00076
   :id: PRS_TS_00076
   :status: new/changed
   :safety_level: QM
   
   The AUTOSAR *Sub-TLV*: Status has a multiplicity of 1 and can either be *CRC* protected (Status Secured) or not (Status Not Secured).
   
   (`RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00076
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00077
   :id: PRS_TS_00077
   :status: new/changed
   :safety_level: QM
   
   If ``MessageCompliance`` is set to ``FALSE`` and ``TLVFollowUpStatusSubTLV`` is set to ``TRUE``, the Time Master shall send a ``Follow_Up``, which contains an AUTOSAR *Sub-TLV*: Status.
   
   (`RS_TS_20048`_, `RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00077
      
      <Verification criteria text>
   

AUTOSAR *TLV Sub-TLV*: UserData Secured / Not Secured
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. sw_req:: PRS_TS_00078
   :id: PRS_TS_00078
   :status: new/changed
   :safety_level: QM
   
   The AUTOSAR *Sub-TLV*: UserData has a multiplicity of 1 and can either be *CRC* protected (UserData Secured) or not (UserData Not Secured).
   
   (`RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00078
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00079
   :id: PRS_TS_00079
   :status: new/changed
   :safety_level: QM
   
   If ``MessageCompliance`` is set to ``FALSE`` and ``Follow_Up TLV User Data configuration`` is set to ``TRUE``, the Time Master shall send a ``Follow_Up``, which contains an AUTOSAR *Sub-TLV*: UserData.
   
   (`RS_TS_20048`_, `RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00079
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00081
   :id: PRS_TS_00081
   :status: new/changed
   :safety_level: QM
   
   The AUTOSAR *Sub-TLV*: UserData shall be read from the current incoming message consistently.
   
   (`RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00081
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00082
   :id: PRS_TS_00082
   :status: new/changed
   :safety_level: QM
   
   The AUTOSAR *Sub-TLV*: UserData shall be written to the next outgoing message consistently.
   
   (`RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00082
      
      <Verification criteria text>
   

AUTOSAR *TLV Sub-TLV*: OFS Secured / Not Secured
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. sw_req:: PRS_TS_00084
   :id: PRS_TS_00084
   :status: new/changed
   :safety_level: QM
   
   The AUTOSAR *Sub-TLV*: OFS has a multiplicity of 16 and can either be *CRC* protected (OFS Secured) or not (OFS Not Secured).
   
   (`RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00084
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00085
   :id: PRS_TS_00085
   :status: new/changed
   :safety_level: QM
   
   The element ``OfsTimeDomain`` of the AUTOSAR *Sub-TLV*: OFS shall contain the Offset Time Domain identifier, which is in a range between 16 and 31.
   
   (`RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00085
      
      <Verification criteria text>
   
**Note:** Compared to CAN and FlexRay, Ethernet does need any optimization on payload bytes on bit-level.

.. sw_req:: PRS_TS_00086
   :id: PRS_TS_00086
   :status: new/changed
   :safety_level: QM
   
   If ``MessageCompliance`` is set to ``FALSE`` and ``TSynTLVFollowUpOFSSubTLV`` is set to ``TRUE``, the Time Master shall send a ``Follow_Up``, which contains at least one AUTOSAR *Sub-TLV*: OFS.
   
   (`RS_TS_20048`_, `RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00086
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00088
   :id: PRS_TS_00088
   :status: new/changed
   :safety_level: QM
   
   The User Data of the AUTOSAR *Sub-TLV*: OFS shall be read from an incoming message consistently.
   
   (`RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00088
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00089
   :id: PRS_TS_00089
   :status: new/changed
   :safety_level: QM
   
   The User Data of the AUTOSAR *Sub-TLV*: OFS shall be written to an outgoing message consistently.
   
   (`RS_TS_20061`_, `RS_TS_20062`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00089
      
      <Verification criteria text>
   

Body/Payload format
-------------------

Placeholder for upcoming Autosar releases.


Data Types
----------

Refer to `DOC_IEEEStandard8021AS30`_.


Message types
=============

Refer to `DOC_IEEEStandard8021AS30`_.


Data Messages
-------------

Refer to `DOC_IEEEStandard8021AS30`_.


Control Messages
----------------

Refer to `DOC_IEEEStandard8021AS30`_.


Services / Commands
===================

Placeholder for upcoming Autosar releases.


Sequences (lower layer)
=======================


``Pdelay`` Protocol for Latency Calculation
-------------------------------------------

.. _`Propagation Delay Measurement (Pdelay)`:

.. figure:: img/PropagationDelayMeasurement.png
   
   Propagation Delay Measurement (Pdelay)
   
_`Figure 5.1: Propagation Delay Measurement (Pdelay)`

.. sw_req:: PRS_TS_00003
   :id: PRS_TS_00003
   :status: new/changed
   :safety_level: QM
   
   The Time Sync module shall use for latency calculation
   
   - either static ``Pdelay`` values (GlobalTimePropagationDelay)
   - or runtime-based values calculated by ``Pdelay_Req, Pdelay_Resp, Pdelay_Resp_Follow_Up`` according to ``Figure 5.1: Propagation Delay Measurement (Pdelay)``

   depending on configuration of ``GlobalTimeTxPdelayReqPeriod``
   
   (`RS_TS_20048`_, `RS_TS_20066`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00003
      
      <Verification criteria text>
   
**Note:**: Due to the limitation [1] and [3] `Limitations`_ "Limitations", it is sufficient that only the Time Slave initiates the Pdelay measurement

.. sw_req:: PRS_TS_00154
   :id: PRS_TS_00154
   :status: new/changed
   :safety_level: QM
   
   If ``GlobalTimeTxPdelayReqPeriod`` is not equal to 0 and if the ``Pdelay`` latency calculation result exceeds ``PdelayLatencyThreshold``, the measured value shall be discarded and the previous value shall be kept.
   
   (`RS_TS_20048`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00154
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00004
   :id: PRS_TS_00004
   :status: new/changed
   :safety_level: QM
   
   A ``Pdelay_Resp`` timeout or incomplete ``Pdelay`` protocol shall stop the latency calculation algorithm. In such cases, the device shall use the latest successful calculated latency value.
   
   (`RS_TS_20048`_, `RS_TS_20051`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00004
      
      <Verification criteria text>
   
**Note:** A timeout is detected, when sending the next subsequent ``Pdelay_Req`` before receiving the ``Pdelay_Resp`` resp. ``Pdelay_Resp_Follow_Up`` belonging to the ``Pdelay_Req`` before.

.. sw_req:: PRS_TS_00164
   :id: PRS_TS_00164
   :status: new/changed
   :safety_level: QM
   
   Time Master and Time Slave shall observe the ``Pdelay`` timeout as given by ``PdelayRespAndRespFollowUpTimeout`` , if a ``Pdelay_Req`` has been transmitted (waiting for ``Pdelay_Resp``\ ) or if a ``Pdelay_Resp`` has been received (waiting for ``Pdelay_Resp_Follow_Up``\ ). A value of 0 deactivates this timeout observation.
   
   (`RS_TS_20048`_, `RS_TS_20051`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00164
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00210
   :id: PRS_TS_00210
   :status: new/changed
   :safety_level: QM
   
   If a reception timeout occurs (refer to [:need:`PRS_TS_00164`]), any received ``Pdelay_Resp`` resp ``Pdelay_Resp_Follow_Up`` shall be ignored, until a new ``Pdelay_Req`` has been sent.
   
   (`RS_TS_20048`_, `RS_TS_20051`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00210
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00140
   :id: PRS_TS_00140
   :status: new/changed
   :safety_level: QM
   
   If ``GlobalTimeTxPdelayReqPeriod`` equals ``0``, Time Master and Time Slave shall not measure the propagation delay. The Time Slave shall use a static value ``GlobalTimePropagationDelay`` as propagation delay instead.
   
   (`RS_TS_20066`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00140
      
      <Verification criteria text>
   
**Note:** Since ``GlobalTimeTxPdelayReqPeriod`` is ECU specific, neither a Time Master nor all Time Slaves have to measure the propagation delay. Global Time Synchronization in AUTOSAR does yet not define dynamic reconfiguration or backup strategies that will reassign the role as Time Master, therefore propagation delay measurements make currently no sense for a Time Master (although a Time Master shall be able to handle ``Pdelay_Req`` initiated by a Time Slave).

.. sw_req:: PRS_TS_00141
   :id: PRS_TS_00141
   :status: new/changed
   :safety_level: QM
   
   If ``GlobalTimeTxPdelayReqPeriod`` is greater than ``0``, Time Master and Time Slave shall cyclically measure the propagation delay using ``Pdelay_Req, Pdelay_Resp, Pdelay_Resp_Follow_Up`` as defined in `DOC_IEEEStandard8021AS30`_ chapter 11.1.2 "Propagation delay measurement".
   
   (`RS_TS_20048`_, `RS_TS_20066`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00141
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00149
   :id: PRS_TS_00149
   :status: new/changed
   :safety_level: QM
   
   If ``GlobalTimeTxPdelayReqPeriod`` is greater than ``0``, Time Master and Time Slave shall cyclically measure the propagation delay only on that Time Domain with the lowest Time Domain ID and shall use this value to adjust all corresponding Time Bases.
   
   (`RS_TS_20048`_, `RS_TS_20066`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00149
      
      <Verification criteria text>
   
**Note:** There is no need to measure the propagation delay for all Time Domains, because the same value is expected. This requirement ensures also the usage of Time Domain ``0`` for ``Pdelay``, to be compatible to `DOC_IEEEStandard8021AS30`_.

.. sw_req:: PRS_TS_00142
   :id: PRS_TS_00142
   :status: new/changed
   :safety_level: QM
   
   If ``GlobalTimeTxPdelayReqPeriod`` is greater than ``0,GlobalTimePropagationDelay`` shall be used as default value for the propagation delay, until first valid propagation delay has been measured.
   
   (`RS_TS_20048`_, `RS_TS_20066`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00142
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00011
   :id: PRS_TS_00011
   :status: new/changed
   :safety_level: QM
   
   If ``GlobalTimeTxPdelayReqPeriod`` is greater than ``0``, Time Master and Time Slave shall periodically transmit ``Pdelay_Req`` for latency calculation with the cycle ``GlobalTimeTxPdelayReqPeriod`` as defined in `DOC_IEEEStandard8021AS30`_ chapter 11.1.2 "Propagation delay measurement".
   
   (`RS_TS_20048`_, `RS_TS_20066`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00011
      
      <Verification criteria text>
   
**Note:** ``GlobalTimePdelayRespEnable`` allows disabling of ``Pdelay_Resp`` and ``Pdelay_Resp_Follow_Up``, if no ``Pdelay_Req`` is expected to be received, i.e. for the Time Master, if all Time Slaves have set ``GlobalTimeTxPdelayReqPeriod`` to ``0`` or for any Time Slave if the Time Master has set ``GlobalTimeTxPdelayReqPeriod`` to ``0``.

.. sw_req:: PRS_TS_00012
   :id: PRS_TS_00012
   :status: new/changed
   :safety_level: QM
   
   If ``GlobalTimePdelayRespEnable`` is set to ``TRUE``, Time Master and Time Slave shall react to ``Pdelay_Req`` by transmitting ``Pdelay_Resp`` for latency calculation as defined in `DOC_IEEEStandard8021AS30`_ chapter 11.1.2 "Propagation delay measurement".
   
   (`RS_TS_20048`_, `RS_TS_20066`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00012
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00143
   :id: PRS_TS_00143
   :status: new/changed
   :safety_level: QM
   
   If ``GlobalTimePdelayRespEnable `` is set to ``FALSE``, ``Pdelay_Resp`` and ``Pdelay_Resp_Follow_Up`` shall be omitted.
   
   (`RS_TS_20066`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00143
      
      <Verification criteria text>
   

Acting as Time Master
---------------------

A Time Master is an entity which is the master for a certain Time Base and which propagates this Time Base to a set of Time Slaves within a certain segment of a communication network, being a source for this Time Base.

If a Time Master is also the owner of the Time Base then he is the Global Time master. A Time Gateway typically consists of one Time Slave and one or more Time Masters. When mapping time entities to real ECUs, an ECU could be Time Master (or even Global Time Master) for one Time Base and Time Slave for another Time Base.


Message Processing
^^^^^^^^^^^^^^^^^^

.. sw_req:: PRS_TS_00050
   :id: PRS_TS_00050
   :status: new/changed
   :safety_level: QM
   
   The Time Master shall support the transmission of ``Sync`` and ``Follow_Up`` according as well as the transmission and reception of ``Pdelay_Req``, ``Pdelay_Resp`` and ``Pdelay_Resp_Follow_Up``.
   
   (`RS_TS_20047`_, `RS_TS_20048`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00050
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00016
   :id: PRS_TS_00016
   :status: new/changed
   :safety_level: QM
   
   The Time Master shall periodically transmit ``Sync`` with the cycle ``GlobalTimeTxPeriod`` as defined in `DOC_IEEEStandard8021AS30`_ chapter 11.1.3 "Transport of time-synchronization information", if the ``GLOBAL_TIME_BASE`` bit within the ``timeBaseStatus``, which is read from the corresponding Time Base, is set and ``GlobalTimeTxPeriod`` is not 0.
   
   (`RS_TS_20047`_, `RS_TS_20048`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00016
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00018
   :id: PRS_TS_00018
   :status: new/changed
   :safety_level: QM
   
   The ``preciseOriginTimestamp`` as calculated above, shall be used in the transmission of the ``Follow_Up`` as defined in `DOC_IEEEStandard8021AS30`_ chapter 11.1.3 "Transport of time-synchronization information".
   
   (`RS_TS_20048`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00018
      
      <Verification criteria text>
   

Frame Debouncing
""""""""""""""""

.. sw_req:: PRS_TS_00186
   :id: PRS_TS_00186
   :status: new/changed
   :safety_level: QM
   
   If multiple frames are triggered at the same time, the frames shall be sent in the following order:
   
   #. ``Sync``
   #. ``Follow_Up``
   #. ``Pdelay_Req``
   #. ``Pdelay_Resp, Pdelay_Resp_Follow_Up``

   (`RS_TS_20047`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00186
      
      <Verification criteria text>
   

Message Field Calculation and Assembling
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. sw_req:: PRS_TS_00092
   :id: PRS_TS_00092
   :status: new/changed
   :safety_level: QM
   
   If ``MessageCompliance`` is set to ``FALSE``, a Time Master shall add an *AUTOSAR TLV* to the ``Follow_Up`` frame.
   
   (`RS_TS_20061`_, `RS_TS_20062`_, `RS_TS_20063`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00092
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00091
   :id: PRS_TS_00091
   :status: new/changed
   :safety_level: QM
   
   If ``MessageCompliance`` is set to ``FALSE, CRC_SUPPORT`` shall be considered.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00091
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00093
   :id: PRS_TS_00093
   :status: new/changed
   :safety_level: QM
   
   Depending on ``CRC_SUPPORT`` the ``Follow_Up.TLV[AUTOSAR].Sub-TLV.Type`` shall be:
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00093
      
      <Verification criteria text>
   
``Follow_Up Message Header [IEEE 802.1AS]``


.. _table_3a_usecases1:
   
   +----------------------------+-----------------------------------------------------------------------------------------------+
   |                            |       ``Sub-TLV.Type``                                                                        |
   +============================+===============================================+===============================================+
   | ``GlobalTimeTxCrcSecured`` | ``CRC_SUPPORTED``                             | ``CRC_NOT_SUPPORTED``                         |
   +----------------------------+-----------------------------------------------+-----------------------------------------------+
   |                            | 0x28 *Sub-TLV*: Time Secured is *CRC* secured | n.a.                                          |
   +----------------------------+-----------------------------------------------+-----------------------------------------------+
   |                            | 0x50 *Sub-TLV*: Status is *CRC* secured       | 0x51 *Sub-TLV*: Status is not *CRC* secured   |
   +----------------------------+-----------------------------------------------+-----------------------------------------------+
   |                            | 0x60 *Sub-TLV*: UserData is *CRC* secured     | 0x61 *Sub-TLV*: UserData is not *CRC* secured |
   +----------------------------+-----------------------------------------------+-----------------------------------------------+
   |                            | 0x44 *Sub-TLV*: OFS is *CRC* secured          | 0x34 *Sub-TLV*: OFS is not *CRC* secured      |
   +----------------------------+-----------------------------------------------+-----------------------------------------------+


.. _chap_3a_SGW_Calion__890a5ae4:

SGW Calculation
"""""""""""""""

.. sw_req:: PRS_TS_00094
   :id: PRS_TS_00094
   :status: new/changed
   :safety_level: QM
   
   The *SGW* value (Time Gateway synchronization status) shall be mapped to the Status element of the *AUTOSAR Sub-TLV*: Status resp. the *AUTOSAR Sub-TLV*: OFS. If the ``SYNC_TO_GATEWAY`` is set, the *SGW* value shall be *SyncToSubDomain*. Otherwise, it shall be *SyncToGTM*.
   
   (`RS_TS_20052`_, `RS_TS_20054`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00094
      
      <Verification criteria text>
   

.. _chap_3a_OFS_Calion__3cbe1cfc:

OFS Calculation
"""""""""""""""

.. sw_req:: PRS_TS_00095
   :id: PRS_TS_00095
   :status: new/changed
   :safety_level: QM
   
   The Time Master of an Offset Time Base shall send the "second" part of the Offset Time Base value via the ``OfsTimeSec`` element of the corresponding AUTOSAR *Sub-TLV*: OFS and the "nanosecond" part of the Offset Time Base value via the ``OfsTimeNSec`` element of the corresponding AUTOSAR *Sub-TLV*: OFS
   
   (`RS_TS_20063`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00095
      
      <Verification criteria text>
   

CRC Calculation
"""""""""""""""

.. sw_req:: PRS_TS_00097
   :id: PRS_TS_00097
   :status: new/changed
   :safety_level: QM
   
   The ``DataID`` shall be calculated as: ``DataID = DataIDList[Follow_Up.sequenceId mod 16]``, where ``DataIDList`` is given by configuration for the ``Follow_Up``.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00097
      
      <Verification criteria text>
   
**Note:** A specific ``DataID`` out of a predefined ``DataIDList`` ensures the identification of data elements of Time Synchronization messages.

.. sw_req:: PRS_TS_00182
   :id: PRS_TS_00182
   :status: new/changed
   :safety_level: QM
   
   If applying the *CRC* calculation on multibyte values, the byte order shall be such, that the byte containing the most significant bit of the value shall be used first.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00182
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00184
   :id: PRS_TS_00184
   :status: new/changed
   :safety_level: QM
   
   If applying the *CRC* calculation on multibyte message data, the byte order shall be in ascending order of the octets, i.e., the octet with the lowest offset shall be used first.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00184
      
      <Verification criteria text>
   

.. _chap_3a_AUTSAR_TLV_SubLV_3a__Time_Secred__88a52f22:

AUTOSAR TLV *Sub-TLV*: Time Secured
"""""""""""""""""""""""""""""""""""

.. sw_req:: PRS_TS_00098
   :id: PRS_TS_00098
   :status: new/changed
   :safety_level: QM
   
   If ``GlobalTimeTxCrcSecured`` is ``CRC_SUPPORTED``, the Time Master shall write the contents of ``CrcTimeFlagsTxSecured`` to ``CRC_Time_Flags`` acc. to the following rule.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00098
      
      <Verification criteria text>
   

.. _table_3a_usecases2:
   
   +--------------------+---------------------------------------------------------------------+
   |                    | ``CrcTimeFlagsTxSecured`` contents:                                 |
   +====================+==============================+======================================+
   | ``CRC_Time_Flags`` | ``Follow_Up`` Message Header | ``Follow_Up`` Message Field          |
   +--------------------+------------------------------+--------------------------------------+
   | BitMask 0x01       | ``CRCMessageLength``         | n.a.                                 |
   +--------------------+------------------------------+--------------------------------------+
   | BitMask 0x02       | ``CRCDomainNumber``          | n.a.                                 |
   +--------------------+------------------------------+--------------------------------------+
   | BitMask 0x04       | ``CrcCorrectionField``       | n.a.                                 |
   +--------------------+------------------------------+--------------------------------------+
   | BitMask 0x08       | ``CRCSourcePortIdentity``    | n.a.                                 |
   +--------------------+------------------------------+--------------------------------------+
   | BitMask 0x10       | ``CRCSequenceIdentity``      | n.a.                                 |
   +--------------------+------------------------------+--------------------------------------+
   | BitMask 0x20       | n.a.                         | ``CRCPrecise`` - ``OriginTimestamp`` |
   +--------------------+------------------------------+--------------------------------------+
   | BitMask 0x40       | n.a.                         | n.a.                                 |
   +--------------------+------------------------------+--------------------------------------+
   | BitMask 0x80       | n.a.                         | n.a.                                 |
   +--------------------+------------------------------+--------------------------------------+

.. sw_req:: PRS_TS_00099
   :id: PRS_TS_00099
   :status: new/changed
   :safety_level: QM
   
   If ``CrcTimeFlagsTxSecured`` is ``supported``, the Time Master shall calculate the *CRC* for ``CRC_Time_0`` by considering the contents of ``CRC_Time_Flags`` itself, the contents of the dependent fields as defined in ``CrcTimeFlagsTxSecured`` acc. to the rule in the table below and the ``DataID``. The data elements used for the calculation of the CRC shall apply the following order:
   
   #. the value of ``CRC_Time_Flags``
   #. the ``domainNumber`` inside the ``Follow_Up`` Message Header, if ``CRC_Time_Flags`` contains BitMask *0x02*
   #. the ``sourcePortIdentity`` inside the ``Follow_Up`` Message Header, if *CRC_Time_Flags*\ contains BitMask *0x08*
   #. the ``preciseOriginTimestamp`` inside the ``Follow_Up`` Message Field, if *CRC_Time_Flags* contains BitMask *0x20*
   #. the ``DataID``

   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00099
      
      <Verification criteria text>
   

.. _table_3a_usecases3:
   
   +-------------------------------------------+-------------------------------------------------------------------------+
   |                                           | **For** ``CRC_Time_0`` **calculation considered contents:**             |
   +===========================================+==================================+======================================+
   | **If** ``CRC_Time_Flags`` **is set to 1** | ``Follow_Up`` **Message Header** | ``Follow_Up`` **Message Field**      |
   +-------------------------------------------+----------------------------------+--------------------------------------+
   | BitMask 0x01                              | n.a.                             | n.a.                                 |
   +-------------------------------------------+----------------------------------+--------------------------------------+
   | BitMask 0x02                              | ``CRCDomainNumber``              | n.a.                                 |
   +-------------------------------------------+----------------------------------+--------------------------------------+
   | BitMask 0x04                              | n.a.                             | n.a.                                 |
   +-------------------------------------------+----------------------------------+--------------------------------------+
   | BitMask 0x08                              | ``CRCSourcePortIdentity``        | n.a.                                 |
   +-------------------------------------------+----------------------------------+--------------------------------------+
   | BitMask 0x10                              | n.a.                             | n.a.                                 |
   +-------------------------------------------+----------------------------------+--------------------------------------+
   | BitMask 0x20                              | n.a.                             | ``CRCPrecise`` - ``OriginTimestamp`` |
   +-------------------------------------------+----------------------------------+--------------------------------------+
   | BitMask 0x40                              | n.a.                             | n.a.                                 |
   +-------------------------------------------+----------------------------------+--------------------------------------+
   | BitMask 0x80                              | n.a.                             | n.a.                                 |
   +-------------------------------------------+----------------------------------+--------------------------------------+

**Note:** ``CRC_Time_Flags`` is having the same value like the configuration item ``CrcTimeFlagsTxSecured``, whereas the resulting *CRC* of the dependent items remains network wide unchanged.

.. sw_req:: PRS_TS_00100
   :id: PRS_TS_00100
   :status: new/changed
   :safety_level: QM
   
   If ``GlobalTimeTxCrcSecured`` is set to ``CRC_SUPPORTED``, the Time Master shall calculate the *CRC* for ``CRC_Time_1`` by considering the contents of ``CRC_Time_Flags`` itself, the contents of the dependent fields as defined in ``CrcTimeFlagsTxSecured`` acc. to the rule in the table below and the ``DataID``. The data elements used for the calculation of the *CRC* shall apply the following order:
   
   #. the value of ``CRC_Time_Flags``
   #. the *messageLength* inside the ``Follow_Up Message Header``, if ``CRC_Time_Flags`` contains BitMask ``0x01``
   #. the ``CrcCorrectionField`` inside the ``Follow_Up Message Header``, if ``CRC_Time_Flags`` contains BitMask ``0x04``
   #. the ``sequenceId`` inside the ``Follow_Up Message Header``, if ``CRC_Time_Flags`` contains BitMask ``0x10``
   #. the ``DataID``

   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00100
      
      <Verification criteria text>
   

.. _table_3a_usecases4:
   
   +-------------------------------------------+--------------------------------------------------------------------+
   |                                           | **For** ``CRC_Time_1`` **calculation considered contents:**        |
   +===========================================+==================================+=================================+
   | **If** ``CRC_Time_Flags`` **is set to 1** | ``Follow_Up`` **Message Header** | ``Follow_Up`` **Message Field** |
   +-------------------------------------------+----------------------------------+---------------------------------+
   | BitMask 0x01                              | ``messageLength``                | n.a.                            |
   +-------------------------------------------+----------------------------------+---------------------------------+
   | BitMask 0x02                              | n.a.                             | n.a.                            |
   +-------------------------------------------+----------------------------------+---------------------------------+
   | BitMask 0x04                              | ``CrcCorrectionField``.          | n.a.                            |
   +-------------------------------------------+----------------------------------+---------------------------------+
   | BitMask 0x08                              | n.a.                             | n.a.                            |
   +-------------------------------------------+----------------------------------+---------------------------------+
   | BitMask 0x10                              | ``sequenceId``                   | n.a.                            |
   +-------------------------------------------+----------------------------------+---------------------------------+
   | BitMask 0x20                              | n.a.                             | n.a.                            |
   +-------------------------------------------+----------------------------------+---------------------------------+
   | BitMask 0x40                              | n.a.                             | n.a.                            |
   +-------------------------------------------+----------------------------------+---------------------------------+
   | BitMask 0x80                              | n.a.                             | n.a.                            |
   +-------------------------------------------+----------------------------------+---------------------------------+

**Note:** ``CRC_Time_Flags`` has the same value as the configuration item ``CrcTimeFlagsTxSecured``.


.. _chap_3a_AUTSAR_TLV_SubLV_3a__Status_secred__ee686ba6:

AUTOSAR TLV Sub-TLV: Status secured
"""""""""""""""""""""""""""""""""""

.. sw_req:: PRS_TS_00101
   :id: PRS_TS_00101
   :status: new/changed
   :safety_level: QM
   
   If ``GlobalTimeTxCrcSecured`` is set to ``CRC_SUPPORTED``, the Time Master shall calculate the *CRC* for ``CRC_STATUS`` by considering the contents of Status and DataID (in this order).
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00101
      
      <Verification criteria text>
   

.. _chap_3a_AUTSAR_TLV_SubLV_3a__Useata_secred__4ca342de:

AUTOSAR TLV Sub-TLV: UserData secured
"""""""""""""""""""""""""""""""""""""

.. sw_req:: PRS_TS_00102
   :id: PRS_TS_00102
   :status: new/changed
   :safety_level: QM
   
   If ``GlobalTimeTxCrcSecured`` is set to ``CRC_SUPPORTED``, the Time Master shall calculate the *CRC* for ``CRC_UserData`` by considering the contents of ``UserDataLength, UserByte_0, UserByte_1, UserByte_2`` and ``DataID`` (in this order).
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00102
      
      <Verification criteria text>
   

.. _chap_3a_AUTSAR_TLV_SubLV_3a__OFS_secred__52874eff:

AUTOSAR TLV Sub-TLV: OFS secured
""""""""""""""""""""""""""""""""

.. sw_req:: PRS_TS_00103
   :id: PRS_TS_00103
   :status: new/changed
   :safety_level: QM
   
   If ``GlobalTimeTxCrcSecured`` is set to ``CRC_SUPPORTED``, the Time Master shall calculate the *CRC* for ``CRC_OFS`` by considering the contents of ``OfsTimeDomain``, ``OfsTimeSec, OfsTimeNSec, Status, UserDataLength, UserByte_0, UserByte_1, UserByte_2`` and ``DataID`` (in this order).
   
   (`RS_TS_20061`_, `RS_TS_20062`_, `RS_TS_20063`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00103
      
      <Verification criteria text>
   

Sequence Counter Calculation
""""""""""""""""""""""""""""

.. sw_req:: PRS_TS_00187
   :id: PRS_TS_00187
   :status: new/changed
   :safety_level: QM
   
   The Sequence Counter (\ ``sequenceId``\ ) of a ``Sync`` and ``Pdelay_Req`` message shall be initialized with 0.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00187
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00188
   :id: PRS_TS_00188
   :status: new/changed
   :safety_level: QM
   
   The Peer Delay Initiator shall increment the Sequence Counter of a ``Pdelay_Req`` message by 1 on each transmission request for a ``Pdelay_Req`` message. The Sequence Counter shall wrap around at 65535 to 0 again.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00188
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00189
   :id: PRS_TS_00189
   :status: new/changed
   :safety_level: QM
   
   The Time Master shall increment the Sequence Counter of a Sync message by 1 on each transmission request for a Sync message of a given Time Domain. The Sequence Counter shall wrap around at 65535 to 0 again.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00189
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00190
   :id: PRS_TS_00190
   :status: new/changed
   :safety_level: QM
   
   The Time Master shall set the Sequence Counter (\ ``sequenceId``\ ) value for a ``Follow_Up`` message to the Sequence Counter (\ ``sequenceId``\ ) value of the corresponding Sync message.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00190
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00191
   :id: PRS_TS_00191
   :status: new/changed
   :safety_level: QM
   
   The Peer Delay Responder shall set the Sequence Counter (\ ``sequenceId``\ ) value for a ``Pdelay_Resp`` and ``Pdelay_Resp_Follow_Up`` message to the Sequence Counter (\ ``sequenceId``\ ) value of the corresponding ``Pdelay_Req`` message.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00191
      
      <Verification criteria text>
   

Message Assembling
""""""""""""""""""

.. sw_req:: PRS_TS_000104
   :id: PRS_TS_000104
   :status: new/changed
   :safety_level: QM
   
   For each transmission of a Time Synchronization message, the Time Synchronization module shall assemble the message as follows:
   
   #. If ``Sync``: Calculate Message Header
   #. If ``Follow_Up``: Calculate ``Follow_Up.preciseOriginTimestamp`` and Message Header inclusive ``CrcCorrectionField``
   #. If ``Follow_Up``: Calculate IEEE *TLV*
   #. If ``Follow_Up``: Calculate AUTOSAR *TLV* (configuration dependent)

   For 4: Calculate *CRC* (configuration dependent) and copy all data to the appropriate position within the related message
   
   (`RS_TS_20061`_, `RS_TS_20062`_, `RS_TS_20063`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_000104
      
      <Verification criteria text>
   

Acting as Time Slave
--------------------

A Time Slave is an entity, which is the recipient for a certain Time Base within a certain segment of a communication network, being a consumer for this Time Base .


Message processing
^^^^^^^^^^^^^^^^^^

.. sw_req:: PRS_TS_00023
   :id: PRS_TS_00023
   :status: new/changed
   :safety_level: QM
   
   The Time Slave shall support the reception of ``Sync`` and ``Follow_Up`` according `DOC_IEEEStandard8021AS30`_ as well as the transmission and reception of ``Pdelay_Req, Pdelay_Resp and Pdelay_Resp_Follow_Up``, [:need:`PRS_TS_00140`], [:need:`PRS_TS_00141`],[:need:`PRS_TS_00004`].
   
   (`RS_TS_20048`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00023
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00025
   :id: PRS_TS_00025
   :status: new/changed
   :safety_level: QM
   
   For each configured Time Slave the Ethernet module shall observe the reception timeout ``GlobalTimeFollowUpTimeout`` between the ``Sync`` and its ``Follow_Up``. If the reception timeout occurs, the sequence shall be reset (i.e. waiting for a new Sync). A value of 0 deactivates this timeout observation.
   
   (`RS_TS_20048`_, `RS_TS_20051`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00025
      
      <Verification criteria text>
   
**Note:** A timeout is detected when receiving the next subsequent Sync before receiving the ``Follow_Up`` belonging to the Sync before. The general timeout monitoring for the Time Base update is located in the Implementation of Time Synchronization and not in the provider modules.


Message Field Validation and Disassembling
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. sw_req:: PRS_TS_00105
   :id: PRS_TS_00105
   :status: new/changed
   :safety_level: QM
   
   If ``MessageCompliance`` is set to ``FALSE, RxCrcValidated`` shall be considered.
   
   (`RS_TS_20061`_, `RS_TS_20062`_, `RS_TS_20063`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00105
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00106
   :id: PRS_TS_00106
   :status: new/changed
   :safety_level: QM
   
   If ``MessageCompliance`` is set to ``FALSE``, a Time Slave shall check if an AUTOSAR *TLV* in the ``Follow_Up`` frame exists.
   
   (`RS_TS_20061`_, `RS_TS_20062`_, `RS_TS_20063`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00106
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00107
   :id: PRS_TS_00107
   :status: new/changed
   :safety_level: QM
   
   The *CRC* of the ``Follow_Up`` *TLV* shall be validated, depending on ``RxCrcValidated`` and the ``Follow_Up.TLV[AUTOSAR].Sub-TLV.Type`` acc. to:
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00107
      
      <Verification criteria text>
   

.. _table_3a_usecases5:
   
   +--------------------+-----------------------------------------------+-----------------------------------------------+
   |                    |                                               | ``Sub-TLV.Type``                              |
   +====================+===============================================+===============================================+
   | ``RxCrcValidated`` | ``CRC_VALIDATED``                             | ``CRC_NOT_VALIDATED``                         |
   +--------------------+-----------------------------------------------+-----------------------------------------------+
   |                    | ``0x28`` Sub-TLV: Time Secured is CRC secured | ``n.a.``                                      |
   +--------------------+-----------------------------------------------+-----------------------------------------------+
   |                    | ``0x50`` Sub-TLV: Status is CRC secured       | ``0x51`` Sub-TLV: Status is not CRC secured   |
   +--------------------+-----------------------------------------------+-----------------------------------------------+
   |                    | ``0x60`` Sub-TLV: UserData is CRC secured     | ``0x61`` Sub-TLV: UserData is not CRC secured |
   +--------------------+-----------------------------------------------+-----------------------------------------------+
   |                    | ``0x44`` Sub-TLV: OFS is CRC secured          | ``0x34`` Sub-TLV: OFS is not CRC secured      |
   +--------------------+-----------------------------------------------+-----------------------------------------------+

.. sw_req:: PRS_TS_00108
   :id: PRS_TS_00108
   :status: new/changed
   :safety_level: QM
   
   The *CRC* of the ``Follow_Up`` *TLV* shall be ignored, if ``RxCrcValidated`` is set to ``CRC_IGNORED`` and the ``Follow_Up.TLV[AUTOSAR].Sub-TLV.Type`` contains any of the following defined values:
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00108
      
      <Verification criteria text>
   

.. _table_3a_usecases6:
   
   +--------------------+-----------------------------------------------------------------------------------------------+
   |                    | ``Sub-TLV.Type``                                                                              |
   +====================+===============================================================================================+
   | ``RxCrcValidated`` | ``CRC_IGNORED``                                                                               |
   +--------------------+-----------------------------------------------+-----------------------------------------------+
   |                    | ``0x28`` Sub-TLV: Time Secured is CRC secured | ``n.a.``                                      |
   +--------------------+-----------------------------------------------+-----------------------------------------------+
   |                    | ``0x50`` Sub-TLV: Status is CRC secured       | ``0x51`` Sub-TLV: Status is not CRC secured   |
   +--------------------+-----------------------------------------------+-----------------------------------------------+
   |                    | ``0x60`` Sub-TLV: UserData is CRC secured     | ``0x61`` Sub-TLV: UserData is not CRC secured |
   +--------------------+-----------------------------------------------+-----------------------------------------------+
   |                    | ``0x44`` Sub-TLV: OFS is CRC secured          | ``0x34`` Sub-TLV: OFS is not CRC secured      |
   +--------------------+-----------------------------------------------+-----------------------------------------------+

.. sw_req:: PRS_TS_00109
   :id: PRS_TS_00109
   :status: new/changed
   :safety_level: QM
   
   The *CRC* of the ``Follow_Up`` *TLV* shall be either validated or not validated, if ``RxCrcValidated`` is set to ``CRC_OPTIONAL`` and the ``Follow_Up.TLV[AUTOSAR].Sub-TLV.Type`` contains any of the following defined values:
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00109
      
      <Verification criteria text>
   

.. _table_3a_usecases7:
   
   +--------------------+-----------------------------------------------------------------------------------------------+
   |                    | ``Sub-TLV.Type``                                                                              |
   +====================+===============================================================================================+
   | ``RxCrcValidated`` | ``CRC_OPTIONAL``                                                                              |
   +                    +-----------------------------------------------+-----------------------------------------------+
   |                    | CRC shall be validated                        | CRC shall not be validated                    |
   +--------------------+-----------------------------------------------+-----------------------------------------------+
   |                    | ``0x28`` Sub-TLV: Time Secured is CRC secured | ``n.a.``                                      |
   +--------------------+-----------------------------------------------+-----------------------------------------------+
   |                    | ``0x50`` Sub-TLV: Status is CRC secured       | ``0x51`` Sub-TLV: Status is not CRC secured   |
   +--------------------+-----------------------------------------------+-----------------------------------------------+
   |                    | ``0x60`` Sub-TLV: UserData is CRC secured     | ``0x61`` Sub-TLV: UserData is not CRC secured |
   +--------------------+-----------------------------------------------+-----------------------------------------------+
   |                    | ``0x44`` Sub-TLV: OFS is CRC secured          | ``0x34`` Sub-TLV: OFS is not CRC secured      |
   +--------------------+-----------------------------------------------+-----------------------------------------------+


.. _chap_3a_SGW_Calion__651176da:

SGW Calculation
"""""""""""""""

.. sw_req:: PRS_TS_00211
   :id: PRS_TS_00211
   :status: new/changed
   :safety_level: QM
   
   If ``MessageCompliance`` is set to ``TRUE`` the ``SYNC_TO_GATEWAY`` bit within ``timeBaseStatus`` shall be set to zero.
   
   (`RS_TS_20054`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00211
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00156
   :id: PRS_TS_00156
   :status: new/changed
   :safety_level: QM
   
   For a Synchronized Time Base and if ``MessageCompliance`` is set to ``FALSE`` the *SGW* value (Time Gateway synchronization status) shall be retrieved from the Status element of the *AUTOSAR Sub-TLV*: Status if an *AUTOSAR TLV* in the ``Follow_Up`` message exists and if this *Sub-TLV* is part of the *AUTOSAR TLV*. If the SGW value is set to ``SyncToSubDomain``, the ``SYNC_TO_GATEWAY`` bit within ``timeBaseStatus`` shall be set to one. Otherwise, it shall be set to zero.
   
   (`RS_TS_20054`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00156
      
      <Verification criteria text>
   
**Note**: Since a Global Time Master will not set the Time Gateway synchronization status to ``SYNC_TO_GATEWAY`` it is superfluous to transmit an *AUTOSAR Sub-TLV*: Status in this case.

.. sw_req:: PRS_TS_00212
   :id: PRS_TS_00212
   :status: new/changed
   :safety_level: QM
   
   If ``MessageCompliance`` is set to ``FALSE`` and if an *AUTOSAR Sub-TLV:* Status in the ``Follow_Up`` message does not exist, the ``SYNC_TO_GATEWAY`` bit within ``timeBaseStatus`` shall be set to zero.
   
   (`RS_TS_20054`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00212
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00213
   :id: PRS_TS_00213
   :status: new/changed
   :safety_level: QM
   
   If ``MessageCompliance`` is set to ``FALSE`` and if an *AUTOSAR TLV* in the ``Follow_Up`` message exists the *SGW* value (Time Gateway synchronization status) shall be retrieved from the Status element of each *AUTOSAR Sub-TLV*: ``OFS`` that is part of the *AUTOSAR TLV*. If the *SGW* value is set to ``SyncToSubDomain``, the ``SYNC_TO_GATEWAY`` bit within ``timeBaseStatus`` shall be set to one. Otherwise, it shall be set to zero.
   
   (`RS_TS_20054`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00213
      
      <Verification criteria text>
   

.. _chap_3a_OFS_Calion__bee4bc18:

OFS Calculation
"""""""""""""""

.. sw_req:: PRS_TS_00110
   :id: PRS_TS_00110
   :status: new/changed
   :safety_level: QM
   
   The Time Slave of an Offset Time Base shall calculate the Offset Time Base from the ``OfsTimeSec`` element of the corresponding AUTOSAR *Sub-TLV*: OFS and the ``OfsTimeNSec`` element of the corresponding AUTOSAR *Sub-TLV*: OFS.
   
   (`RS_TS_20063`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00110
      
      <Verification criteria text>
   

CRC Validation
""""""""""""""

.. sw_req:: PRS_TS_00112
   :id: PRS_TS_00112
   :status: new/changed
   :safety_level: QM
   
   _`crcvalidation`\ The ``DataID`` shall be calculated as: ``DataID = DataIDList[Follow_Up.sequenceId mod 16]``, where ``DataIDList`` is given by configuration for the ``Follow_Up``.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00112
      
      <Verification criteria text>
   
**Note:** A specific ``DataID`` out of a predefined ``DataIDList`` ensures the identification of data elements of Time Synchronization messages.

.. sw_req:: PRS_TS_00183
   :id: PRS_TS_00183
   :status: new/changed
   :safety_level: QM
   
   If applying the *CRC* calculation on multibyte values, the byte order shall be such that the byte containing the most significant bit of the value shall be used first.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00183
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00185
   :id: PRS_TS_00185
   :status: new/changed
   :safety_level: QM
   
   If applying the *CRC* calculation on multibyte message data, the byte order shall be in ascending order of the octets, i.e., the octet with the lowest offset shall be used first.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00185
      
      <Verification criteria text>
   

.. _chap_3a_AUTSAR_TLV_SubLV_3a__Time_Secred__24fc487a:

AUTOSAR TLV Sub-TLV: Time Secured
"""""""""""""""""""""""""""""""""

.. sw_req:: PRS_TS_00157
   :id: PRS_TS_00157
   :status: new/changed
   :safety_level: QM
   
   If ``RxCrcValidated`` is set to ``CRC_VALIDATED`` or ``CRC_OPTIONAL``, the Time Slave shall validate the *CRC* as defined in ``CrcFlagsRxValidated`` acc. to the following rule.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00157
      
      <Verification criteria text>
   

.. _table_3a_usecases8:
   
   +--------------------------------------+-----------------------------------------------------------------+
   |                                      | Validate if ``CrcFlagsRxValidated`` element is set to ``TRUE``: |
   +======================================+===================================+=============================+
   | Element                              | ``Follow_Up`` Message Header      | ``Follow_Up`` Message Field |
   +--------------------------------------+-----------------------------------+-----------------------------+
   | ``CrcMessageLength``                 | ``messageLength``                 | ``n.a.``                    |
   +--------------------------------------+-----------------------------------+-----------------------------+
   | ``CrcDomainNumber``                  | ``domainNumber``                  | ``n.a.``                    |
   +--------------------------------------+-----------------------------------+-----------------------------+
   | ``CrcCorrectionField``               | ``CrcCorrectionField``            | ``n.a.``                    |
   +--------------------------------------+-----------------------------------+-----------------------------+
   | ``CrcSourcePortIdentity``            | ``sourcePortIdentity``            | ``n.a.``                    |
   +--------------------------------------+-----------------------------------+-----------------------------+
   | ``CrcSequenceId``                    | ``sequenceId``                    | ``n.a.``                    |
   +--------------------------------------+-----------------------------------+-----------------------------+
   | ``CrcPrecise`` - ``OriginTimestamp`` | ``n.a.``                          | ``preciseOriginTimestamp``  |
   +--------------------------------------+-----------------------------------+-----------------------------+

.. sw_req:: PRS_TS_00113
   :id: PRS_TS_00113
   :status: new/changed
   :safety_level: QM
   
   If ``RxCrcValidated`` is set to ``CRC_VALIDATED`` or ``CRC_OPTIONAL``, , the Time Slave shall validate the *CRC* for ``CRC_Time_0`` by considering the contents of ``CRC_Time_Flags`` itself, the contents of the dependent fields as defined in ``CrcFlagsRxValidated`` acc. to the rule in the table below and the ``DataID``. The data elements used for the calculation and thus validation of the *CRC* shall apply the following order:
   
   #. the value of ``CRC_Time_Flags``
   #. the ``domainNumber`` inside the ``Follow_Up`` Message Header, if ``CrcDomainNumber`` is set to ``TRUE``
   #. the ``preciseOriginTimestamp`` inside the ``Follow_Up`` Message Field, if ``CrcPreciseOriginTimestamp``\ is set to ``TRUE``
   #. the ``DataID`` (refer to [:need:`PRS_TS_00112`])
   #. the ``sourcePortIdentity`` inside the ``Follow_Up`` Message Header, if ``CrcSourcePortIdentity`` is set to ``TRUE``

   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00113
      
      <Verification criteria text>
   

.. _table_3a_usecases9:
   
   +--------------------------------------------------------+------------------------------------------------------------+
   |                                                        | For ``CRC_Time_0`` verification required contents:         |
   +========================================================+==============================+=============================+
   | If ``CrcFlagsRxValidated`` element is set to ``TRUE``: | ``Follow_Up`` Message Header | ``Follow_Up`` Message Field |
   +--------------------------------------------------------+------------------------------+-----------------------------+
   | ``CrcMessageLength``                                   | ``n.a.``                     | ``n.a.``                    |
   +--------------------------------------------------------+------------------------------+-----------------------------+
   | ``CrcDomainNumber``                                    | ``domainNumber``             | ``n.a.``                    |
   +--------------------------------------------------------+------------------------------+-----------------------------+
   | ``CrcCorrectionField``                                 | ``n.a.``                     | ``n.a.``                    |
   +--------------------------------------------------------+------------------------------+-----------------------------+
   | ``CrcSourcePortIdentity``                              | ``sourcePortIdentity``       | ``n.a.``                    |
   +--------------------------------------------------------+------------------------------+-----------------------------+
   | ``CrcSequenceId``                                      | ``n.a.``                     | ``n.a.``                    |
   +--------------------------------------------------------+------------------------------+-----------------------------+
   | ``CrcPrecise`` - ``OriginTimestamp``                   | ``n.a.``                     | ``preciseOriginTimestamp``  |
   +--------------------------------------------------------+------------------------------+-----------------------------+

.. sw_req:: PRS_TS_00114
   :id: PRS_TS_00114
   :status: new/changed
   :safety_level: QM
   
   If ``RxCrcValidated`` is set to ``CRC_VALIDATED`` or ``CRC_OPTIONAL``, the Time Slave shall validate the *CRC* for ``CRC_Time_1`` by considering the contents of ``CRC_Time_Flags`` itself, the contents of the dependent fields as defined in ``CrcFlagsRxValidated`` acc. to the rule in the table below and the ``DataID``. The data elements used for the calculation and thus validation of the *CRC* shall apply the following order:
   
   #. the value of ``CRC_Time_Flags``
   #. the ``messageLength`` inside the ``Follow_Up`` Message Header, if ``CrcMessageLength`` is set to ``TRUE``
   #. the ``CrcCorrectionField`` inside the ``Follow_Up`` Message Header, if ``CrcCorrectionField`` is set to ``TRUE``
   #. the ``sequenceId`` inside the ``Follow_Up`` Message Field, if ``CrcSequenceId`` is set to ``TRUE``
   #. the ``DataID`` (refer to [:need:`PRS_TS_00112`])

   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00114
      
      <Verification criteria text>
   

.. _table_3a_usecases10:
   
   +--------------------------------------------------------+------------------------------------------------------------+
   |                                                        | For ``CRC_Time_1`` verification required contents:         |
   +========================================================+==============================+=============================+
   | If ``CrcFlagsRxValidated`` element is set to ``TRUE``: | ``Follow_Up`` Message Header | ``Follow_Up`` Message Field |
   +--------------------------------------------------------+------------------------------+-----------------------------+
   | ``CrcMessageLength``                                   | ``messageLength``            | ``n.a.``                    |
   +--------------------------------------------------------+------------------------------+-----------------------------+
   | ``CrcDomainNumber``                                    | ``n.a.``                     | ``n.a.``                    |
   +--------------------------------------------------------+------------------------------+-----------------------------+
   | ``CrcCorrectionField``                                 | ``CrcCorrectionField``       | ``n.a.``                    |
   +--------------------------------------------------------+------------------------------+-----------------------------+
   | ``CrcSourcePortIdentity``                              | ``n.a.``                     | ``n.a.``                    |
   +--------------------------------------------------------+------------------------------+-----------------------------+
   | ``CrcSequenceId``                                      | ``sequenceId``               | ``n.a.``                    |
   +--------------------------------------------------------+------------------------------+-----------------------------+
   | ``CrcPrecise`` - ``OriginTimestamp``                   | ``n.a.``                     | ``n.a.``                    |
   +--------------------------------------------------------+------------------------------+-----------------------------+


.. _chap_3a_AUTSAR_TLV_SubLV_3a__Status_secred__46cea86d:

AUTOSAR TLV Sub-TLV: Status secured
"""""""""""""""""""""""""""""""""""

.. sw_req:: PRS_TS_00115
   :id: PRS_TS_00115
   :status: new/changed
   :safety_level: QM
   
   If ``RxCrcValidated`` is set to ``CRC_VALIDATED`` or ``CRC_OPTIONAL``, the Time Slave shall validate the *CRC* for ``CRC_Status`` by considering the contents of ``Status`` and ``DataID`` (in this order).
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00115
      
      <Verification criteria text>
   

.. _chap_3a_AUTSAR_TLV_SubLV_3a__Useata_secred__b15c5f87:

AUTOSAR TLV Sub-TLV: UserData secured
"""""""""""""""""""""""""""""""""""""

.. sw_req:: PRS_TS_00116
   :id: PRS_TS_00116
   :status: new/changed
   :safety_level: QM
   
   If ``RxCrcValidated`` is set to ``CRC_VALIDATED`` or ``CRC_OPTIONAL``, the Time Slave shall validate the *CRC* for ``CRC_UserData`` by considering the contents of ``UserDataLength, UserByte_0, UserByte_1, UserByte_2 and DataID`` (in this order).
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00116
      
      <Verification criteria text>
   

.. _chap_3a_AUTSAR_TLV_SubLV_3a__OFS_secred__5115d642:

AUTOSAR TLV Sub-TLV: OFS secured
""""""""""""""""""""""""""""""""

.. sw_req:: PRS_TS_00117
   :id: PRS_TS_00117
   :status: new/changed
   :safety_level: QM
   
   If ``RxCrcValidated`` is set to ``CRC_VALIDATED`` or ``CRC_OPTIONAL``, the Time Slave shall validate the *CRC* for ``CRC_OFS`` by considering the contents of ``OfsTimeDomain, OfsTimeSec, OfsTimeNSec, Status, UserDataLength, UserByte_0, UserByte_1, UserByte_2 and DataID`` (in this order).
   
   (`RS_TS_20061`_, `RS_TS_20063`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00117
      
      <Verification criteria text>
   

Sequence Counter (sequenceId) Validation
""""""""""""""""""""""""""""""""""""""""

.. sw_req:: PRS_TS_00192
   :id: PRS_TS_00192
   :status: new/changed
   :safety_level: QM
   
   If the Sequence Counter (\ ``sequenceId``\ ) of a received ``Pdelay_Resp`` message does not match the Sequence Counter (\ ``sequenceId``\ ) of the corresponding ``Pdelay_Req`` message, the Peer Delay Initiator shall ignore the Pdelay_RResp message.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00192
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00193
   :id: PRS_TS_00193
   :status: new/changed
   :safety_level: QM
   
   The Peer Delay Initiator shall ignore a ``Pdelay_Resp`` message, if the ``Pdelay_Resp`` message has not been received within the timeout interval ``GlobalTimePdelayRespAndRespFollowUpTimeout``.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00193
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00194
   :id: PRS_TS_00194
   :status: new/changed
   :safety_level: QM
   
   If the Sequence Counter (\ ``sequenceId``\ ) of a received ``Pdelay_Resp_Follow_Up`` message does not match the Sequence Counter (\ ``sequenceId``\ ) of the transmitted ``Pdelay_Req`` message, the Peer Delay Initiator shall ignore the received ``Pdelay_Resp_Follow_Up`` message.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00194
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00195
   :id: PRS_TS_00195
   :status: new/changed
   :safety_level: QM
   
   The Peer Delay Initiator shall discard the content of a ``Pdelay_Resp`` message, if no ``Pdelay_Resp_Follow_Up`` message with a matching Sequence Counter (\ ``sequenceId``\ ) has been received within the timeout interval ``GlobalTimePdelayRespAndRespFollowUpTimeout``.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00195
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00196
   :id: PRS_TS_00196
   :status: new/changed
   :safety_level: QM
   
   _`target_3a_PRSTS00196`\ If the Sequence Counter (\ ``sequenceId``\ ) of a received Follow_Up message does not match the Sequence Counter (\ ``sequenceId``\ ) of the previously received Sync message of the same Time Domain (\ ``domainNumber``\ ), the Time Slave shall ignore the Follow_Up message.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00196
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00197
   :id: PRS_TS_00197
   :status: new/changed
   :safety_level: QM
   
   If no Follow_Up message with a matching Sequence Counter (\ ``sequenceId``\ ) and Time Domain (\ ``domainNumber``\ ) has been received within the timeout interval ``GlobalTimeFollowUpTimeout``, the Time Slave shall discard the contents of the already received Sync message.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00197
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00198
   :id: PRS_TS_00198
   :status: new/changed
   :safety_level: QM
   
   If the Sequence Counter Jump Width between two consecutive Sync messages of the same Time Domain (\ ``domainNumber``\ ) is 0 or greater than ``GlobalTimeSequenceCounterJumpWidth``, the Time Slave shall ignore the Sync message. If ``GlobalTimeSequenceCounterJumpWidth`` is set to its maximum allowed value, the Time Slave shall accept any jump width greater than 0.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00198
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00199
   :id: PRS_TS_00199
   :status: new/changed
   :safety_level: QM
   
   A Time Slave shall check the Sequence Counter (\ ``sequenceId``\ ) of a received Sync message per Time Domain (\ ``domainNumber``\ ) against the configured value of ``GlobalTimeSequenceCounterJumpWidth``, unless it is the first Sync message after Initialization or after a Synchronization Timeout.
   
   (`RS_TS_20061`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00199
      
      <Verification criteria text>
   
**Note:** A Synchronization Timeout means, that a Time Slave could not (re-)synchronize within a given timeout interval due to missing or invalid ``Sync`` or Follow_up messages.

**Note:** There are scenarios when it makes sense to skip the check of the Sequence Counter Jump Width, e.g. at startup (Time Slaves start asynchronously to the Time Master) or after a message timeout to allow for Sequence Counter (re-)synchronization. In case of a timeout the error has been detected already by the timeout monitoring, there is no benefit in generating a subsequent error by the jump width check.

**Note:** During Time Base update timeout the Sequence Counter validation will still discard messages with a Sequence Counter Jump Width being zero (i.e., stuck Sequence Counter).


Message Disassembling
"""""""""""""""""""""

.. sw_req:: PRS_TS_00118
   :id: PRS_TS_00118
   :status: new/changed
   :safety_level: QM
   
   If the Type of a *Sub-TLV* cannot be recognized at the receiver side, it shall be ignored and the next subsequent *Sub-TLV* shall be evaluated.
   
   (`RS_TS_20061`_, `RS_TS_20062`_, `RS_TS_20063`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00118
      
      <Verification criteria text>
   
**Note:** The Length field of each *Sub-TLV* is always at the same position within each *Sub-TLV*. It will be used to jump over the unknown *Sub-TLV* to the next Type field.

.. sw_req:: PRS_TS_00119
   :id: PRS_TS_00119
   :status: new/changed
   :safety_level: QM
   
   If any of the following conditions is not met, a Time Slave shall discard a received ``Sync`` or ``Follow_Up`` message:
   
   #. Validation of Sequence Counter (\ ``sequenceId``\ ) is successful (refer to: [:need:`PRS_TS_00196`], [:need:`PRS_TS_00197`], [:need:`PRS_TS_00198`], [:need:`PRS_TS_00199`]).
   #. If ``Follow_Up``: ``Follow_Up.TLV[AUTOSAR].Sub-TLV.Type`` matches depending on configuration of ``RxCrcValidated``
   #. The Time Domain matches to the defined Time Domain range for each ``domainNumber`` resp. to the element *OfsTimeDomain* of the AUTOSAR *Sub-TLV*: OFS (configuration dependent).
   #. The Time Domain matches to one of the configured Time Domains
   #. If ``Follow_Up``: The element ``OfsTimeNSec`` of the AUTOSAR *Sub-TLV*: OFS is within the allowed range of ``0..999999999``.
   #. If *Follow_Up*: All *CRCs* are successfully validated depending on the configuration of ``RxCrcValidated`` and ``CrcFlagsRxValidated``.

   (`RS_TS_20061`_, `RS_TS_20062`_, `RS_TS_20063`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00119
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00120
   :id: PRS_TS_00120
   :status: new/changed
   :safety_level: QM
   
   For each received Time Synchronization message, the Time synchronization protocol shall disassemble the message after successful validation.
   
   (`RS_TS_20061`_, `RS_TS_20062`_, `RS_TS_20063`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00120
      
      <Verification criteria text>
   

Time measurement with Switches
==============================

In a time aware Ethernet network, two basic HW types of control units exists:

#. Endpoints directly working on a local Ethernet-Controller
#. Time Gateways, resp. Time Aware Bridges, where the local Ethernet-Controller connects to an external Switch device.

The extension "Time measurement with Switches" focusses on 2. A Switch device leads to additional delays, which have to be considered for the calculation of the corresponding Time Base. Additionally, the support of time stamping in HW is a Switch-Port specific feature, which leads to an extension of the used function APIs. These APIs enabling a Switch port specific detection of ingress and egress messages together with a given timestamp, if enabled.

If the Switch Management and Global Time support is implemented as a part of the program running on the Switch HW, this will not be considered by 2. For this case, the behavior can be seen as described in 1.

.. sw_req:: PRS_TS_00053
   :id: PRS_TS_00053
   :status: new/changed
   :safety_level: QM
   
   Time measurement with Switches supports the use case "Time Aware Bridge with GTM as Management CPU" like shown in Figure ``Figure 5.2``.
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00053
      
      <Verification criteria text>
   
.. _fig_3a_TimestampingSequence:

.. figure:: img/TimeAwareBridgeWithGTMAsManagementCPU.png
   
   Time Aware Bridge with GTM as Management CPU
   
_`Figure 5.2`

.. sw_req:: PRS_TS_00054
   :id: PRS_TS_00054
   :status: new/changed
   :safety_level: QM
   
   Time measurement with Switches supports the use case "Time Aware Bridge with GTM not as Management CPU" like shown in ``Figure 5.3``.
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00054
      
      <Verification criteria text>
   
.. _fig_3a_TimestampingSequenceNotManaged:

.. figure:: img/TimeAwareBridgeWithGTMNotAsManagementCPU.png
   
   Time Aware Bridge with GTM not as Management CPU
   
_`Figure 5.3`


Pdelay and Time Synchronization measurement point
=================================================

.. sw_req:: PRS_TS_00055
   :id: PRS_TS_00055
   :status: new/changed
   :safety_level: QM
   
   The path delay measurement will be done always as Port-to-Port measurement like specified in in `DOC_IEEEStandard8021AS30`_ chapter 11.1.2 Propagation delay measurement for the device external Ethernet path.
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00055
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00056
   :id: PRS_TS_00056
   :status: new/changed
   :safety_level: QM
   
   The inner delay of the Ethernet path (Residence Time) is determined at the time where ``Sync`` is received and transmitted, by using the message specific ingress and egress timestamps.
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00056
      
      <Verification criteria text>
   
**Note:** This belongs to the fact, that the Residence Time might be discontinuous, depending on the current busload, while ``Sync`` messages are transmitted / received, the Switch HW architecture and the message forwarding method. A static delay measurement method for this part of the communication path might lead to an unprecise time measurement. Nevertheless, static Residence Time parameters are considered by this specification, to increase the performance while calculating the Global Time resp. the ``CrcCorrectionField`` and the flexibility to support different Switch devices, such as Switches, which do not support time stamping on each ingress or egress port.


.. _chap_3a_Time_Aware_Bridge_with_GTM_as_Manent_CPU__f79ffb40:

Time Aware Bridge with GTM as Management CPU
============================================

.. sw_req:: PRS_TS_00057
   :id: PRS_TS_00057
   :status: new/changed
   :safety_level: QM
   
   Time measurement with Switches supporting the use case "Time Aware Bridge with GTM as Management CPU" following the given timestamping points like shown in ``Figure 5.4``\ and ``Figure 5.5``
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00057
      
      <Verification criteria text>
   
.. _`Sync/Follow_Up message flow with Timestamping points for Sync for Time Aware Bridge with GTM as Management CPU`:

.. figure:: img/SyncGTM.png
   
   Sync/Follow_Up message flow with Timestamping points for Sync for Time Aware Bridge with GTM as Management CPU
   
_`Figure 5.4`

.. _`Pdelay message flow with Timestamping points for Time Aware Bridge with GTM as Management CPU`:

.. figure:: img/PDelayGTM.png
   
   Pdelay message flow with Timestamping points for Time Aware Bridge with GTM as Management CPU
   
_`Figure 5.5`

**Note:** The picture ``Figure 5.4`` and Figure ``Figure 5.5`` shows an example Port selection as simplification.

.. sw_req:: PRS_TS_00058
   :id: PRS_TS_00058
   :status: new/changed
   :safety_level: QM
   
   Time measurement with Switches supporting the use case "Time Aware Bridge with GTM as Management CPU" considers the inner Switch delay by a modification of the ``CrcCorrectionField`` as well as ``Pdelay`` timestamping for ``requestReceiptTimestamp`` and ``responseOriginTimestamp`` like shown in ``Figure 5.6``.
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00058
      
      <Verification criteria text>
   
.. _`Timestamping sequence for Time Aware Bridge with GTM as Management CPU`:

.. figure:: img/TimestampingSequence.png
   
   Timestamping sequence for Time Aware Bridge with GTM as Management CPU
   
_`Figure 5.6`

**Note:** The calculation in ``Figure 5.6`` shows an example Port selection as simplification.

.. sw_req:: PRS_TS_00166
   :id: PRS_TS_00166
   :status: new/changed
   :safety_level: QM
   
   If ``GlobalTimeUplinkToTxSwitchResidenceTime`` is set to 0, the Ethernet module shall ignore this parameter and measure the inner delay of the Switch egress Ethernet path (Uplink to Tx Residence Time (\ **T3 - T2**\ )) by using always the ingress (\ **T2**\ ) and egress (\ **T3**\ ) timestamp as given in ``Figure 5.6``.
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00166
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00167
   :id: PRS_TS_00167
   :status: new/changed
   :safety_level: QM
   
   If ``GlobalTimeUplinkToTxSwitchResidenceTime`` is greater than 0, the Ethernet module shall use this parameter as value for the inner delay of the Switch egress Ethernet path (Uplink to Tx Residence Time (\ **T3 - T2**\ )) instead of using the measurement method described in [:need:`PRS_TS_00166`].
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00167
      
      <Verification criteria text>
   

.. _chap_3a_Time_Aware_Bridge_with_GTM_not_as_Manent_CPU__b2f1bba9:

Time Aware Bridge with GTM not as Management CPU
================================================

.. sw_req:: PRS_TS_00059
   :id: PRS_TS_00059
   :status: new/changed
   :safety_level: QM
   
   Time measurement with Switches supporting the use case Time Aware Bridge with GTM not as Management CPU following the given timestamping points like shown in ``Figure 5.7`` and ``Figure 5.8``.
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00059
      
      <Verification criteria text>
   
.. _`Sync/Follow_Up message flow with Timestamping points for Sync for Time Aware Bridge with GTM not as Management CPU`:

.. figure:: img/SyncGTMNotManaged.png
   
   Sync/Follow_Up message flow with Timestamping points for Sync for Time Aware Bridge with GTM not as Management CPU
   
_`Figure 5.7`

.. _`Pdelay message flow with Timestamping points for Time Aware Bridge with GTM not as Management CPU`:

.. figure:: img/PdelayGTMNotManaged.png
   
   Pdelay message flow with Timestamping points for Time Aware Bridge with GTM not as Management CPU
   
_`Figure 5.8`

.. sw_req:: PRS_TS_00060
   :id: PRS_TS_00060
   :status: new/changed
   :safety_level: QM
   
   Time measurement with Switches supporting the use case Time Aware Bridge with GTM not as Management CPU considers the inner Switch delay by a modification of the ``CrcCorrectionField`` as well as ``Pdelay`` ``timestamping`` for ``requestReceiptTimestamp`` and ``responseOriginTimestamp``.
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00060
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00207
   :id: PRS_TS_00207
   :status: new/changed
   :safety_level: QM
   
   If the ``Follow_Up`` message contains an AUTOSAR *TLV*, which contains a *Sub-TLV*: Time Secured it shall be checked, if the element ``CRC_Time_Flags`` contains BitMask ``0x04`` (i.e., the content of ``CrcCorrectionField`` is *CRC* protected). If this bit is set then the validation of the ``CRC_Time_1`` element shall be done as follows: The *CRC* Validation shall be done as specified in section `crcvalidation`_. The data elements used for the calculation and thus validation of the CRC shall be applied with the following order:
   
   #. the value of ``CRC_Time_Flags``
   #. the ``length of the message`` inside the ``Follow_Up`` Message Header, if the element ``CRC_Time_Flags`` contains BitMask ``0x01``
   #. the ``CrcCorrectionField`` inside the ``Follow_Up`` Message Header
   #. the ``CRCsequenceId`` inside the ``Follow_Up`` Message Header, if the element ``CRC_Time_Flags`` contains BitMask ``0x10``
   #. the *DataID*

   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00207
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00208
   :id: PRS_TS_00208
   :status: new/changed
   :safety_level: QM
   
   If the validation ``CRC`` validation of an *AUTOSAR TLV* fails, the ``Follow_Up`` message shall be dropped instead of being forwarded.
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00208
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00209
   :id: PRS_TS_00209
   :status: new/changed
   :safety_level: QM
   
   If the validation ``CRC`` validation of an *AUTOSAR TLV* is successful, the ``CrcCorrectionField`` shall be modified and the element ``CRC_Time`` inside the *Sub-TLV*: Time Secured shall be calculated according to the content of the ``CRC_Time_Flags`` element.
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00209
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00168
   :id: PRS_TS_00168
   :status: new/changed
   :safety_level: QM
   
   If ``rx_residence_time`` is set to 0, the Time Synchronization over Ethernet shall ignore this parameter and measure the inner delay of the Switch ingress Ethernet path (Rx to Uplink Residence Time (T5 - T4)) by using always the ingress (T4) and egress (T5) timestamp.
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00168
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00171
   :id: PRS_TS_00171
   :status: new/changed
   :safety_level: QM
   
   If ``rx_residence_time`` is greater than 0, the Time Synchronization over Ethernet shall use this parameter as value for the inner delay of the Switch ingress Ethernet path (Rx to Uplink Residence Time (T5 - T4)) instead of using the measurement method.
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00171
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00169
   :id: PRS_TS_00169
   :status: new/changed
   :safety_level: QM
   
   If ``rx_residence_time`` and ``tx_residence_time`` are set to 0, the Ethernet module shall ignore both parameter and measure the inner delay of the Switch ingress and egress Ethernet path (Rx to Uplink and Uplink to Tx Residence Time (T7 to T4)) by using always the ingress (T4) and egress (T7) timestamp.
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00169
      
      <Verification criteria text>
   
.. sw_req:: PRS_TS_00170
   :id: PRS_TS_00170
   :status: new/changed
   :safety_level: QM
   
   If ``rx_residence_time`` and ``tx_residence_time`` are greater than 0, the Ethernet module shall use the sum of both parameter for the value of the inner delay of the Switch ingress and egress Ethernet path (Rx to Uplink and Uplink to Tx Residence Time (T7 to T4)) instead of using the measurement method
   
   (`RS_TS_20048`_, `RS_TS_20059`_)
   
   .. verify:: <VC Head>
      :id: VC_PRS_TS_00170
      
      <Verification criteria text>
   
**Note:** A separate Uplink to Tx Residence Time (T7 to T\ :sub:`UplinkMmCpu`\ ) replacement by using ``tx_residence_time`` might be also possible, but is not considered by the scenario.


Error messages
==============

Error handling is specified in the corresponding classic and adaptive platform documents.


************************
Configuration parameters
************************

The Following chapter summarizes all the configuration parameters that are used.


.. _table_3a_ConfigParams:
   
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | **Name**                                                                                  | **Description**                                                                                                                                                                                                                                                                                                        |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`CRCSupport`\ CRCSupport                                                                 | represents whether the configuration is supported or not                                                                                                                                                                                                                                                               |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`rx_residence_time`\ rx_residence_time                                                   | This parameter is specifying the default value used for the residence time                                                                                                                                                                                                                                             |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`tx_residence_time`\ tx_residence_time                                                   | This parameter is specifying the default value used for the residence time                                                                                                                                                                                                                                             |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`FramePrio`\ FramePrio                                                                   | This optional parameter, if present, indicates the priority of outgoing messages, if sent via VLAN (used for the 3-bit PCP field of the VLAN tag). If this optional parameter is not present, frames are sent without a priority and VLAN field.                                                                       |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`GlobalTimeTxPdelayReqPeriod`\ GlobalTimeTxPdelayReqPeriod                               | This parameter represents configuration of the TX period for Pdelay_Req messages. A value of 0 disables the cyclic Pdelay measurement.                                                                                                                                                                                 |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`PdelayLatencyThreshold`\ PdelayLatencyThreshold                                         | Threshold for calculated Pdelay. If a measured Pdelay exceeds PdelayLatencyThreshold, this value is discarded.                                                                                                                                                                                                         |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`PdelayRespAndResp`\ PdelayRespAndResp-                                                  |                                                                                                                                                                                                                                                                                                                        |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`FollowUpTimeout`\ FollowUpTimeout                                                       | Timeout value for Pdelay_Resp and Pdelay_Resp_Follow_Up after a Pdelay_Req has been transmitted resp. a Pdelay_Resp has been received. A value of 0 deactivates this timeout observation.                                                                                                                              |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`GlobalTimePropagationDelay`\ GlobalTimePropagationDelay                                 | If cyclic propagation delay measurement is enabled, this parameter represents the default value of the propagation delay until the first actually measured propagation delay is available. If cyclic propagation delay measurement is disabled, this parameter replaces a measured propagation delay by a fixed value. |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`GlobalTimePdelayRespEnable`\ GlobalTimePdelayRespEnable                                 | This parameter allows disabling Pdelay_Resp, Pdelay_Resp_Follow_Up transmission, if no Pdelay_Req messages are expected. FALSE: No Pdelay requests expected. Pdelay_Resp / Pdelay_Resp_Follow_Up transmission is disabled. TRUE: Pdelay requests expected. Pdelay_Resp, Pdelay_Resp_Follow_Up transmission is enabled. |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`GlobalTimeTxPeriod`\ GlobalTimeTxPeriod                                                 | This parameter represents configuration of the TX period.                                                                                                                                                                                                                                                              |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`GlobalTimeFollowUpTimeout`\ GlobalTimeFollowUpTimeout                                   | Timeout value of the Follow_Up message (of the subsequent Sync message). A value of 0 deactivates this timeout observation.                                                                                                                                                                                            |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`MasterSlaveConflictDetection`\ MasterSlaveConflictDetection                             | Enables master / slave conflict detection and notification. true: detection and notification is enabled. false: detection and notification is disabled.                                                                                                                                                                |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`MessageCompliance`\ MessageCompliance                                                   | true: IEEE 802.1AS compliant message format will be used. false: IEEE 802.1AS message format with AUTOSAR extension will be used.                                                                                                                                                                                      |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`RxCrcValidated`\ RxCrcValidated                                                         | - CRC_IGNORED (ignores any CRC inside the Sub-TLVs)                                                                                                                                                                                                                                                                    |
   |                                                                                           | - CRC_NOT_VALIDATED (If MessageCompliance is set to FALSE: Ethernet discards Follow_Up messages with Sub-TLVs of Type 0x28, 0x44, 0x50 or 0x60)                                                                                                                                                                        |
   |                                                                                           | - CRC_OPTIONAL ( If MessageCompliance is set to FALSE: Ethernet discards Follow_Up messages with Sub-TLVs of Type 0x28, 0x44, 0x50 or 0x60, that contain an incorrect CRC value.)                                                                                                                                      |
   |                                                                                           | - CRC_VALIDATED (If MessageCompliance is set to FALSE: Ethernet discards Follow_Up messages with Sub-TLVs of Type 0x28, 0x44, 0x50 or 0x60, that contain an incorrect CRC value. Ethernet rejects Follow_Up messages with Sub-TLVs of Type 0x34, 0x51 or 0x61)                                                         |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`CrcFlagsRxValidated`\ CrcFlagsRxValidated                                               | This container collects definitions which parts of the Follow_Up message elements shall be included in the CRC validation.                                                                                                                                                                                             |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`CrcMessageLength`\ CrcMessageLength                                                     | messageLength from the Follow_Up Message Header shall be included in CRC calculation.                                                                                                                                                                                                                                  |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`CrcDomainNumber`\ CrcDomainNumber                                                       | domainNumber from the Follow_Up Message Header shall be included in CRC calculation.                                                                                                                                                                                                                                   |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`CrcCorrectionField`\ CrcCorrectionField                                                 | correctionField from the Follow_Up Message Header shall be included in CRC calculation.                                                                                                                                                                                                                                |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`CrcSourcePortIdentity`\ CrcSourcePortIdentity                                           | sourcePortIdentity from the Follow_Up Message Header shall be included in CRC calculation.                                                                                                                                                                                                                             |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`CrcSequenceId`\ CrcSequenceId                                                           | sequenceId from the Follow_Up Message Header shall be included in CRC calculation.                                                                                                                                                                                                                                     |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`CrcPreciseOriginTimestamp`\ CrcPreciseOriginTimestamp                                   | preciseOriginTimestamp from the Follow_Up Message Field shall be included in CRC calculation.                                                                                                                                                                                                                          |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`GlobalTimeUplinkToTxSwitchResidenceTime`\ GlobalTimeUplinkToTxSwitchResidenceTime       | This parameter is specifying the default value used for the residence time of the Ethernet Switch [Uplink to Egress]. This value is used by the Ethernet module if the calculation of the residence time failed.                                                                                                       |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`TLVFollowUpTimeSubTLV`\ TLVFollowUpTimeSubTLV                                           | This represents the configuration of whether an AUTOSAR Follow_Up TLV Time Sub-TLV is used or not.                                                                                                                                                                                                                     |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`TLVFollowUpStatusSubTLV` TLVFollowUpStatusSubTLV                                        | This represents the configuration of whether an AUTOSAR Follow_Up TLV Status Sub-TLV is used or not.                                                                                                                                                                                                                   |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`TLVFollowUpUserDataSubTLV` TLVFollowUpUserDataSubTLV                                    | This represents the configuration of whether an AUTOSAR Follow_Up TLV UserData Sub-TLV is used or not.                                                                                                                                                                                                                 |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`TSynTLVFollowUpOFSSubTLV`\ TSynTLVFollowUpOFSSubTLV                                     | This represents the configuration of whether an AUTOSAR Follow_Up TLV OFS Sub-TLV is used or not.                                                                                                                                                                                                                      |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`CrcTimeFlagsTxSecured`\ CrcTimeFlagsTxSecured                                           | This item collects definitions which parts of the Follow_Up message elements shall be used for CRC calculation.                                                                                                                                                                                                        |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`CRCcorrectionField_duplicate`\ CRCcorrectionField_duplicate                             | correctionField from the Follow_Up Message Header shall be included in CRC calculation.                                                                                                                                                                                                                                |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`GlobalTimeTxCrcSecured`\ GlobalTimeTxCrcSecured                                         | This represents the configuration of whether or not CRC is supported.                                                                                                                                                                                                                                                  |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`GlobalTimeSequenceCounterJumpWidth`\ GlobalTimeSequenceCounterJumpWidth                 | GlobalTimeSequenceCounterJumpWidth specifies the maximum allowed jump of the Sequence Counter between consecutive two Sync messages.                                                                                                                                                                                   |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | _`GlobalTimePdelayRespAndRespFollowUpTimeout`\ GlobalTimePdelayRespAndRespFollowUpTimeout | Timeout value for Pdelay_Resp and Pdelay_Resp_Follow_Up after a Pdelay_Req has been transmitted resp. a Pdelay_Resp has been received. .                                                                                                                                                                               |
   +-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+


*****************************
Protocol usage and guidelines
*****************************

Please note that chapter 5 provides several requirements on usage.


**********
References
**********

_`DOC_IEEE8021Q2011`: "IEEE 802.1Q-2011 - IEEE Standard for Local and metropolitan area networks - Media Access Control (MAC) Bridges and Virtual Bridged Local Area Networks"; Publisher: IEEE

_`DOC_IEEEStandard8021AS30`: "IEEE Standard 802.1AS-30"; Publisher: IEEE; URL: http://standards.ieee.org/getieee802/download/802.1AS-2011.pdf

