#********************************************************************************************************************
#*                                                                                                                  *
#* COPYRIGHT RESERVED, Robert Bosch GmbH, 2021-2022. All rights reserved.                                                *
#* The reproduction, distribution and utilization of this document as well as the communication of its contents to  *
#* others without explicit authorization is prohibited. Offenders will be held liable for the payment of damages.   *
#* All rights reserved in the event of the grant of a patent, utility model or design.                              *
#*                                                                                                                  *
#********************************************************************************************************************
name: '[$(Date:yyyyMMdd).$(Rev:r)][$(Build.SourceBranchName)]'

# ========================================================================
#                          Pipeline Triggers
# ========================================================================
schedules:
- cron: "0 22 * * *" # time in UTC timezone
  displayName: Daily midnight build for mainline branches
  branches:
    include:
    - develop-vrte
  always: true # whether to always run the pipeline or only if there have been source code changes since the last successful scheduled run.
               # this is required because even if there is no code changes, dependencies may have been changed and a build is required
- cron: "0 22 * * *" # time in UTC timezone
  displayName: Daily midnight build for release branches
  branches:
    include:
    - main
    - release/*
  always: false # for release branches we do not want to rebuild if there are no code changes

trigger:
  batch: true # Batch changes and don't build for each push
  branches:
    include:
    - main
    - develop-vrte
    - release/*
    - feature/F_* # Cross-cutting feature branches

pr:
  autoCancel: true # Cancel old builds when new changes are pushed

# ========================================================================
#                          Template resource
# ========================================================================
resources:
  repositories:
  - repository: templates # id for reuse in below script code
    type: git
    name: VRTE-Infrastructure/azure-pipeline-templates
    ref: refs/tags/software/swe-aap/6

# ========================================================================
#                          Template reference
# ========================================================================
extends:
  template: software/swe-aap/swe-aap-exttemplate.yml@templates # Template reference
  parameters:
    PackageName: "tsync-ptp-daemon"
    MainlineBranchName: 'develop-vrte'
    VariantDataPackageVersion: "[^2.0.0]"
    shallEnableVrtebPrePostCommands: false
    isStaticCodeAnalysisEnabled: false
    deployment_config_ruleset: "./integration/deployment-ruleset-tsync-ptp-daemon.yml"
