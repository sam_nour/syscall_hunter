// =============================================================================
//  C O P Y R I G H T
// -----------------------------------------------------------------------------
//  Copyright (c) 2021 by Robert Bosch GmbH. All rights reserved.
//
//  This file is property of Robert Bosch GmbH. Any unauthorized copy, use or
//  distribution is an offensive act against international law and may be
//  prosecuted under federal law. Its content is company confidential.
// =============================================================================


#include <stdio.h>
#include <string.h>
#include "rb_impl/inc/Ptpd_Rb.h"
#include "rb_impl/inc/Ptpd_Rb_Arith.h"

/** @brief  The structure that is used to store the information for computation
 * of globalTime */
extern Ptpd_Rb_Consumer_t Ptpd_Rb_Consumer_st;

/** @brief Type for storing the configuration information and information that
 * is derived from the configuration. Configuration info mainly comes from the
 * command line.
*/
extern Ptpd_Rb_Cfg_t   Ptpd_Rb_Cfg_st;

/** @brief Function to update the follow-up information received .
 * @details
 * This function stores the follow-up information into a global data structure.
 * The information is originally received from the follow-up message.
 * @param[in]  originTimePtrU8          Precise Origin Time.
 * @param[in]  corrTimePtrU8            Correction field.
 * @param[in]  userDataPtrU8            Pointer to buffer containing userdata.
 * @return None.
 */
void Ptpd_Rb_ReadConsumerFupInfo(const uint8_t *originTimePtrU8,
                                 const uint8_t *corrTimePtrU8,
                                 const uint8_t *userDataPtrU8)
{
    /* Todo: Buf size check */
    if((NULL != userDataPtrU8) && (NULL != originTimePtrU8))
    {
        /* Type cast the information received to TimeStamp type */
        TSync_TimeStampType  *corrTime_st = (TSync_TimeStampType*) corrTimePtrU8;

        Ptpd_Rb_TimeStampType_t  *originTime_ptr_st = (Ptpd_Rb_TimeStampType_t*) originTimePtrU8;

        /* Proceed only if a valid Sync message VLT was updated earlier */
        if(PTPD_RB_STATE_SYNC_RCVD == Ptpd_Rb_Consumer_st.syncState)
        {
            /* Update the Correction Field Value in Consumer structure */
            Ptpd_Rb_Consumer_st.followUpMsg_st.correctionField_st.ts.secondsHi =
            corrTime_st->secondsHi;

            Ptpd_Rb_Consumer_st.followUpMsg_st.correctionField_st.ts.seconds =
            corrTime_st->seconds;

            Ptpd_Rb_Consumer_st.followUpMsg_st.correctionField_st.ts.nanoseconds =
            corrTime_st->nanoseconds;

            /* Set default sign of the time to POSITIVE */
            Ptpd_Rb_Consumer_st.followUpMsg_st.correctionField_st.sign = PTPD_RB_POSITIVE;

            /* Update the Origin Time in Consumer structure */
            Ptpd_Rb_Consumer_st.followUpMsg_st.PreciseOriginTimeStamp_st.ts.secondsHi
            = originTime_ptr_st->secondsHi;

            Ptpd_Rb_Consumer_st.followUpMsg_st.PreciseOriginTimeStamp_st.ts.seconds =
            originTime_ptr_st->seconds;

            Ptpd_Rb_Consumer_st.followUpMsg_st.PreciseOriginTimeStamp_st.ts.nanoseconds =
            originTime_ptr_st->nanoseconds;

            /* Set default sign of the time to POSITIVE */
            Ptpd_Rb_Consumer_st.followUpMsg_st.PreciseOriginTimeStamp_st.sign = PTPD_RB_POSITIVE;

            Ptpd_Rb_ReadUserData(userDataPtrU8);

            Ptpd_Rb_ConsumerSend();
        }
        else
        {
            printf("PTPD-RB-ERROR : The Sync-State is not in SYNC-RECEIVED. FUP message will not be processed \n");
        }
    }
} /* End of function Ptpd_Rb_ReadConsumerFupInfo */


/** @brief Update the VirtualLocalTime for given message type.
 * @details
 * This functions gets the Virtual local Time from the TSync and updates it in
 * the consumer data structure. Dependending on the argument - Sync or FUP
 * the VLT will be updated in the respective variable. These values are T1 and
 * T2 for the globalTime computation.
 * @param TimeStampType_en        PTPD_RB_INGRESS_T1 for updating T1
 *                                PTPD_RB_INGRESS_T2 for updating T2.
 * @return None.
 */
void Ptpd_Rb_ReadIngressVLT( Ptpd_Rb_StateType_t VltType_en )
{
    TSync_ReturnType returnVal = E_NOT_OK;
    TSync_VirtualLocalTimeType virtualLocalTime =
                                         {PTPD_RB_ZERO,PTPD_RB_ZERO};

    /* Get the Current Virtual Local Time */
    returnVal = TSync_GetCurrentVirtualLocalTime(
            Ptpd_Rb_Cfg_st.tsyncHandle, &virtualLocalTime);

    if(E_NOT_OK == returnVal)
    {
        printf("PTPD-RB-ERROR : Failed to get Virtual Local Time from TSync\n");

        /* Update the Sync-State to Invalid */
        Ptpd_Rb_Consumer_st.syncState = PTPD_RB_STATE_INVALID;
    }
    else
    {
        /* Get TSync Time Stamp and update in the appropriate field */
        switch(VltType_en)
        {
            case PTPD_RB_INGRESS_T1:
                Ptpd_Rb_Consumer_st.t1VLT_st.nanosecondsHi = virtualLocalTime.nanosecondsHi;

                Ptpd_Rb_Consumer_st.t1VLT_st.nanosecondsLo = virtualLocalTime.nanosecondsLo;

                /* Update the Sync-State to Sync-Received */
                Ptpd_Rb_Consumer_st.syncState = PTPD_RB_STATE_SYNC_RCVD;
            break;

            case PTPD_RB_INGRESS_T2:
                Ptpd_Rb_Consumer_st.t2VLT_st.nanosecondsHi = virtualLocalTime.nanosecondsHi;

                Ptpd_Rb_Consumer_st.t2VLT_st.nanosecondsLo = virtualLocalTime.nanosecondsLo;

                /* Update the Sync-State to Fup received */
                Ptpd_Rb_Consumer_st.syncState = PTPD_RB_STATE_FUP_RCVD;
            break;
            default:
            break;
        }
    }
}  /* End of function Ptpd_Rb_ReadIngressVLT */


/** @brief Computes the globalTime and calls BusSetGlobalTime.
 * @details
 * This functions performs the globalTime computation and makes call to
 * BusSetGlobalTime. The fomula for globalTime computation is:
 *    globalTime = PreciseOriginTime + correctionfield + (T2vlt-T1vlt) + Pdelay;
 *    localTime = T2vlt,
 *    measureData = Pdelay
 * @param TimeStampType_en        PTPD_RB_INGRESS_T1 for updating T1.
 *                                PTPD_RB_INGRESS_T2 for updating T2.
 * @return None.
 */
void Ptpd_Rb_ConsumerSend( void )
{
    TSync_ReturnType returnVal = E_NOT_OK;

    Ptpd_Rb_TimeIntDiffType_t       lTempTimeStampDiff_st = {{PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO}, PTPD_RB_POSITIVE};

    Ptpd_Rb_TimeIntDiffType_t       lOffset_st = {{PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO}, PTPD_RB_POSITIVE};

    Ptpd_Rb_TimeIntDiffType_t       lSyncTime_st = { {PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO}, PTPD_RB_POSITIVE};

    Ptpd_Rb_TimeIntDiffType_t       lAddedTime_st = {{PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO}, PTPD_RB_POSITIVE};

    Ptpd_Rb_TimeIntDiffType_t       lConsumerTime_st = {{PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO}, PTPD_RB_POSITIVE};

    Ptpd_Rb_TimeIntDiffType_t       lSyncTimeStamp_st = {{PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO}, PTPD_RB_POSITIVE};

    Ptpd_Rb_TimeIntDiffType_t       lFupTimeStamp_st = {{PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO}, PTPD_RB_POSITIVE};

    uint64_t lSyncRxTimeStamp_u64 = PTPD_RB_ZERO;
    uint64_t lFupRxTimeStamp_u64 = PTPD_RB_ZERO;

    /* Variables that are used to send time to TSync Lib */
    TSync_TimeStampType               lGlobalTime = { PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO};
    TSync_UserDataType                lUserData = { PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO};
    TSync_MeasurementType             lMeasureData;
    TSync_VirtualLocalTimeType        lLocalTime;

    /* Update the T2 Time - Virtual Local Time Type */
    Ptpd_Rb_ReadIngressVLT( PTPD_RB_INGRESS_T2 );

    /* Proceed only if a valid FUP message VLT was successful */
    /* Protection to variable Ptpd_Rb_Consumer_st is not needed as the
    control-flow is synchronous */
    if(PTPD_RB_STATE_FUP_RCVD == Ptpd_Rb_Consumer_st.syncState)
    {
        /* Update the pdelay */
        lMeasureData.pathDelay   = Ptpd_Rb_Cfg_st.pdelayCfg_st.ts.nanoseconds;

        /* Update the virtual local time to send to TSync-Lib */
        lLocalTime.nanosecondsLo = Ptpd_Rb_Consumer_st.t2VLT_st.nanosecondsLo;
        lLocalTime.nanosecondsHi = Ptpd_Rb_Consumer_st.t2VLT_st.nanosecondsHi;

        /* Left shift nanoseconds high position by 32 times and perform OR operation with nanoseconds low value -- for T1 */
        lSyncRxTimeStamp_u64 = ( uint64_t )( ( ( ( uint64_t ) Ptpd_Rb_Consumer_st.t1VLT_st.nanosecondsHi ) << PTPD_RB_THIRTYTWO ) | Ptpd_Rb_Consumer_st.t1VLT_st.nanosecondsLo );

        /* Left shift nanoseconds high position by 32 times and perform OR operation with nanoseconds low value -- for T2 */
        lFupRxTimeStamp_u64 = ( uint64_t )( ( ( ( uint64_t ) Ptpd_Rb_Consumer_st.t2VLT_st.nanosecondsHi ) << PTPD_RB_THIRTYTWO ) | Ptpd_Rb_Consumer_st.t2VLT_st.nanosecondsLo );

        /* Convert and store the current time in Eth_TimeIntDiffType structure */
        Ptpd_Rb_CnvToTimeStamp( lSyncRxTimeStamp_u64, PTPD_RB_ZERO, &lTempTimeStampDiff_st );

        lSyncTimeStamp_st.ts = lTempTimeStampDiff_st.ts;

        /* Convert and store the current time in Eth_TimeIntDiffType structure */
        Ptpd_Rb_CnvToTimeStamp( lFupRxTimeStamp_u64, PTPD_RB_ZERO, &lTempTimeStampDiff_st );

        lFupTimeStamp_st.ts = lTempTimeStampDiff_st.ts;

        /* Obtain time difference between Provider and Consumer by subtracting Ingress Time from PreciseOriginTimeStamp obtained in FollowUp frame */
        Ptpd_Rb_SubTi( &lFupTimeStamp_st, &lSyncTimeStamp_st, &lOffset_st );

        /* Add the offset time to Consumer to sync time with Providers's */
        Ptpd_Rb_AddTi( &Ptpd_Rb_Consumer_st.followUpMsg_st.PreciseOriginTimeStamp_st, &lOffset_st, &lSyncTime_st );

        /* Add Path delay to the time obtained*/
        Ptpd_Rb_AddTi( &lSyncTime_st, &Ptpd_Rb_Cfg_st.pdelayCfg_st, &lAddedTime_st );

        /* Add CorrectionField to the time difference */
        Ptpd_Rb_AddTi( &lAddedTime_st, &Ptpd_Rb_Consumer_st.followUpMsg_st.correctionField_st, &lConsumerTime_st );

        /* Assign the obtained time to a variable of type StbMTimeStamp_st */
        lGlobalTime.nanoseconds       = lConsumerTime_st.ts.nanoseconds;
        lGlobalTime.seconds           = lConsumerTime_st.ts.seconds;
        lGlobalTime.secondsHi         = lConsumerTime_st.ts.secondsHi;

        lUserData.userDataLength = Ptpd_Rb_Consumer_st.followUpMsg_st.subTlv_st.userData.userDataLength;
        lUserData.userByte0 = Ptpd_Rb_Consumer_st.followUpMsg_st.subTlv_st.userData.userByte0;
        lUserData.userByte1 = Ptpd_Rb_Consumer_st.followUpMsg_st.subTlv_st.userData.userByte1;
        lUserData.userByte2 = Ptpd_Rb_Consumer_st.followUpMsg_st.subTlv_st.userData.userByte2;

        #ifdef PTPD_RB_DEBUG
        printf("\nPTPD-RB-DEBUG: Sending the Global Time to TSync %u secondsHI, %u seconds and %u nanoseconds\n",
        lGlobalTime.secondsHi,lGlobalTime.seconds, lGlobalTime.nanoseconds);

        printf("PTPD-RB-DEBUG: Sending the Virtual Local Time to TSync %u nanosecondsLo, %u nanosecondsHi \n\n",
        lLocalTime.nanosecondsLo, lLocalTime.nanosecondsHi);
        #endif

        returnVal = TSync_BusSetGlobalTime(Ptpd_Rb_Cfg_st.tsyncHandle,
                                            &lGlobalTime,
                                            &lUserData,
                                            &lMeasureData,
                                            &lLocalTime);

        if(E_NOT_OK == returnVal)
        {
            printf("PTPD-RB-ERROR : Call to TSync_BusSetGlobalTime failed\n");
        }
    } /* End of conditon Sync-State */
    else
    {
        printf("PTPD-RB-ERROR : The Sync-State is not in FUP-RECEIVED. No BusSetGlobalTime called \n");
    }
} /* End of function Ptpd_Rb_ConsumerSend */


/** @brief Reads the SubTlv(UserData) information .
 * @details
 * This function is used by Consumer to read/extract the SubTlv (read user data
 * in the PTP frame) that was sent by the Ethernet Bus.
 * @param[in]  userDataPtrU8      Pointer to buffer where SubTLV info can be read from.
 * @return None.
 */
void Ptpd_Rb_ReadUserData(const uint8_t *userDataPtrU8)
{
    if(NULL != userDataPtrU8)
    {
        /* User data length */
        Ptpd_Rb_Consumer_st.followUpMsg_st.subTlv_st.userData.userDataLength = *(uint8_t*)(userDataPtrU8);

        /* User data byte 1 */
        Ptpd_Rb_Consumer_st.followUpMsg_st.subTlv_st.userData.userByte0 = *(uint8_t*)(userDataPtrU8+1);

        /* User data byte 2 */
        Ptpd_Rb_Consumer_st.followUpMsg_st.subTlv_st.userData.userByte1 = *(uint8_t*)(userDataPtrU8+2);

        /* User data byte 3 */
        Ptpd_Rb_Consumer_st.followUpMsg_st.subTlv_st.userData.userByte2 = *(uint8_t*)(userDataPtrU8+3);
    }
}
