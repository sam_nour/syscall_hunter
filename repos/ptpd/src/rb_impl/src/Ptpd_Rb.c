// =============================================================================
//  C O P Y R I G H T
// -----------------------------------------------------------------------------
//  Copyright (c) 2021 by Robert Bosch GmbH. All rights reserved.
//
//  This file is property of Robert Bosch GmbH. Any unauthorized copy, use or
//  distribution is an offensive act against international law and may be
//  prosecuted under federal law. Its content is company confidential.
// =============================================================================

#include "rb_impl/inc/Ptpd_Rb.h"
#include <math.h>

/** @brief Type for storing the configuration information and information that
 * is derived from the configuration. Configuration info mainly comes from the
 * command line.
*/
Ptpd_Rb_Cfg_t   Ptpd_Rb_Cfg_st;

/** @brief Defines the all the received information - both Sync and Fup message
 * fields that are required for the calculation of globaltime
*/
Ptpd_Rb_Consumer_t Ptpd_Rb_Consumer_st;

/** @brief Consolidates the all the provider timing information to compute
 * PreciseOriginTime
*/
Ptpd_Rb_Provider_t gPtpd_Rb_Provider_st;

/** @brief Init function to update configuration information.
 * @details
 * This function stores the configuration information into a global structure.
 * The information is received from the command line options.
 * @param[in]  pdelay        Static Propagation delay.
 * @param[in]  domainNumber  Time Base Identifier.
 * @param[in]  isProvider    If the ptpd instance is Provider.
 * @return boolean           TRUE for successful Init
 *                           FALSE otherwise.
 */
Ptpd_Rb_boolean Ptpd_Rb_Init(double pdelay, uint16_t domainNumber,
Ptpd_Rb_boolean isProvider)
{
    /* For the received TimeBase, the handle must be derived from the
    TSync-Lib. This handle will be used for making tsync calls related to
    the above timebaseId */
    if(E_OK == TSync_Open())
    {
        Ptpd_Rb_Cfg_st.tsyncHandle = TSync_OpenTimebase( (TSync_SynchronizedTimeBaseType)(domainNumber) );

        /* Update the timebase id */
        Ptpd_Rb_Cfg_st.timeBaseId_u16 = domainNumber;

        if(isProvider)
            TSync_RegisterTransmitGlobalTimeCallback(Ptpd_Rb_Cfg_st.tsyncHandle,
            Ptpd_Rb_TransmitGlobalTimeCallback);
    }
    else
    {
        Ptpd_Rb_Cfg_st.tsyncHandle = NULL;
        printf("PTPD-RB-ERROR : Tsync basic initialization failed !!!!\n");
        return PTPD_RB_FALSE;
    }

    /* Update pdelay - Convert the received pdelay into TimeStamp type */
    Ptpd_Rb_Cfg_st.pdelayCfg_st.ts.secondsHi = PTPD_RB_ZERO;
    Ptpd_Rb_Cfg_st.pdelayCfg_st.ts.seconds = trunc(pdelay);
    Ptpd_Rb_Cfg_st.pdelayCfg_st.ts.nanoseconds = (pdelay - (Ptpd_Rb_Cfg_st.pdelayCfg_st.ts.seconds + 0.0)) * 1E9;

    /* Updating the default sign as POSITIVE */
    Ptpd_Rb_Cfg_st.pdelayCfg_st.sign = PTPD_RB_POSITIVE;

    /* Update the default Sync-State to INVALID */
    Ptpd_Rb_Consumer_st.syncState = PTPD_RB_STATE_INVALID;

#ifdef PTPD_RB_DEBUG
    printf("PTPD-RB-DEBUG: The config values are : \n");
    printf("PTPD-RB-DEBUG: Pdelay : %d secondsHi, %d seconds and %d nanoseconds\n",
    Ptpd_Rb_Cfg_st.pdelayCfg_st.ts.secondsHi,
    Ptpd_Rb_Cfg_st.pdelayCfg_st.ts.seconds,
    Ptpd_Rb_Cfg_st.pdelayCfg_st.ts.nanoseconds);

    printf("PTPD-RB-DEBUG: DomainNumber : %d", Ptpd_Rb_Cfg_st.timeBaseId_u16);
#endif

    return PTPD_RB_TRUE;
} /* End of function Ptpd_Rb_Init */


/** @brief Deinit function to perform graceful shutdown.
 * @details
 * This functions performs all the activities associated with graceful shutdown
 * - Like close the connection with TSync, set the tsync handle to NULL.
 * @param None.
 * @return None.
 */
void Ptpd_Rb_Deinit(void)
{
    /* Close the TimeBase */
    TSync_CloseTimebase( Ptpd_Rb_Cfg_st.tsyncHandle );

    /* Set the timebase handle to NULL */
    Ptpd_Rb_Cfg_st.tsyncHandle = NULL;

    /* Update the Sync-State to Invalid */
    Ptpd_Rb_Consumer_st.syncState = PTPD_RB_STATE_INVALID;

    /* Close the connection to TSync */
    TSync_Close();

} /* End of function Ptpd_Rb_DeInit */

