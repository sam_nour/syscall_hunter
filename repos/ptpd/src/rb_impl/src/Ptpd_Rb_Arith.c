// =============================================================================
//  C O P Y R I G H T
// -----------------------------------------------------------------------------
//  Copyright (c) 2021 by Robert Bosch GmbH. All rights reserved.
//
//  This file is property of Robert Bosch GmbH. Any unauthorized copy, use or
//  distribution is an offensive act against international law and may be
//  prosecuted under federal law. Its content is company confidential.
// =============================================================================


#include <stdlib.h>
#include <string.h>
#include "../inc/Ptpd_Rb_Arith.h"


/** @brief Algorithm to add two timestamps.
 * @details
 * This function enables the user to add two timestamps. The timestamps
 * have sign as well. Hence as per the sign, the addition/subtraction shall be
 * performed.
 * @param[in]  Time1             First Timestamp operand.
 * @param[in]  Time2             Second Timestamp operand.
 * @param[out] TimeResult        The result is of the operation is updated here.
 * @return None.
 */
void Ptpd_Rb_AddTi( const Ptpd_Rb_TimeIntDiffType_t *Time1,
                    const Ptpd_Rb_TimeIntDiffType_t *Time2,
                         Ptpd_Rb_TimeIntDiffType_t       *TimeResult )
{
    /* local variable declaration */
    uint64_t lTime1;
    uint64_t lTime2;
    uint64_t lTimeAdd;

    /* Convert the TimeStamps(seconds part) to uint64_t */
    lTime1   =   (uint64_t)( ( ( ( uint64_t )( Time1->ts.secondsHi ) ) << PTPD_RB_THIRTYTWO ) | ( Time1->ts.seconds ) );
    lTime2   =   (uint64_t)( ( ( ( uint64_t )( Time2->ts.secondsHi ) ) << PTPD_RB_THIRTYTWO ) | ( Time2->ts.seconds ) );
    lTimeAdd =   PTPD_RB_ZERO;

    /* Check signs of both Timestamps are same */
    if( Time1->sign == Time2->sign )
    {
        /* Add nanoseconds part */
        TimeResult->ts.nanoseconds = Time1->ts.nanoseconds + Time2->ts.nanoseconds;

        /* Add seconds part now*/
        lTimeAdd                  = lTime1+lTime2;

        /* If nanoseconds is greater than 999999999(0x3B9AC9FFU), max
        nanoseconds are reached. Time to increment the seconds count by 1 */
        if( PTPD_RB_NS_UPPRLIM < TimeResult->ts.nanoseconds )
        {
            lTimeAdd                  += PTPD_RB_ONE;
            TimeResult->ts.nanoseconds -= PTPD_RB_NANOSEC;
        }
        /* Take over the sign from either of the TimeStamps - as both times are
        same */
        TimeResult->sign = Time1->sign;
    }
    /* Since the signs are different, subtraction to be carried out */
    else
    {
        /* The seconds part of both the Timestamps are same */
        if( lTime1 == lTime2 )
        {
            lTimeAdd = PTPD_RB_ZERO;

            /* Nanoseconds of first TimeStamp is greater than the seconds */
            if( Time1->ts.nanoseconds >= Time2->ts.nanoseconds )
            {
                TimeResult->ts.nanoseconds = Time1->ts.nanoseconds - Time2->ts.nanoseconds;
                TimeResult->sign             = Time1->sign;
            }
            else
            {
                TimeResult->ts.nanoseconds = Time2->ts.nanoseconds - Time1->ts.nanoseconds;
                TimeResult->sign             = Time2->sign;
            }
        }
        /* The seconds part of first TimeStamp is greater than second */
        else if( lTime1 > lTime2 )
        {
            /* Nanoseconds of first TimeStamp is greater than the seconds */
            if( Time1->ts.nanoseconds >= Time2->ts.nanoseconds )
            {
                TimeResult->ts.nanoseconds = Time1->ts.nanoseconds - Time2->ts.nanoseconds;
                lTimeAdd                  = lTime1 - lTime2;
                TimeResult->sign             = Time1->sign;
            }
            else
            {
                TimeResult->ts.nanoseconds = ( Time1->ts.nanoseconds + PTPD_RB_NANOSEC ) - Time2->ts.nanoseconds;
                lTimeAdd                  = ( lTime1 - PTPD_RB_ONE ) - lTime2;
                TimeResult->sign             = Time1->sign;
            }
        }
        else
        {
            if( Time1->ts.nanoseconds > Time2->ts.nanoseconds )
            {
                TimeResult->ts.nanoseconds = ( Time2->ts.nanoseconds + PTPD_RB_NANOSEC ) - Time1->ts.nanoseconds;
                lTimeAdd                  = ( lTime2 - PTPD_RB_ONE ) - lTime1;
                TimeResult->sign             = Time2->sign;
            }
            else
            {
                TimeResult->ts.nanoseconds = Time2->ts.nanoseconds - Time1->ts.nanoseconds;
                lTimeAdd                  = lTime2 - lTime1;
                TimeResult->sign             = Time2->sign;
            }
        }
    }
    /* Updating seconds and secondsHi of TimeResult from local variable */
    TimeResult->ts.secondsHi =   ( uint16_t )( ( ( lTimeAdd ) & ( PTPD_RB_HIGHER_DOUBLE_WORD_MASK ) ) >> PTPD_RB_THIRTYTWO );
    TimeResult->ts.seconds   =   ( uint32_t )( ( lTimeAdd ) & ( PTPD_RB_LOWER_DOUBLE_WORD_MASK ) );
} /* End of function Ptpd_Rb_AddTi */

/** @brief Algorithm to subtract two timestamps.
 * @details
 * This function enables the user to subtract two timestamps.
 * @param[in]  Time1             First Timestamp operand.
 * @param[in]  Time2             Second Timestamp operand.
 * @param[out] TimeDiff          The result is of the operation is updated here.
 * @return None.
 */
void Ptpd_Rb_SubTi( const Ptpd_Rb_TimeIntDiffType_t   *Time1,
                    const Ptpd_Rb_TimeIntDiffType_t   *Time2,
                          Ptpd_Rb_TimeIntDiffType_t   *TimeDiff )
{
    /* local variable declaration */
    uint64_t lTime1;
    uint64_t lTime2;
    uint64_t lTimeDiff;

    /* Convert the TimeStamps(seconds part) to uint64_t */
    lTime1    =   (uint64_t)( ( ( ( uint64_t )( Time1->ts.secondsHi ) ) << 32 ) | ( Time1->ts.seconds ) );
    lTime2    =   (uint64_t)( ( ( ( uint64_t )( Time2->ts.secondsHi ) ) << 32 ) | ( Time2->ts.seconds ) );

    lTimeDiff =   PTPD_RB_ZERO;

    /* The seconds part are equal */
    if( lTime1 == lTime2 )
    {
        lTimeDiff = PTPD_RB_ZERO;

        /* First operand is greater, hence carry out the subtraction and take
        over the POSITIVE sign */
        if( Time1->ts.nanoseconds >= Time2->ts.nanoseconds )
        {
            TimeDiff->ts.nanoseconds  = Time1->ts.nanoseconds - Time2->ts.nanoseconds;
            TimeDiff->sign              = PTPD_RB_POSITIVE;
        }
        /* Since the second operand is greater, take over the Negative sign */
        else
        {
            TimeDiff->ts.nanoseconds  = Time2->ts.nanoseconds - Time1->ts.nanoseconds;
            TimeDiff->sign              = PTPD_RB_NEGATIVE;
        }
    }
    /* First operand is greater */
    else if( lTime1 > lTime2 )
    {
        /* The sign in both the cases below would be POSITIVE as the first
        operand is greater */
        /* When nanoseconds of first operand is greater than second, simple
        subtraction can be carried */
        if( Time1->ts.nanoseconds >= Time2->ts.nanoseconds )
        {
            TimeDiff->ts.nanoseconds  = Time1->ts.nanoseconds - Time2->ts.nanoseconds;
            lTimeDiff                   = lTime1 - lTime2;
            TimeDiff->sign              = PTPD_RB_POSITIVE;
        }
        /* When nanoseconds of first operand is smaller than second(however the
        seconds part of the first operand is greater ), a borrow operation is
        necessary */
        else
        {
            TimeDiff->ts.nanoseconds = ( Time1->ts.nanoseconds + PTPD_RB_NANOSEC ) - Time2->ts.nanoseconds;
            lTimeDiff                  = ( lTime1 - PTPD_RB_ONE ) - lTime2;
            TimeDiff->sign             = PTPD_RB_POSITIVE;
        }
    }
    /* Second operand is greater */
    else
    {
        /* The sign in both the cases below would be NEGATIVE as the second
        operand is greater than first */
        if( Time1->ts.nanoseconds > Time2->ts.nanoseconds )
        {
            TimeDiff->ts.nanoseconds  = ( Time2->ts.nanoseconds + PTPD_RB_NANOSEC ) - Time1->ts.nanoseconds;
            lTimeDiff                   = ( lTime2 - PTPD_RB_ONE ) - lTime1;
            TimeDiff->sign              = PTPD_RB_NEGATIVE;
        }
        else
        {
            TimeDiff->ts.nanoseconds  = Time2->ts.nanoseconds - Time1->ts.nanoseconds;
            lTimeDiff                   = lTime2 - lTime1;
            TimeDiff->sign              = PTPD_RB_NEGATIVE;
        }
    }
    /* Update seconds and secondsHi of TimeDiff from local variable */
    TimeDiff->ts.secondsHi =   ( uint16_t )( ( ( lTimeDiff ) & ( PTPD_RB_HIGHER_DOUBLE_WORD_MASK ) ) >> PTPD_RB_THIRTYTWO );
    TimeDiff->ts.seconds   =   ( uint32_t )( ( lTimeDiff ) & ( PTPD_RB_LOWER_DOUBLE_WORD_MASK ) );

} /* End of function Ptpd_Rb_SubTi */

/** @brief Algorithm to convert uint64_t value to TimeStamp stamp.
 * @details
 * This function enables the user convert uint64_t value into TimeStamp type.
 * It also initializes the sign of the TimeStamp generated as POSITIVE.
 * @param[in]  Value_u64         First Timestamp operand.
 * @param[in]  Factor_u8         Second Timestamp operand.
 * @param[out] Time_pst          The result is of the operation is updated here.
 * @return None.
 */
void Ptpd_Rb_CnvToTimeStamp( uint64_t                  Value_u64,
                             uint8_t                   Factor_u8,
                             Ptpd_Rb_TimeIntDiffType_t *TimeStampPtr )
{
    /* Local variable declaration */
    uint64_t lTime_u64;

    /* Convert from Bitfield to Time */
    lTime_u64 = Value_u64 >> Factor_u8;

    /* Obtain nanoseconds part of time */
    TimeStampPtr->ts.nanoseconds = (uint32_t)( lTime_u64 % PTPD_RB_NANOSEC );

    /* Obtain Seconds part of time */
    lTime_u64 = lTime_u64 / PTPD_RB_NANOSEC;

    /* Initialising seconds and secondsHi of Time */
    TimeStampPtr->ts.secondsHi = ( uint16_t )( ( ( lTime_u64 ) & ( PTPD_RB_HIGHER_DOUBLE_WORD_MASK) ) >> PTPD_RB_THIRTYTWO );
    TimeStampPtr->ts.seconds   = ( uint32_t )(   ( lTime_u64 ) & ( PTPD_RB_LOWER_DOUBLE_WORD_MASK ) );

    /* Initializes the sign */
    TimeStampPtr->sign = PTPD_RB_POSITIVE;

} /* End of function Ptpd_Rb_CnvToTimeStamp */


/** @brief Algorithm to interpolate the global time based on VirtualLocalTimes.
 * @details
 * This function interpolates the global time based on the passed
 * VirtualLocalTime. The computation is as follows :
 * new_global_time =
 * global_time_passed + (new_virtual_local_time - old_virtual_local_time)
 * @param[in]  vlt1Ptr               The new Virtual Local time.
 * @param[in]  vlt2Ptr               The previous/old Virtual Local Time.
 * @param[in]  globalTimeStampPtr    The the previous/old global time.
 * @param[out] resultTimeStampPtr    Pointer to newly computed global time.
 * @return None.
 */
void Ptpd_Rb_InterpolateVlt(const TSync_VirtualLocalTimeType *vlt1Ptr,
                            const TSync_VirtualLocalTimeType *vlt2Ptr,
                            const Ptpd_Rb_TimeIntDiffType_t  *globalTimeStampPtr,
                                  TSync_TimeStampType        *resultTimeStampPtr )
{
    uint64_t lVlt1_u64, lVlt2_u64 = PTPD_RB_ZERO;

    Ptpd_Rb_TimeIntDiffType_t lVlt1TimeStamp_st = {{PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO}, PTPD_RB_POSITIVE};

    Ptpd_Rb_TimeIntDiffType_t lVlt2TimeStamp_st = {{PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO}, PTPD_RB_POSITIVE};

    Ptpd_Rb_TimeIntDiffType_t lVltDiff_st = {{PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO}, PTPD_RB_POSITIVE};

    Ptpd_Rb_TimeIntDiffType_t lGlobalResult_st = {{PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO}, PTPD_RB_POSITIVE};

    lVlt1_u64 = ((uint64_t)vlt1Ptr->nanosecondsHi << 32) | vlt1Ptr->nanosecondsLo;

    lVlt2_u64 = ((uint64_t)vlt2Ptr->nanosecondsHi << 32) | vlt2Ptr->nanosecondsLo;

    /* Convert and store the current time */
    Ptpd_Rb_CnvToTimeStamp( lVlt1_u64, PTPD_RB_ZERO, &lVlt1TimeStamp_st );

    /* Convert and store the current time */
    Ptpd_Rb_CnvToTimeStamp( lVlt2_u64, PTPD_RB_ZERO, &lVlt2TimeStamp_st );

    Ptpd_Rb_SubTi(&lVlt1TimeStamp_st, &lVlt2TimeStamp_st, &lVltDiff_st);

    Ptpd_Rb_AddTi(globalTimeStampPtr, &lVltDiff_st, &lGlobalResult_st);

    resultTimeStampPtr->secondsHi = lGlobalResult_st.ts.secondsHi;
    resultTimeStampPtr->seconds = lGlobalResult_st.ts.seconds;
    resultTimeStampPtr->nanoseconds = lGlobalResult_st.ts.nanoseconds;
}
