// =============================================================================
//  C O P Y R I G H T
// -----------------------------------------------------------------------------
//  Copyright (c) 2021 by Robert Bosch GmbH. All rights reserved.
//
//  This file is property of Robert Bosch GmbH. Any unauthorized copy, use or
//  distribution is an offensive act against international law and may be
//  prosecuted under federal law. Its content is company confidential.
// =============================================================================


#include <stdio.h>
#include <string.h>
#include "rb_impl/inc/Ptpd_Rb.h"
#include "rb_impl/inc/Ptpd_Rb_Arith.h"
#include <pthread.h>
#include <errno.h>

/** Typedefs used only in this file */

/** @brief Typedef to define autosar subtlv format - here Userdata is used
 * as SubTLVs
*/
typedef struct
{
    uint8_t  subtlvType;     /* Type of the Subtlv */
    uint8_t  subtlvLength;   /* Length of the Subtlv */
    uint8_t  userDataLen;    /* Length of the SubtlvType - Userdata length */
    uint8_t  userDataByte0;  /* Userdata byte 0*/
    uint8_t  userDataByte1;  /* Userdata byte 1*/
    uint8_t  userDataByte2;  /* Userdata byte 2*/
    uint8_t  reserved;       /* Reserved field - To be used if CRC is introduced*/

} Ptpd_Rb_SubTlv_Userdata_t;


/** @brief Consolidates the all the provider timing information to compute
 * PreciseOriginTime
*/
extern Ptpd_Rb_Provider_t gPtpd_Rb_Provider_st;

/** @brief Type for storing the configuration information and information that
 * is derived from the configuration. Configuration info mainly comes from the
 * command line.
*/
extern Ptpd_Rb_Cfg_t   Ptpd_Rb_Cfg_st;

pthread_mutex_t gMutex_Provider = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t gMutex_ImmTrigg = PTHREAD_MUTEX_INITIALIZER;

/** @brief Update the VirtualLocalTime for given message type.
 * @details
 * This functions gets the Virtual local Time from the TSync and updates it in
 * the consumer data structure. Dependending on the argument - Sync or FUP
 * the VLT will be updated in the respective variable. These values are T1 and
 * T2 for the globalTime computation.
 * @param TimeStampType_en        PTPD_RB_INGRESS_T1 for updating T1
 *                                PTPD_RB_INGRESS_T2 for updating T2.
 * @return None.
 */
void Ptpd_Rb_ReadEgressVLT( Ptpd_Rb_EgressVltType_t VltType_en )
{
    TSync_ReturnType returnVal = E_NOT_OK;
    TSync_VirtualLocalTimeType virtualLocalTime =
                                         {PTPD_RB_ZERO,PTPD_RB_ZERO};

    /* Get the Current Virtual Local Time */
    returnVal = TSync_GetCurrentVirtualLocalTime(
            Ptpd_Rb_Cfg_st.tsyncHandle, &virtualLocalTime);

    if(E_NOT_OK == returnVal)
    {
        printf("PTPD-RB-ERROR : Failed to get Virtual Local Time from TSync\n");

        /* Update the Sync-State to Invalid */
        gPtpd_Rb_Provider_st.syncState = PTPD_RB_PROVIDER_INVALID;

    }
    else
    {
        if( PTPD_RB_EGRESS_T4 == VltType_en)
        {
            gPtpd_Rb_Provider_st.t4VLT_st.nanosecondsHi = virtualLocalTime.nanosecondsHi;

            gPtpd_Rb_Provider_st.t4VLT_st.nanosecondsLo = virtualLocalTime.nanosecondsLo;

            /* Update the Sync-State to Fup Ready */
            gPtpd_Rb_Provider_st.syncState = PTPD_RB_PROVIDER_FUP_READY;
#ifdef PTPD_RB_DEBUG
            printf("PTPD-RB-DEBUG: T4Vlt =>  %u : %u    \n\n", virtualLocalTime.nanosecondsHi,virtualLocalTime.nanosecondsLo);
#endif
        }
    }
}  /* End of function Ptpd_Rb_ReadEgressVLT */

/** @brief Update the GlobalTime and VirtualLocalTime for T0 and T0Vlt
 * respectively.
 * @details
 * This function gets the Virtual local Time and GlobalTime from TSync and
 * store it as T0 and T0Vlt. These values are used later to compute OriginTime.
 * @param None.
 * @return None.
 */
void Ptpd_Rb_BusGetGlobalTime( void )
{
    TSync_ReturnType returnVal = E_NOT_OK;

    TSync_UserDataType lUserData_st = {PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO};

    TSync_MeasurementType lMeasureData_st = {PTPD_RB_ZERO};

    TSync_VirtualLocalTimeType lVlt_st = {PTPD_RB_ZERO, PTPD_RB_ZERO};

    TSync_TimeStampType lGlobalTime_st = {PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO};

    lGlobalTime_st.seconds = 0u;
    lGlobalTime_st.secondsHi = 0u;
    lGlobalTime_st.nanoseconds = 0u;

    int ret_code = 0;

    returnVal = TSync_BusGetGlobalTime(Ptpd_Rb_Cfg_st.tsyncHandle,
                                    &lGlobalTime_st,
                                    &lUserData_st,
                                    &lMeasureData_st,
                                    &lVlt_st);

     if(E_NOT_OK == returnVal)
    {
        printf("PTPD-RB-ERROR : Failed to get Global Time from TSync\n");

        /* Update the Sync-State to Invalid */
        gPtpd_Rb_Provider_st.syncState = PTPD_RB_PROVIDER_INVALID;
    }
    else
    {
#ifdef PTPD_RB_DEBUG
        printf("\n\nPTPD-RB-DEBUG: ***************************************");
        printf("\nGlobal Time Read is => ");
        printf("  %u", lGlobalTime_st.secondsHi);
        printf(" : %u", lGlobalTime_st.seconds);
        printf(" : %u\n", lGlobalTime_st.nanoseconds);

        printf("T0Vlt => ");
        printf("  %u", lVlt_st.nanosecondsHi);
        printf(" : %u\n", lVlt_st.nanosecondsLo);
#endif
        ret_code = pthread_mutex_lock( &gMutex_Provider );

        if (EOK == ret_code)
        {
            gPtpd_Rb_Provider_st.t0GlobalTime_st.secondsHi = lGlobalTime_st.secondsHi;

            gPtpd_Rb_Provider_st.t0GlobalTime_st.seconds = lGlobalTime_st.seconds;

            gPtpd_Rb_Provider_st.t0GlobalTime_st.nanoseconds = lGlobalTime_st.nanoseconds;

            gPtpd_Rb_Provider_st.t0VLT_st.nanosecondsHi = lVlt_st.nanosecondsHi;

            gPtpd_Rb_Provider_st.t0VLT_st.nanosecondsLo = lVlt_st.nanosecondsLo;

            /* Fetch User data */
            gPtpd_Rb_Provider_st.userData.userDataLength = lUserData_st.userDataLength;
            gPtpd_Rb_Provider_st.userData.userByte0 = lUserData_st.userByte0;
            gPtpd_Rb_Provider_st.userData.userByte1 = lUserData_st.userByte1;
            gPtpd_Rb_Provider_st.userData.userByte2 = lUserData_st.userByte2;


            /* Update the Sync-State to Sync ready */
            gPtpd_Rb_Provider_st.syncState = PTPD_RB_PROVIDER_SYNC_READY;

            ret_code = pthread_mutex_unlock( &gMutex_Provider );

            if (ret_code != EOK)
            {
                printf ("PTPD-RB-ERROR : pthread_mutex_unlock(gMutex_Provider) in BusGetGlobalTime failed. Error code : %s\n",
                strerror(ret_code));
            }
        }
        else
        {

            printf ("PTPD-RB-ERROR : pthread_mutex_lock(gMutex_Provider) in BusGetGlobalTime failed. Error code : %s\n",
            strerror(ret_code));
        }
    }
}

/** @brief Fetch T4Vlt and compute PreciseOriginTime .
 * @details
 * This functions fetches T4Vlt from tsync. Then based on T4Vlt and T0Vlt
 * (which is stored previously in Provider information), T0
 * is interpolated to compute the PreciseOriginTime.
 * @param None.
 * @return None.
 */
void Ptpd_Rb_ComputeOriginTime( void )
{
    Ptpd_Rb_TimeIntDiffType_t lGlobalTimeDiffType_st = {{PTPD_RB_ZERO, PTPD_RB_ZERO, PTPD_RB_ZERO}, PTPD_RB_POSITIVE};

    int ret_code = 0;

    ret_code = pthread_mutex_lock( &gMutex_Provider );

    if (ret_code == EOK)
    {
        Ptpd_Rb_ReadEgressVLT(PTPD_RB_EGRESS_T4);

        lGlobalTimeDiffType_st.ts.secondsHi = gPtpd_Rb_Provider_st.t0GlobalTime_st.secondsHi;
        lGlobalTimeDiffType_st.ts.seconds = gPtpd_Rb_Provider_st.t0GlobalTime_st.seconds;
        lGlobalTimeDiffType_st.ts.nanoseconds = gPtpd_Rb_Provider_st.t0GlobalTime_st.nanoseconds;

        Ptpd_Rb_InterpolateVlt(&gPtpd_Rb_Provider_st.t4VLT_st,
                                &gPtpd_Rb_Provider_st.t0VLT_st,
                                &lGlobalTimeDiffType_st,
                                &gPtpd_Rb_Provider_st.originTime_st);

#ifdef PTPD_RB_DEBUG
        printf("Interpolated time to send");
        printf(" SecondsHI: %u", gPtpd_Rb_Provider_st.originTime_st.secondsHi);
        printf(" Seconds: %u", gPtpd_Rb_Provider_st.originTime_st.seconds);
        printf(" Nanoseconds : %u\n", gPtpd_Rb_Provider_st.originTime_st.nanoseconds);

#endif

        ret_code = pthread_mutex_unlock( &gMutex_Provider );
        if (ret_code != EOK)
        {
            printf ("PTPD-RB-ERROR : pthread_mutex_unlock(gMutex_Provider) in ComputeOriginTime failed. Error code : %s\n",
            strerror(ret_code));
        }
    }
    else
    {
        printf ("PTPD-RB-ERROR : pthread_mutex_lock(gMutex_Provider) in ComputeOriginTime failed. Error code : %s\n",
        strerror(ret_code));
    }
}


/** @brief Fetch PreciseOriginTime from Provider data-structure.
 * @details
 * This functions fetches origin time that was computed and stored in provider
 * data-structure based on T4Vlt and T0Vlt
 * @param originTimePtrU8      Pointer to store the computed origin time.
 * @return None.
 */
void Ptpd_Rb_WriteOriginTime( uint8_t *originTimePtrU8, uint8_t originTimeSize)
{
    int ret_code = 0;
    Ptpd_Rb_TimeStampType_t *originTimeStamp_st = NULL;

    if(( NULL != originTimePtrU8 ) && (originTimeSize == sizeof(Ptpd_Rb_TimeStampType_t)))
    {
        originTimeStamp_st = (Ptpd_Rb_TimeStampType_t*)(originTimePtrU8);

         ret_code = pthread_mutex_lock( &gMutex_Provider );
        if (ret_code == EOK)
        {

            originTimeStamp_st->secondsHi = gPtpd_Rb_Provider_st.originTime_st.secondsHi;
            originTimeStamp_st->seconds = gPtpd_Rb_Provider_st.originTime_st.seconds;
            originTimeStamp_st->nanoseconds = gPtpd_Rb_Provider_st.originTime_st.nanoseconds;
#ifdef PTPD_RB_DEBUG
        printf("originTimeStamp_st timeupdated \n");
        printf("originTimeStamp_st SecondsHI: %u\n", originTimeStamp_st->secondsHi);
        printf("originTimeStamp_st Seconds: %u\n", originTimeStamp_st->seconds);
        printf("originTimeStamp_st Nanoseconds : %u\n", originTimeStamp_st->nanoseconds);

#endif

            (void)pthread_mutex_unlock( &gMutex_Provider );

            if (ret_code != EOK)
            {
                printf ("PTPD-RB-ERROR : pthread_mutex_unlock(gMutex_Provider) failed in FetchOriginTime. Error code : %s\n",
                strerror(ret_code));
            }
        }
        else
        {
            printf ("PTPD-RB-ERROR : pthread_mutex_lock(gMutex_Provider) failed in FetchOriginTime. Error code : %s\n",
            strerror(ret_code));
        }
    }
}

/** @brief Callback function to indicate Immediate time transmission.
 * @details
 * This function is called by TSync to indicate that immediate time transmission
 * is needed. It sets triggerTime variable to TRUE. This will enable the time
 * transmission on the Bus in the next cycle.
 * @param[in]  domainId        The domain id for which time has to be
 * transmitted.
 * @return E_OK if successful, E_NOT_OK otherwise.
 */
TSync_ReturnType Ptpd_Rb_TransmitGlobalTimeCallback(TSync_SynchronizedTimeBaseType domainId)
{
    int ret_code = 0;
    TSync_ReturnType retValue = E_NOT_OK;

    /* This condition is not really necessary, since the ptpd instance is always
    run for single domain */
    if(domainId == Ptpd_Rb_Cfg_st.timeBaseId_u16)
    {
        /* Acquire the lock to change the TriggerTime variable */
        ret_code = pthread_mutex_lock( &gMutex_ImmTrigg );

        if(EOK == ret_code)
        {
            /* New time update received from TSync. Immediately send the time
            over Ethernet bus */
            gPtpd_Rb_Provider_st.triggerTime = PTPD_RB_TRUE;

            printf("TriggerTime set to true!\n");

            ret_code = pthread_mutex_unlock( &gMutex_ImmTrigg );

            if (ret_code != EOK)
            {
                printf ("PTPD-RB-ERROR : pthread_mutex_unlock(gMutex_ImmTrig) failed in TransmitGlobalTimeCallback. Error code : %s\n",
                strerror(ret_code));
            }
            retValue = E_OK;
        }
        else
        {
            printf ("PTPD-RB-ERROR : pthread_mutex_lock(gMutex_ImmTrig) in TransmitGlobalTimeCallback failed. Error code: %s\n",
            strerror(ret_code));
        }
    }

    return retValue;
}


/** @brief Returns if Immediate time transmission is set to TRUE.
 * @details
 * This function returns if the Immediate time transmission is set to TRUE.
 * @param[in]  None
 * @return TRUE if Immediate time transmission is TRUE, FALSE otherwise.
 */
Ptpd_Rb_boolean Ptpd_Rb_IsImmediateTimeTriggerTrue(void)
{
    return gPtpd_Rb_Provider_st.triggerTime;
}


/** @brief Resets the Immediate time transmission flag to FALSE.
 * @details
 * This function resets the Immediate time transmission flag to FALSE. It shall
 * be called as soon as the transmission is initiated.
 * @param[in]  None.
 * @return None.
 */
void Ptpd_Rb_ResetImmediateTimeTrigger(void)
{
    int ret_code = 0;

    ret_code = pthread_mutex_lock( &gMutex_ImmTrigg ) ;

    if(EOK == ret_code)
    {
        /* Time info will sent on the bus. Now reset the Immediate time transmission flag to FALSE */
        gPtpd_Rb_Provider_st.triggerTime = PTPD_RB_FALSE;

        printf("TriggerTime is reset to false!\n");

        ret_code = pthread_mutex_unlock( &gMutex_ImmTrigg );
        if(EOK != ret_code)
        {
            printf ("PTPD-RB-ERROR : pthread_mutex_unlock(gMutex_ImmTrig) failed in ResetImmediateTimeTrigger. Error code : %s\n",
                strerror(ret_code));
        }
    }
    else
    {
        printf ("PTPD-RB-ERROR : pthread_mutex_lock(gMutex_ImmTrig) in ResetImmediateTimeTrigger failed. Error code: %s\n",
        strerror(ret_code));
    }
}


/** @brief Updates the SubTlv(UserData) information into the ptpd buffer.
 * @details
 * This function is used by Provider to update the SubTlv (write user data in
 * the PTP frame) that was received from the TSync.
 * @param[out]  userDataPtrU8    Pointer to SubTLV array - To update Userdata.
 * @param[in]   userDataSizeU8   Size of the buffer being passed.
 * @return None.
 */
void Ptpd_Rb_WriteUserData(uint8_t *userDataPtrU8, uint8_t userDataSizeU8)
{
    int ret_code = 0;

    if(( NULL != userDataPtrU8 ) && (userDataSizeU8 >= PTPD_RB_FUP_AUTOSAR_SUBTLV_USERDATA_LEN))
    {
        Ptpd_Rb_SubTlv_Userdata_t *lSubtlv_ptr = (Ptpd_Rb_SubTlv_Userdata_t*)(userDataPtrU8);

        ret_code = pthread_mutex_lock( &gMutex_Provider ) ;

        if(EOK == ret_code)
        {
            /* SubTLV type */
            lSubtlv_ptr->subtlvType = PTPD_RB_FUP_USERDATA_WITHOUT_CRC;

            /* SubTLV length */
            lSubtlv_ptr->subtlvLength = PTPD_RB_FUP_USERDATA_LENGTH;

            /* Length of the Subtlv type - User data length */
            lSubtlv_ptr->userDataLen = gPtpd_Rb_Provider_st.userData.userDataLength;

            /* User data byte 0 */
            lSubtlv_ptr->userDataByte0 = gPtpd_Rb_Provider_st.userData.userByte0;

            /* User data byte 1 */
            lSubtlv_ptr->userDataByte1 = gPtpd_Rb_Provider_st.userData.userByte1;

            /* User data byte 2 */
            lSubtlv_ptr->userDataByte2 = gPtpd_Rb_Provider_st.userData.userByte2;

            /* Reserved field - To be used if CRC is introduced*/
            lSubtlv_ptr->reserved = PTPD_RB_ZERO;

            ret_code = pthread_mutex_unlock( &gMutex_Provider );

            if(EOK != ret_code)
            {
                printf ("PTPD-RB-ERROR : pthread_mutex_unlock(gMutex_Provider) failed in Ptpd_Rb_WriteUserData. Error code : %s\n",
                    strerror(ret_code));
            }
        }
        else
        {
            printf ("PTPD-RB-ERROR : pthread_mutex_lock(gMutex_Provider) in Ptpd_Rb_WriteUserData failed. Error code: %s\n",
            strerror(ret_code));
        }
    }
}