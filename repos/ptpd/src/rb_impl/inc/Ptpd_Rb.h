// =============================================================================
//  C O P Y R I G H T
// -----------------------------------------------------------------------------
//  Copyright (c) 2021 by Robert Bosch GmbH. All rights reserved.
//
//  This file is property of Robert Bosch GmbH. Any unauthorized copy, use or
//  distribution is an offensive act against international law and may be
//  prosecuted under federal law. Its content is company confidential.
// =============================================================================


#ifndef PTPD_RB_H
#define PTPD_RB_H

#ifdef __cplusplus
extern "C" {
#endif


#include "rb_impl/inc/Ptpd_Rb_Cfg.h"
#include "Ptpd_Rb_Types.h"

#ifndef __QNXNTO__
/* Success return value for linux variant */
#define EOK 0u
#endif /* __QNXNTO __ */

/** @brief Init function to update configuration information.
 * @details
 * This function stores the configuration information into a global structure.
 * The information is received from the command line options.
 * @param[in]  pdelay        Static Propagation delay.
 * @param[in]  domainNumber  Time Base Identifier.
 * @param[in]  isProvider    If the ptpd instance is Provider.
 * @return boolean           TRUE for successful Init
 *                           FALSE otherwise.
 */
Ptpd_Rb_boolean Ptpd_Rb_Init(double pdelay, uint16_t domainNumber,
Ptpd_Rb_boolean isProvider);

/** @brief Deinit function to perform graceful shutdown.
 * @details
 * This functions performs all the activities associated with graceful shutdown
 * - Like close the connection with TSync, set the tsync handle to NULL.
 * @param None.
 * @return None.
 */
void Ptpd_Rb_Deinit(void);

/** @brief Update the VirtualLocalTime for given message type.
 * @details
 * This functions gets the Virtual local Time from the TSync and updates it in
 * the consumer data structure. Dependending on the argument - Sync or FUP
 * the VLT will be updated in the respective variable. These values are T1 and
 * T2 for the globalTime computation.
 * @param MsgType_en              PTPD_RB_INGRESS_T1 for updating T1
 *                                PTPD_RB_INGRESS_T2 for updating T2.
 * @return None.
 */
void Ptpd_Rb_ReadIngressVLT( Ptpd_Rb_StateType_t VltType_en );

/** @brief Update the VirtualLocalTime for given message type.
 * @details
 * This functions gets the Virtual local Time from the TSync and updates it in
 * the provider data structure. Dependending on the argument - Sync or FUP
 * the VLT will be updated in the respective variable. These values are T0 and
 * T2 for the preciseOriginTime computation.
 * @param MsgType_en              PTPD_RB_EGRESS_T0 for updating T0
 *                                PTPD_RB_EGRESS_T4 for updating T4.
 * @return None.
 */
void Ptpd_Rb_ReadEgressVLT( Ptpd_Rb_EgressVltType_t VltType_en );


/** @brief Function to update the follow-up information received .
 * @details
 * This function stores the follow-up information into a global data structure.
 * The information is originally received from the follow-up message.
 * @param[in]  originTimePtrU8          Precise Origin Time.
 * @param[in]  corrTimePtrU8            Correction field.
 * @param[in]  userDataPtrU8            Pointer to buffer containing userdata.
 * @return None.
 */
void Ptpd_Rb_ReadConsumerFupInfo(const uint8_t *originTimePtrU8,
                                 const uint8_t *corrTimePtrU8,
                                 const uint8_t *userDataPtrU8);


/** @brief Computes the globalTime and calls BusSetGlobalTime.
 * @details
 * This functions performs the globalTime computation and makes call to
 * BusSetGlobalTime. The fomula for globalTime computation is:
 *    globalTime = PreciseOriginTime + correctionfield + (T2vlt-T1vlt) + Pdelay;
 *    localTime = T2vlt,
 *    measureData = Pdelay
 * @param MsgType_en              PTPD_RB_INGRESS_T1 for updating T1
 *                                PTPD_RB_INGRESS_T2 for updating T2.
 * @return None.
 */
void Ptpd_Rb_ConsumerSend( void );


/** @brief Update the GlobalTime and VirtualLocalTime for T0 and T0Vlt
 * respectively.
 * @details
 * This function gets the Virtual local Time and GlobalTime from TSync and
 * store it as T0 and T0Vlt. These values are used later to compute OriginTime.
 * @param None.
 * @return None.
 */
void Ptpd_Rb_BusGetGlobalTime( void );


/** @brief Fetch T4Vlt and compute PreciseOriginTime .
 * @details
 * This functions fetches T4Vlt from tsync. Then based on T4Vlt and T0Vlt
 * (which is stored previously in Provider information), T0
 * is interpolated to compute the PreciseOriginTime.
 * @param None.
 * @return None.
 */
void Ptpd_Rb_ComputeOriginTime( void );


/** @brief Fetch PreciseOriginTime from Provider data-structure.
 * @details
 * This functions fetches origin time that was computed and stored in provider
 * data-structure based on T4Vlt and T0Vlt
 * @param originTimePtrU8      Pointer to store the computed origin time.
 * @return None.
 */
void Ptpd_Rb_WriteOriginTime( uint8_t *originTimePtrU8,  uint8_t originTimeSize);


/** @brief Algorithm to interpolate the global time based on VirtualLocalTimes.
 * @details
 * This function interpolates the global time based on the passed
 * VirtualLocalTime. The computation is as follows :
 * new_global_time =
 * global_time_passed + (new_virtual_local_time - old_virtual_local_time)
 * @param[in]  vlt1_st         The new Virtual Local time.
 * @param[in]  vlt2_st         The previous/old Virtual Local Time.
 * @param[in]  globalTime_st   The the previous/old global time.
 * @param[out] result_st       Pointer to newly computed global time.
 * @return None.
 */
void Ptpd_Rb_InterpolateVlt(const TSync_VirtualLocalTimeType *vlt1_st,
                                 const TSync_VirtualLocalTimeType *vlt2_st,
                                 const Ptpd_Rb_TimeIntDiffType_t *globalTime_st,
                                 TSync_TimeStampType *result_st );


/** @brief Callback function to indicate Immediate time transmission.
 * @details
 * This function is called by TSync to indicate that immediate time transmission
 * is needed. It sets triggerTime variable to TRUE. This will enable the time
 * transmission on the Bus in the next cycle.
 * @param[in]  domainId        The domain id for which time has to be
 * transmitted.
 * @return E_OK if successful, E_NOT_OK otherwise.
 */
TSync_ReturnType Ptpd_Rb_TransmitGlobalTimeCallback(TSync_SynchronizedTimeBaseType domainId);

/** @brief Returns if Immediate time transmission is set to TRUE.
 * @details
 * This function returns if the Immediate time transmission is set to TRUE.
 * @param[in]  None
 * @return TRUE if Immediate time transmission is TRUE, FALSE otherwise.
 */
Ptpd_Rb_boolean Ptpd_Rb_IsImmediateTimeTriggerTrue(void);


/** @brief Resets the Immediate time transmission flag to FALSE.
 * @details
 * This function resets the Immediate time transmission flag to FALSE. It shall
 * be called as soon as the transmission is initiated.
 * @param[in]  None.
 * @return None.
 */
void Ptpd_Rb_ResetImmediateTimeTrigger(void);


/** @brief Updates the SubTlv(UserData) information into the ptpd buffer.
 * @details
 * This function is used by Provider to update the SubTlv (write user data in
 * the PTP frame) that was received from the TSync.
 * @param[out]  buf       Pointer to SubTLV array - To update Userdata.
 * @param[in]  buf_size   Size of the buffer being passed.
 * @return None.
 */
void Ptpd_Rb_WriteUserData(uint8_t *buf, uint8_t buf_size);


/** @brief Reads the SubTlv(UserData) information from the ptpd buffer.
 * @details
 * This function is used by Consumer to read/extract the SubTlv (read user data
 * in the PTP frame) that was sent by the Ethernet Bus.
 * @param[in]  buf      Pointer to buffer where SubTLV info can be read from.
 * @return None.
 */
void Ptpd_Rb_ReadUserData(const uint8_t *userDataPtrU8);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* PTPD_RB_H */



