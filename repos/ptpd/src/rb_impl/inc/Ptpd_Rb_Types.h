// =============================================================================
//  C O P Y R I G H T
// -----------------------------------------------------------------------------
//  Copyright (c) 2021 by Robert Bosch GmbH. All rights reserved.
//
//  This file is property of Robert Bosch GmbH. Any unauthorized copy, use or
//  distribution is an offensive act against international law and may be
//  prosecuted under federal law. Its content is company confidential.
// =============================================================================


#ifndef PTPD_RB_TYPES_H
#define PTPD_RB_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif


#include <stdint.h>
#include "bosch/vrte/tsync/tsync_ptp_lib.h"


/**
 * @brief Macro to define indices of various fields in Followup message
 */
#define PTPD_RB_FUP_MSG_TYPE_IDX                 0u
#define PTPD_RB_FUP_FLAGS_OCTET_0_IDX            6u
#define PTPD_RB_FUP_FLAGS_OCTET_1_IDX            7u
#define PTPD_RB_FUP_MSG_LENGTH_IDX               2u
#define PTPD_RB_FUP_SEQUENCE_ID_IDX              30u
#define PTPD_RB_FUP_CONTROL_IDX                  32u
#define PTPD_RB_FUP_LOG_MSG_INTERVAL_IDX         33u
#define PTPD_RB_FUP_ORIGIN_TIME_SECONDSHI_IDX    34u
#define PTPD_RB_FUP_ORIGIN_TIME_SECONDS_IDX      36u
#define PTPD_RB_FUP_ORIGIN_TIME_NANOSECONDS_IDX  40u
#define PTPD_RB_FUP_INFO_START_IDX               44u
#define PTPD_RB_FUP_AUTOSAR_USERDATA_IDX         88u

/**
 * @brief SubTlv type for UserData without CRC */
#define PTPD_RB_FUP_USERDATA_WITHOUT_CRC         0x61u

/**
 * @brief UserData length without CRC */
#define PTPD_RB_FUP_USERDATA_LENGTH              0x05u

/**
 * @brief Autosar SubTLV length for UserData */
#define PTPD_RB_FUP_AUTOSAR_SUBTLV_USERDATA_LEN  0x07

/**
 * @brief Total Followup TLV information (IEEE + AUTOSAR ) = 49 bytes
 *        FollowUp Tlv info from IEEE           = 32 bytes
 *        Followup Tlv info from AUOSAR         = 10 bytes
 *        Followup SubTlv from AUTOSAR          = 7 bytes (For UserData SubTlv)
 */
#define PTPD_RB_FUP_INFO_TLV_LENGTH          49u

/* Macros for Follow-Up info IEEE 802.1AS */
#define PTPD_RB_FUP_TLV_TYPE                 0x0003u
#define PTPD_RB_FUP_TLV_LENGTH               0x001Cu    /* 28u */
#define PTPD_RB_FUP_TLV_ORGAID               0x0080C2u
#define PTPD_RB_FUP_TLV_ORGASUBTYPE          0x00001u
#define PTPD_RB_FUP_TLV_SCALEDRATEOFF        0x00000000u
#define PTPD_RB_FUP_TLV_GMTIMEBASEIND        0x0000u
#define PTPD_RB_FUP_TLV_LASTGMPHASECHG       0x00u           /* 12 Bytes */
#define PTPD_RB_FUP_TLV_LASTGMFREQCHG        0x000000u

/** @brief Macros for Follow-Up info AUTOSAR */
#define PTPD_RB_FUP_AUTOSAR_TLV_TYPE         0x0003u
#define PTPD_RB_FUP_AUTOSAR_TLV_LENGTH       13u  /* 6 + SubTLVs */
#define PTPD_RB_FUP_AUTOSAR_TLV_ORGAID       0x1A75FB /* AUTOSAR */
#define PTPD_RB_FUP_AUTOSAR_TLV_ORGSUBTYPE   0x605676 /* GlobalTimeEthTSyn */


/** @brief Magic Number macro for 0u */
#define PTPD_RB_ZERO                        0u

/** @brief Magic Number macro for 1u */
#define PTPD_RB_ONE                         1u

/** @brief Magic Number macro for 32u */
#define PTPD_RB_THIRTYTWO                   32U

/** @brief Magic Number macro for Nanoseconds */
#define PTPD_RB_NANOSEC                     1000000000UL

/** @brief Max Value of Nanoseconds */
#define PTPD_RB_NS_UPPRLIM                  0x3B9AC9FFU

/** @brief Mask for extracting lower 32bits from unit64_t variable */
#define PTPD_RB_LOWER_DOUBLE_WORD_MASK            0x00000000FFFFFFFFUL

/** @brief Mask for extracting upper 32bits from unit64_t variable */
#define PTPD_RB_HIGHER_DOUBLE_WORD_MASK           0xFFFFFFFF00000000UL

/** @brief Macro to for sign of Timestamp - 1 means POSITIVE timestamp */
#define PTPD_RB_POSITIVE     1u

/** @brief Macro to for sign of Timestamp - 1 means NEGATIVE timestamp */
#define PTPD_RB_NEGATIVE     0u

/** @brief Macro for TRUE */
#define PTPD_RB_TRUE     1u

/** @brief Macro for FALSE */
#define PTPD_RB_FALSE    0u

/** @brief Typedef to define sign of a number */
typedef uint8_t Ptpd_Rb_Sign_t;

/** @brief Typedef to define boolean type */
typedef uint8_t Ptpd_Rb_boolean;

/** @brief Enum to identify the message type - Consumer */
typedef enum
{
    PTPD_RB_INGRESS_T1 = 0,
    PTPD_RB_INGRESS_T2 = 1,
} Ptpd_Rb_IngressVltType_t;

/** @brief Enum to hold the current State of the Sync-Cycle */
typedef enum
{
    PTPD_RB_STATE_SYNC_RCVD = 0,
    PTPD_RB_STATE_FUP_RCVD,
    PTPD_RB_STATE_INVALID

} Ptpd_Rb_StateType_t;


/** @brief Enum to identify the message type - Provider */
typedef enum
{
    PTPD_RB_EGRESS_T0 = 0,
    PTPD_RB_EGRESS_T4 = 1
} Ptpd_Rb_EgressVltType_t;


/** @brief Enum to hold the current State of the Sync-Cycle for Provider */
typedef enum
{
    PTPD_RB_PROVIDER_SYNC_READY = 0,
    PTPD_RB_PROVIDER_FUP_READY,
    PTPD_RB_PROVIDER_INVALID

} Ptpd_Rb_ProviderStateType_t;


/** @brief Type for expressing timestamps along with the sign */
typedef struct
{
    /** @brief  TimeStamp value */
    TSync_TimeStampType ts;

    /** @brief  Positive (True) / negative (False) time */
    Ptpd_Rb_Sign_t sign;

}Ptpd_Rb_TimeIntDiffType_t;


/** @brief Defines various SubTlvs that are supported by AUTOSAR
*/
typedef struct
{
    TSync_UserDataType    userData;
}Ptpd_Rb_SubTlv_t;


/** @brief Defines the Follow_Up message fields that are required for the
 * calculation of globaltime
*/
typedef struct
{
    /** @brief  Correction field as received in follow-up message */
    Ptpd_Rb_TimeIntDiffType_t  correctionField_st;

    /** @brief  Sync Origin timestamp as received in the follow-up message */
    Ptpd_Rb_TimeIntDiffType_t  PreciseOriginTimeStamp_st;

    /** SubTLV information */
    Ptpd_Rb_SubTlv_t subTlv_st;

}Ptpd_Rb_FollowUpMsg_t;


/** @brief Defines the all the received information - both Sync and Fup message
 * fields that are required for the calculation of globaltime
*/
typedef struct
{
    /** @brief  Stores the follow-up message information */
    Ptpd_Rb_FollowUpMsg_t              followUpMsg_st;

    /** @brief  Stores the Ingress Time Stamp of Sync message - T1 */
    TSync_VirtualLocalTimeType              t1VLT_st;

    /** @brief   Stores the Ingress Time Stamp of FUP message - T2 */
    TSync_VirtualLocalTimeType              t2VLT_st;

    /** @brief   The current State of the Sync Cycle */
    Ptpd_Rb_StateType_t                syncState;

} Ptpd_Rb_Consumer_t;


/** @brief Defines the Timestamp type compatible to PTP daemon.
*/
typedef struct
{
    /** @brief 32 bit LSB of the 48 bits Seconds part of the time */
    uint32_t seconds;

    /** @brief 16 bit MSB of the 48 bits Seconds part of the time */
    uint16_t secondsHi;

    /** @brief nanoseconds part of the time */
    uint32_t nanoseconds;

}Ptpd_Rb_TimeStampType_t;


/** @brief Defines all the Provider information needed for the calculation of
 * preciseOriginTime
*/
typedef struct
{
    /** @brief  Stores the Engress Virtual Local Time of Sync message - T0 */
    TSync_VirtualLocalTimeType              t0VLT_st;

    /** @brief   Stores the Engress Virtual Local Time when Sync message was on
     * the Bus - T4 */
    TSync_VirtualLocalTimeType              t4VLT_st;

    /** @brief  Stores the Engress Global Time of Sync message - T0 */
    TSync_TimeStampType                     t0GlobalTime_st;

    /** @brief  Stores the Origin time according to the following computation
     * originTime = T0 + (T4Vlt - T0Vlt)
     */
    TSync_TimeStampType                     originTime_st;

    /* User data */
    TSync_UserDataType                      userData;

    /** @brief  State that defines SYNC/FUP readiness */
    Ptpd_Rb_ProviderStateType_t        syncState;

    /** @brief  Indicates if ImmediateTimeTransmission is needed.
     * TRUE = Transmit immediately
     * FALSE = No immediate transmission requested */
    Ptpd_Rb_boolean        triggerTime;

} Ptpd_Rb_Provider_t;


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* PTPD_RB_TYPES_H */
