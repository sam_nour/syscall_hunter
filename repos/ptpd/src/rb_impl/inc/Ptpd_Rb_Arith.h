// =============================================================================
//  C O P Y R I G H T
// -----------------------------------------------------------------------------
//  Copyright (c) 2021 by Robert Bosch GmbH. All rights reserved.
//
//  This file is property of Robert Bosch GmbH. Any unauthorized copy, use or
//  distribution is an offensive act against international law and may be
//  prosecuted under federal law. Its content is company confidential.
// =============================================================================


#ifndef PTPD_RB_ARITH_H
#define PTPD_RB_ARITH_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Ptpd_Rb_Types.h"

/** @brief Algorithm to subtract two timestamps.
 * @details
 * This function enables the user to subtract two timestamps.
 * @param[in]  Time1             First Timestamp operand.
 * @param[in]  Time2             Second Timestamp operand.
 * @param[out] TimeDiff          The result is of the operation is updated here.
 * @return None.
 */
void Ptpd_Rb_SubTi( const Ptpd_Rb_TimeIntDiffType_t   *Time1,
                    const Ptpd_Rb_TimeIntDiffType_t   *Time2,
                          Ptpd_Rb_TimeIntDiffType_t   *TimeDiff );

/** @brief Algorithm to add two timestamps.
 * @details
 * This function enables the user to add two timestamps. The timestamps
 * have sign as well. Hence as per the sign, the addition/subtraction shall be
 * performed.
 * @param[in]  Time1             First Timestamp operand.
 * @param[in]  Time2             Second Timestamp operand.
 * @param[out] TimeResult        The result is of the operation is updated here.
 * @return None.
 */
void Ptpd_Rb_AddTi( const Ptpd_Rb_TimeIntDiffType_t *Time1,
                    const Ptpd_Rb_TimeIntDiffType_t *Time2,
                          Ptpd_Rb_TimeIntDiffType_t *TimeResult );

/** @brief Algorithm to convert uint64_t(nanoseconds) value to TimeStamp stamp.
 * @details
 * This function enables the user convert uint64_t value into TimeStamp type.
 * It also initializes the sign of the TimeStamp generated as POSITIVE.
 * @param[in]  Value_u64         First Timestamp operand.
 * @param[in]  Factor_u8         Second Timestamp operand.
 * @param[out] Time_pst          The result is of the operation is updated here.
 * @return None.
 */
void Ptpd_Rb_CnvToTimeStamp( uint64_t               Value_u64,
                                  uint8_t                Factor_u8,
                                  Ptpd_Rb_TimeIntDiffType_t *Time_pst );

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* PTPD_RB_ARITH_H */



