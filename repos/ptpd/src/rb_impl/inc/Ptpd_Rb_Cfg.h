// =============================================================================
//  C O P Y R I G H T
// -----------------------------------------------------------------------------
//  Copyright (c) 2021 by Robert Bosch GmbH. All rights reserved.
//
//  This file is property of Robert Bosch GmbH. Any unauthorized copy, use or
//  distribution is an offensive act against international law and may be
//  prosecuted under federal law. Its content is company confidential.
// =============================================================================


#ifndef PTPD_RB_CFG_H
#define PTPD_RB_CFG_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Ptpd_Rb_Types.h"
#include <bosch/vrte/tsync/tsync_ptp_lib.h>


/** @brief Type for storing the configuration information and information that
 * is derived from the configuration. Configuration info mainly comes from the
 * command line.
*/
typedef struct
{
    /** @brief TimeBase id associated with the current ptpd instance */
    uint16_t                           timeBaseId_u16;

    /** @brief The static GlobalPropogationDelay - this is statically configured
     * as per autosar requirement. */
    Ptpd_Rb_TimeIntDiffType_t       pdelayCfg_st;

    /**  @brief Time base handle assciated with the above TimeBase id */
    TSync_TimeBaseHandleType           tsyncHandle;

} Ptpd_Rb_Cfg_t;


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* PTPD_RB_CFG_H */


