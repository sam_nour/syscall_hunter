from conans import ConanFile, AutoToolsBuildEnvironment
import os
import pathlib


class PTPDaemonConan(ConanFile):
    name = "tsync-ptp-daemon"
    description = "This package contains the PTP Daemon OSS implementation, used for Time Synchronization."
    license = "Copyright."
    url = "https://dev.azure.com/bosch-denso-common/VRTE-TimeManagement/_git/ptpd"
    revision = "auto"
    author = "FUN"

    # Settings must relate to ~/.conan/settings.yml and be a subset, are parameterized by the profile
    settings = "os", "arch", "compiler"
    # generators maintained in base class

    # Add vrte base logic
    python_requires = "vrte-conan-base/1.2.0@vrte-ci/tools"
    python_requires_extend = "vrte-conan-base.VRTEBase"

    options = {}
    default_options = {}

    #########################################################################################

    def requirements(self):
        self.requires(f"tsync-ptp-lib/[^1.0.0]@vrte-ci/stable")

    # TO-DO items on improvements :
    # - Find a way to pass the linux autoconf host target to the configure() command below
    # - Use autotools for QNX build, removing dependency on the do_build* scripts
    # - In-source builds currently done for autotools, to be placed in a separate _build directory
    # - The set_deps() workaround above needs to be removed, once the points above have been implemented
    def build(self):
        self.run("rm -rf _bin configure")
        if self.settings.os == "Neutrino":
            #self.set_deps_env()
            #os.environ["CONAN_BIN_DIR"] = self._bin_dir
            #os.environ["CONAN_BUILD_DIR"] = self._build_dir
            target_arch = str(self.settings.arch)
            if target_arch.startswith('arm'):
                build_arch_prefix = "aarch64-unknown"
            else:
                build_arch_prefix = "x86_64-pc"
            host = build_arch_prefix + "-nto-qnx7.1.0" # for QNX
        else:
            #host = "x86_64-boschdenso-linux" # For Linux
            host = self.env["CROSS_COMPILE"][:-1]
        self.run("autoreconf -vfi")
        autotools =  AutoToolsBuildEnvironment(self)
        #autotools.defines.append("PTPD_RB_DEBUG")
        autotools.configure(host=host, args=[
            "ac_cv_func_malloc_0_nonnull=yes",
            "--with-pcap-config=no",
            "--enable-experimental-options",
            "--disable-snmp",
            "--prefix=",
        ])
        self.run("make clean")
        autotools.make()
        bindir = pathlib.Path.cwd() / self._bin_dir
        autotools.install(vars={"DESTDIR": str(bindir)})

    def package(self):
        """ Conan package method. """
        #super().package()
        #The PTPd is not part of the release
        #self.copy(pattern=f"deployment-ruleset-{self.name}.yml", dst="integration", src="integration", keep_path=False)
        self.copy("*", dst="./rb-aptpd2/bin", src="./_bin/bin/", keep_path=True)
        self.copy("*", dst="./share", src="./_bin/share", keep_path=True)
        self.copy("COPYRIGHT", src="./", dst="./", keep_path=True)
