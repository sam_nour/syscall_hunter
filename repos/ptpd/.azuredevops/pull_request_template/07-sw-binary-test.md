# Software Binary Test
##  Test Specification and Traceability

- [ ] Test design available
- [ ] Test steps available and complete
- [ ] Relevant test methods are applied and tagged (test_equi_class, test_req_based, test_interface)
- [ ] Test specification linked to sw requirements
- [ ] SW requirement's verification criteria taken into account
- [ ] If applicable, Software Safety Analysis measures are considered
- [ ] Fully tested SW requirements set to status 'verified'
- [ ] Test specification linked to architectural element (binary)
- [ ] Test result linked to the test specification

## Test Implementation & Execution

- [ ] All interactions on diagrams tested
- [ ] All offered interfaces tested
- [ ] All design decisions tested
- [ ] All error cases and states considered
- [ ] All related requirements are tested by at least one test
- [ ] Skipped regression tests (e.g xfail) are tracked in a Problem
