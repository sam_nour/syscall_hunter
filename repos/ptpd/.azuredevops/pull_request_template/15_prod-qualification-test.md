# Product Qualification Test
##  Test Specification and Traceability

- [ ] Test design available
- [ ] Test steps available and complete
- [ ] Relevant test methods are applied and tagged (test_equi_class, test_resource, test_req_based, test_operational)
- [ ] Test specification linked to product requirements
- [ ] Product requirement's verification criteria taken into account
- [ ] If applicable, Software Safety Analysis measures are considered
- [ ] Fully tested product requirements set to status `verified`
- [ ] Test result linked to the test specification

## Test Implementation & Execution

- [ ] All error cases and states considered
- [ ] All related requirements are tested by at least one test
- [ ] Skipped regression tests (e.g xfail) are tracked in a Problem
