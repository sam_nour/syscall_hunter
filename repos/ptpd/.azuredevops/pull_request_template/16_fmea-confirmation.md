# FMEA Review Confirmation 
##  Checklist

- [ ] The FMEA was done in accordance with appropriate standards or guidelines and the defined objectives
- [ ] The FMEA indicates whether the respective safety goals or safety requirements are complied with or not
- [ ] Prevention, detection, or effect mitigation measures were derived
- [ ] The measures derived from the safety analysis are implemented
- [ ] The newly identified hazards are included in the updated hazard analysis and risk assessment
- [ ] The fault models used for the safety analyses are at the software architectural level
- [ ] Additional safety-related test cases were considered
- [ ] The safety analysis is verified

-  The qualitative safety analysis includes the following:
    - [ ] A systematic identification of faults or failures
    - [ ] The evaluation of the consequences of each identified fault
    - [ ] The identification of the causes of each identified fault
    - [ ] The identification, or the support for the identification of potential safety concept weaknesses
