# General Document
## Formal Elements
- [ ] The document contains title, change history, date, version, author and approver (if relevant)
- [ ] Appropriate version of template is used (if available)
- [ ] Goal and scope of the document are stated
- [ ] Table of contents is correct and complete
- [ ] Document is compliant with law and regulations 
- [ ] Document doesn't contain confidential information

## Content 
- [ ] well structured, focused on the goal and up to date
- [ ] State of the art, precise, concise, complete and easy to understood for intended users
- [ ] Unambiguous, free of spelling and grammatical errors
- [ ] No redundancies, contradictions, conflicts and inconsistencies
- [ ] Facts, assumptions and open points are clearly distinguishable
- [ ] Definitions are correct and clear
- [ ] Used terminology is consistent
- [ ] Used abbreviations are very common or explained
- [ ] References to outside are available, correct and complete
