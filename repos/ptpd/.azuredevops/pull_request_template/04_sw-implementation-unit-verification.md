# SW Implementation and Unit Verification
## Maintenance, Readability, Understandability

- [ ] The code fits to the description in the related work item
- [ ] The code is readable and understandable
- [ ] The implementation is simple and clean and avoids complexity risks
- [ ] Unit tests are available for new functionality
- [ ] A code formatter (e.g. clang-format) is applied to newly introduced code

## Conformity to Design and Rules

- [ ] The code does what it is supposed to do (according to detailed design)
- [ ] The detailed design is in sync with the code
- [ ] There are no obvious backdoors or insecure implementations
- [ ] All implausible / invalid function input and output values are handled
- [ ] All function return values are handled
- [ ] The dynamic behavior of the code is understood and reasonable
- [ ] Boolean statements are correct and simple
- [ ] Debug functionality is encapsulated efficiently
- [ ] The build file (e.g CMakefile) is reviewed to ensure no undesired functionality is compiled

##  Unit Verification
- [ ] Relevant test methods are applied and tagged (test_equi_class, test_boundary, test_req_based, test_interface)
- [ ] Sufficient documentation
- [ ] Unit sufficiently isolated (using stubs, mocks)
- [ ] The code is inspected when deriving test cases
- [ ] Test end criteria met: Coverage goals achieved; uncovered statements and branches justified
- [ ] Static code analysis goals achieved
- [ ] Compiler warnings fixed or justified
- [ ] UT test name matches the unit name
- [ ] Skipped regression tests (e.g xfail) are tracked in a Problem

##  Product Conformity
- [ ] Copyright clause is aligned with defined copyright use cases
