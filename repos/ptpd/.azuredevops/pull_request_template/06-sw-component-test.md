# Software Component Test
##  Test Specification and Traceability

- [ ] Test specification *need* available
- [ ] Test specification linked to an architectural element
- [ ] Relevant test methods are applied and tagged (test_equi_class, test_boundary, test_req_based, test_interface)
- [ ] Test result linked to the test specification

##  Test Implementation & Execution

- [ ] All interactions on diagrams tested
- [ ] All offered interfaces tested
- [ ] Relevant design decisions tested
- [ ] All error cases and states considered
- [ ] Skipped regression tests (e.g xfail) are tracked in a Problem
