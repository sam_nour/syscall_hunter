# FMEA Review Verification 
##  Checklist
 
- [ ] The appropriate product version is selected
- [ ] The appropriate changes in the impact analysis and the impact assessment are made
- [ ] The analysis target and relevant elements are clear
- [ ] The internal review is complete
- [ ] All the fields are filled
- [ ] All the considered HAZOP Keywords are reviewed
- [ ] The failure mode and effect descriptions are true, clear, realistic and complete
- [ ] The current design is sufficient, and all mandatory changes from the action plan have been committed to
- [ ] All software measures from the action plan are either closed or linked to a work item
