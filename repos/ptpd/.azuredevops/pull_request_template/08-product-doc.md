# FC Documentation
## Product Documentation 
- [ ] The template is followed
- [ ] Update is concise and clear  
- [ ] The document is up-to-date and consistent with implementation 
- [ ] The document is internally consistent (e.g. terminology) 
- [ ] Confidential information is not included (e.g. person/customer names)

## Delivery Notes 
- [ ] The template is followed
- [ ] Update is concise and clear  
- [ ] Changes from the previous release are correctly added and complete  
- [ ] Confidential information is not included (e.g. person/customer names)

## Known Limitations 
- [ ] The template is followed 
- [ ] The description of a limitation is concise and clear 
- [ ] Impacts on system are described for defects
- [ ] Changes regarding limitations from the previous release are correctly added and complete (new and not resolved issues) 
- [ ] Confidential information is not included (e.g. person/customer names)
