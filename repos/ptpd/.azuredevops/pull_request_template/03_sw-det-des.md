# Software Detailed Design
##  Checklist
- [ ] Detailed design fits the intended functionality described in the corresponding WI
- [ ] Class and function design is clean, readable, avoids complexity risks, testable and complete
- [ ] Detailed design and software architecture are in sync
- [ ] Folders and namespaces are in sync with software architecture
- [ ] The chosen design is sufficiently explained; there are enough diagrams
- [ ] Detailed design and (existing) code are in sync
- [ ] Doxygen Guidelines are adhered to
