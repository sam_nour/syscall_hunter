# Product-Architecture
##  Checklist
- [ ] All the satisfying requirements are covered by the architecture ("satisfies link")
- [ ] All architectural elements match the implementation (as needs elements)
- [ ] All architecture elements justified with a design decision
- [ ] All architecture elements well described
- [ ] All architecture elements adhere to the design principles 
(hierarchical structure, restricted size and complexity, strong cohesion, loose coupling, scheduling, resource management)
- [ ] Dynamic behavior sufficiently described (e.g. using Plantuml diagrams)
- [ ] Static relation sufficiently described (e.g. using Sphinx Needs)
- [ ] All required verification criteria are available
- [ ] Architecture design considers verifiability, configurability, feasibility, testability, maintainability
