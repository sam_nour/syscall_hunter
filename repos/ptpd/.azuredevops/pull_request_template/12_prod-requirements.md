# Product-Requirements
##  Checklist
### Content
- [ ] Realizable: No requirement shall contradict unchangeable environment conditions
- [ ] Comprehensible: Requirements shall be formulated in a way which can be understood when reading them only once
- [ ] Solution-neutral: Requirements shall always address the problem space, not solution space
- [ ] Correct: Requirements shall be formulated in a way which leads to the same understanding of this requirement on both the requestor and requirements engineer side
- [ ] Consistent: Requirements shall be consistent with other requirements. E.g. no redundant requirements on the same level
- [ ] Conforming: Requirements shall not conflict with applicable stakeholder requirement specifications (especially those that refer government, automotive industry and product standards, specifications and interfaces)
- [ ] Atomic: Each requirement shall express only a single need
- [ ] Are the keywords defined in RFC2119 (must, shall, should, may) used according to the semantics defined in this standard?\
      Are they used in natural case (e.g. "shall" not "SHALL") and are the keywords "must" and "must not" only used if the related requirement is enforced by law?
- [ ] Structured: The requirements shall be grouped into sensible chapters and sections

### Attribute
- [ ] Attribute "id" shall follow the naming convention to prevent duplication of IDs (unique prefix for each requirement specification)
- [ ] Attribute "id" shall not be reassigned (no more applicable requirements shall be kept in state "obsolete")
- [ ] Attribute "status" shall be either set to "new/changed", "in clarification", "accepted", "decomposed", "rejected", "obsolete", "implemented" or "verified"
- [ ] Are verification criteria defined for each accepted requirement?
      Are these verification criteria either realistically testable or can be satisfied by a design or code review? \
      Evidence: Attribute "Verification Criteria" is not empty in requirements with state "accepted"
- [ ] Attribute "safety_level" shall be set if state is "accepted" or higher
- [ ] Attribute "security_relevant" shall be set if state is "accepted" or higher
- [ ] Attribute "reject_reason" shall be set if state is "rejected" or "obsolete"

### Links
- [ ] The functionality described by the requirement content shall be fully covered by the derived requirements linked in "satisfied_by", if the state is "decomposed" or higher
- [ ] The content of the requirement is consistent with requirements on higher level which link to this requirement (see "satisfies" link)
