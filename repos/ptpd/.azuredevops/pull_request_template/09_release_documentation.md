# Release-Documentation
##  Checklist
- [ ] Release type, use case are clearly described
- [ ] Release Management Strategy and defined templates are followed
- [ ] Release baseline is available
- [ ] Safety/security chapter and related limitations, if any, described
- [ ] Valid process version is specified
- [ ] Safety manual is included and up-to-date
- [ ] List of relevant release components/packages is complete
- [ ] Delivery/release documentation of components and tools is available
- [ ] List of known problems/defects is available and there are no blocking issues for the release
- [ ] Third party SW: OSS report is available, commercial licenses are identified and reported, used mother-company
internal and external SW components (if any) are baselined and released
- [ ] HW, SW, tools dependencies are described
- [ ] Quality reports are available and complete, all required Artifacts are available and evaluated against the release type (e.g. RT8 QM, RT8 ASIL B)
