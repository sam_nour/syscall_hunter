# Product Integration Test
##  Test Specification and Traceability

- [ ] Test specification *need* available
- [ ] Test specification linked to an architectural element
- [ ] Relevant test methods are applied and tagged (test_equi_class, test_boundary, test_req_based, test_interface, test_operational)
- [ ] Test result linked to the test specification
- [ ] Integration Specification reviewed to ensure no undesired functionality is included

##  Test Implementation & Execution

- [ ] All interactions on diagrams tested
- [ ] All offered interfaces tested
- [ ] All design decisions tested
- [ ] All error cases and states considered
- [ ] Skipped regression tests (e.g xfail) are tracked in a Problem
