#!/usr/bin/python3
# ***************************************************************************************************************
# COPYRIGHT RESERVED, Robert Bosch GmbH, 2021. All rights reserved.
# The reproduction, distribution and utilization of this document as well as the communication of its contents to
# others without explicit authorization is prohibited. Offenders will be held liable for the payment of damages.
# All rights reserved in the event of the grant of a patent, utility model or design.
# ***************************************************************************************************************
import argparse
import pathlib
import shutil


def install_docs(fcname, docs, dst):
    source = pathlib.Path.cwd() / docs
    dest = pathlib.Path.cwd() / dst / fcname

    if not dest.parent.exists():
        dest.parent.mkdir(parents=True, exist_ok=True)

    if dest.exists():
        shutil.rmtree(dest)

    shutil.copytree(source, dest)


# ./do_generate_documentation.py ara-core docs _bin_qnx_aarch64
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generates documentation.')
    parser.add_argument('name', help='Component name(e.g. ara-core).')
    parser.add_argument('docs', help='Path to the docs folder(e.g. docs).')
    parser.add_argument('dest', help='Path to the destination folder(e.g. _bin_qnx_x86_64/share/doc).')

    args = parser.parse_args()

    install_docs(args.name, args.docs, args.dest)
