# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
import sys
import os

sys.path.insert(0, os.path.abspath('/conf'))
print("Sys Path: ", sys.path)

import vrteconf as vrteconf



# -- Project information -----------------------------------------------------

project = 'VRTE-Documentation'
copyright = '2021'
author = 'Seema Pathak'

# The full version, including alpha/beta/rc tags
release = vrteconf.release


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = vrteconf.extensions

#Breathe (doxygen settings)
breathe_projects = {
    "VRTE": "_doc/doxygen/xml/"
    }

# PlantUML settings
cwd = os.getcwd()
if os.path.exists(os.path.join(cwd, "../_tools/plantuml.jar")):
    plantuml = 'java -Djava.awt.headless=true -jar %s' % os.path.join(cwd, "../_tools/plantuml.jar")
elif os.path.exists('/usr/share/plantuml/plantuml.jar'):
    plantuml = 'java -Djava.awt.headless=true -jar /usr/share/plantuml/plantuml.jar'
else:
    raise RuntimeError("Could not find plantuml.jar, please check the README.md")

# If we are running on windows, we need to manipulate the path,
# otherwise plantuml will have problems.
if os.name == "nt":
    plantuml = plantuml.replace("/", "\\")
    plantuml = plantuml.replace("\\", "\\\\")

plantuml_output_format = 'png'

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']


# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = ['.rst', '.md']
source_suffix = '.rst'


# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = [
        'Thumbs.db',
        '.DS_Store',
        'build',
        '_dep*',
        '_build*',
        '_tools',
        '_misc',
        '_releases',
        '_tmp',
        '_log',
        '_quality*',
        '_test*',
        '_doxygen*',
        '_build*',
        'doc',
        'README.md',
        'tests',
        '.azuredevops',
        '**/SimpleFcCluster/_build',
        'docs/misc/work_in_progress/*',
        'unused_tools'
        ]


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#

#import sphinx_theme
#html_theme = "stanford_theme"
#html_theme_path = [sphinx_theme.get_html_theme_path('stanford-theme')]

import sphinx_rtd_theme 
html_theme = 'sphinx_rtd_theme' 
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
html_css_files = [
    'custom.css'
]

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['docs/static_html']


# -- Need Directives configuration ------------------------------------------------

needs_services = vrteconf.needs_services
needs_extra_options = vrteconf.needs_extra_options
needs_types = vrteconf.needs_types
needs_extra_links = vrteconf.needs_extra_links
