#!/bin/bash
#=============================================================================
# C O P Y R I G H T
#-----------------------------------------------------------------------------
# Copyright (c) 2021 by Robert Bosch GmbH. All rights reserved.
#
# This file is property of Robert Bosch GmbH. Any unauthorized copy, use or
# distribution is an offensive act against international law and may be
# prosecuted under federal law. Its content is company confidential.
#=============================================================================

# $1: relative path to the clang-format binary
CLANG_BIN=$1

if [[ -z $(which $CLANG_BIN) ]]; then
    echo "Please install clang-format. The code can only be auto formatted if it's installed."
    echo "Use the following command to install it:"
    echo
    echo "sudo apt install clang-format"
    echo
    exit 1
fi

# We use this XML replacement file to check if there are no changes for a file.
xml=`cat <<EOF
<?xml version='1.0'?>
<replacements xml:space='preserve' incomplete_format='false'>
</replacements>
EOF
`

pat="^(src|tests|include|examples)/.*\.(cpp|c|cxx|h|hpp)$"
formatted=0
add_cmd="git add"

for file in $(git diff --cached --name-only); do
    if [[ $file =~ $pat ]]; then
        if [[ $($CLANG_BIN -style=file -output-replacements-xml $file) != $xml ]]; then
            $CLANG_BIN -style=file -i $file
            formatted=1
            add_cmd="$add_cmd $file"
        fi
    fi
done

if [[ $formatted == 1 ]]; then
    echo "Some unformatted files have been automatically formatted. They need to be committed again."
    echo "Use the following command to add the formatted files:"
    echo
    echo $add_cmd
    echo
    echo "You can either create a new commit for the formatting changes or use the following to modify the previous commit:"
    echo
    echo "git commit --amend"
    echo
fi
