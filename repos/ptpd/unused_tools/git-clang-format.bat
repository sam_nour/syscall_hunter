@echo off
echo **********************************************************************************************************************
echo *
echo * COPYRIGHT RESERVED, Robert Bosch GmbH, 2019. All rights reserved.
echo * The reproduction, distribution and utilization of this document as well as the communication of its contents to
echo * others without explicit authorization is prohibited. Offenders will be held liable for the payment of damages.
echo * All rights reserved in the event of the grant of a patent, utility model or design.
echo *
echo **********************************************************************************************************************
setlocal enabledelayedexpansion
echo   Script:    %0
echo   Date:      %DATE%  %TIME%
echo   Node:      %computername%
echo   User:      %username%
echo.

call tini python latest
call python -u ./.git/hooks/git-clang-format.py --binary ./.git/hooks/clang-format.exe