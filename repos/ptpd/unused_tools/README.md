Tools

<h2> License and Copyright </h2>
Copyright (C) 2017-18, Robert Bosch GmbH. All rights reserved.
For detailed information, see the LICENSE file/s distributed with this project.

<h2> Git hooks </h2>
To install the git hooks copy the files in the tools/ directory to .git/hooks/