#!/bin/bash
#
# setup the repository
# - copy git hooks and tools to .git/hooks
set -x

# obtain script source directory for bash/zsh
SCRIPT_DIR=`dirname ${BASH_SOURCE[0]:-${(%):-%x}}`

REPO_FILES="\
  format-staged.sh \
  clang-format.exe \
  git-clang-format.bat \
  git-clang-format.py \
  pre-commit"

GIT_REPO_DIR=$(git rev-parse --show-toplevel)
GIT_HOOKS_DIR=".git/hooks/"
REPO_TOOLS_DIR="tools"

if [ ! -d "${GIT_REPO_DIR}" ]; then
    echo "ERROR: current directory is not a git repository"
    exit 1
fi

if [ -d "${GIT_REPO_DIR}/${GIT_HOOKS_DIR}" ]; then
    pushd ${REPO_TOOLS_DIR}
    cp ${REPO_FILES} ${GIT_REPO_DIR}/${GIT_HOOKS_DIR}
    popd
else
    echo "ERROR: ${GIT_HOOKS_DIR} does not exist or is not in the expected location."
    echo "  - please run that script from the repository root directory."
fi
