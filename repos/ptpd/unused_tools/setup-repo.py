#!/usr/bin/env python
#=============================================================================
# C O P Y R I G H T
#-----------------------------------------------------------------------------
# Copyright (c) 2021 by Robert Bosch GmbH. All rights reserved.
#
# This file is property of Robert Bosch GmbH. Any unauthorized copy, use or
# distribution is an offensive act against international law and may be
# prosecuted under federal law. Its content is company confidential.
#=============================================================================

import git
import os
import shutil

def get_git_root():
    repo = git.Repo(os.getcwd(), search_parent_directories=True)
    root = repo.working_tree_dir
    return root

githook_dir = '/.git/hooks/'
tools_dir = '/tools/'
repo_dir = get_git_root()

file_list = [
    "format-staged.sh",
    "clang-format.exe",
    "git-clang-format.bat",
    "git-clang-format.py",
    "pre-commit"
    ]

def main():
    for filename in file_list:
        shutil.copy (repo_dir + tools_dir + filename, repo_dir + githook_dir)
    return

if __name__ == '__main__':
    main()
