ptpd
============================================

Disclaimer: This PTPd is provided without any warranty of any kind, expressed or implied. It is intended for development purposes only, and is not intended to be used in production. Usage of the PTPd in a production environment is at your own risk and discretion. Please refer to the COPYRIGHT file in the delivery package for additional information on copyright and licensing.
Please note that PTPd is built upon Open Source Code. Hence, the documentation shall be provided only for the newly added implementation.

- Delivery notes under ``docs/delivery_notes``
- Requirements under ``docs/requirements``
- Detailed design under ``docs/detailed_design``

Engineering artifacts
----------------------------

.. toctree::
   :maxdepth: 1

   docs/delivery_notes/delivery_notes.rst
   docs/requirements/AUTOSAR_PRS_TimeSyncProtocol.rst
   docs/detailed_design/dd_ptpd_tsync_ipc.rst
   docs/views/index.rst
