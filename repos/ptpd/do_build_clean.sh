#!/bin/bash -e
# **********************************************************************************************************************
# *                                                                                                                    *
# * COPYRIGHT RESERVED, Robert Bosch GmbH, 2021. All rights reserved.                                                  *
# * The reproduction, distribution and utilization of this document as well as the communication of its contents to    *
# * others without explicit authorization is prohibited. Offenders will be held liable for the payment of damages.     *
# * All rights reserved in the event of the grant of a patent, utility model or design.                                *
# *                                                                                                                    *
# **********************************************************************************************************************

set -x
rm -rf _build_* 2>1 >/dev/null
rm -rf _bin_* 2>1 >/dev/null

set +x

echo "done"
