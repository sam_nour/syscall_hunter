    
import subprocess

def execute_locate_cmd(cmd):
    results = set()  # Initialize an empty set to store the results
    
    try:
        result = subprocess.run(
            cmd,
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True
        )
        
        if result.returncode == 0:
            output_string = result.stdout
            # Split the output into lines and add them to the set
            results.update(output_string.splitlines())
        else:
            error_message = result.stderr
            print("Error:", error_message)
    except Exception as e:
        print("An error occurred:", str(e))
    
    return results



