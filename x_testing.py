import re
from colorama import Fore, Style

def syscall_hunter(syscall: set(), c_cpp_file):
    syscalls = syscall # Add more syscalls as needed

    file_path = c_cpp_file # Replace with the actual path to your file
    found_syscalls = set()

    with open(file_path, 'r') as file:
        code = file.read()

    pattern = r'\b(' + '|'.join(syscalls) + r')\s*\('

    matches = re.findall(pattern, code)
    found_syscalls.update(matches)

    print("Syscalls found in the file:")
    for syscall in found_syscalls:
        print(syscall)

    return found_syscalls

def x86_64_syscalls():
    syscalls_1_2 = set()
    with open('strace_calls.txt', 'r') as f:
        data = f.read()
    lines = data.split('\n')
    # count number of calls
    counter = 0
    # set of unique syscalls(1), syscalls(2)
    pattern = r'\d+\s+(\w+)\s+'
    for line in lines:
        counter += 1
        match = re.search(pattern, line)
        # If a match is found, add to set()
        if match:
            syscalls_1_2.add(match.group(1))
        # Write the results to the output file
        with open("debugging/x86_64_syscalls.txt", 'w') as outfile:
            for syscall in syscalls_1_2:
                outfile.write(syscall + '\n')
    return syscalls_1_2





print(f"{Fore.RED}Hello World{Style.RESET_ALL}")
